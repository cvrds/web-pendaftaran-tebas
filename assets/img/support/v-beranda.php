<!--  HEADER -->
<div id="header">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%;visibility: hidden;" alt="header">
  <div class="container header">

    <div id="Carousel" class="carousel slide sliderlimit">
        <ol class="carousel-indicators">
          <li data-target="#Carousel" data-slide-to="0" class="active"></li>
          <li data-target="#Carousel" data-slide-to="1"></li>
          <!-- SLIDE 3
          <li data-target="#Carousel" data-slide-to="2"></li>
          SLIDE 3 -->
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                            <img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive">
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <h1>TEBAS 2016</h1>
                          <h3>GORESAN PESAN PERUBAHAN</h3>
                          <p>TEBAS (The Best Annual Multimedia Show) lomba multimedia akbar dari Jogjakarta hadir kembali di tahun 2016 dengan tema Goresan Pesan Perubahan. Kritikan yang tidak membatasi pola pikir seseorang untuk dapat menghasilkan pemikiran baru yang bertujuan, mengedukasi, memotivasi, dan menginspirasi yang dituangkan ke dalam karya multimedia untuk perubahan masa depan yang lebih baik dan tidak menimbulkan kritikan baru. Terdapat tiga kompetisi yaitu Indie Movie Competition, Photography Contest dan Battle Of Poster Design.</p>
                          <a class='youtube' href="https://www.youtube.com/embed/YW128_bUa9I"><button type="button" class="btn btn-default btn-lg">VIDEO TEASER</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
							       <br><br>
                            <img src="<?php echo base_url();?>assets/img/koma.png" alt="logo" class="img-responsive">
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <h1>KOMA</h1>
                          <h3>KOMUNITAS MULTIMEDIA AMIKOM</h3>
                          <p>KOMA adalah organisasi mahasiswa yang bergerak dan mewadahi kreatifitas dalam bidang Multimedia. Ada empat divisi di KOMA : Graphic Design, Web Design, Broadcast dan Vfx Animation. KOMA -  Tempatnya Kreator Audio Visual Amikom yang Menghasilkan Karya dengan Kreatifitas Tanpa Batas. </p>
                          <a href="http://koma.or.id" target="blank"><button type="button" class="btn btn-default btn-lg">WEB KOMA</button></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- SLIDE 3
            <div class="item">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                            <img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive">
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <h1>TEBAS 2016</h1>
                          <h3>GORESAN PESAN PERUBAHAN</h3>
                           <p>TEBAS 2016 bertemakan "Goresan Pesan Perubahan" Kritikan yang tidak membatasi pola pikir seseorang untuk dapat menghasilkan pemikiran baru yang bertujuan, mengedukasi, memotivasi, dan menginspirasi yang dituangkan ke dalam karya multimedia untuk perubahan masa depan yang lebih baik dan tidak menimbulkan kritikan baru.</p>
                          <a href="<?php echo site_url('faq');?>"><button type="button" class="btn btn-default btn-lg">MORE INFO</button></a>
                        </div>
                    </div>
                </div>
            </div>
            SLIDE 3 -->
          </div>
      </div>
  </div>
  <div class="footerhead">
    <div style="background-image:url(assets/img/symphony.jpg); padding-bottom:35px;border-bottom:5px solid #e41e26;">
      <br><br>
      <a href="#turun-limit" title="Turun" class="totop"><span class="glyphicon glyphicon-circle-arrow-down" style="margin-top:3px"></span></a>
    </div>
    <!--<img src="<?php echo base_url();?>assets/img/footerhead.png" class="img-responsive" style="width:100%" alt="foothead">-->
  </div>
  </div>
</div>

<!--  KOMPETISI -->
<div id="turun-limit" syle="margin-top:100px"></div>
<div id="competition">
  <div class="container">
      <h2 class="caption">COMPETITION</h2>
      <a href="<?php echo site_url('kompetisi/invect');?>">
      <div class="col-md-4 icon">
        <img src="<?php echo base_url();?>assets/img/i-invect.png" alt="invect" class="img-responsive">
        <h2>Indie Movie Competition</h2>
        <h4>Tema : Bebas (Non Dokumenter)</h4>
        <p>Karya yang dihasilkan bertemakan bebas tapi bukan film dokumenter. Diharapkan, hasil karya ini mengandung pesan untuk yang melihatnya...</p>
        <button type="button" class="btn btn-danger btn-sm">More Info <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/fotografi');?>">
      <div class="col-md-4 icon">
        <img src="<?php echo base_url();?>assets/img/i-pc.png" alt="invect" class="img-responsive">
        <h2>Photography Contest</h2>
        <h4>Tema : Manusia dan Pembangunan</h4>
        <p>Karya yang dihasilkan menggambarkan suatu daerah tentang keadaan dampak pembangunan baik positif atau negatif....</p>
        <button type="button" class="btn btn-danger btn-sm">More Info <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/poster');?>">
      <div class="col-md-4 icon">
          <img src="<?php echo base_url();?>assets/img/i-bodi.png" alt="invect" class="img-responsive">
          <h2>Battle Of Poster Design</h2>
          <h4>Tema : Pesan untuk Tanah Airku</h4>
          <p>Visualkan hal-hal yang menurut anda baik dan memotivasi dalam bentuk poster, sehingga orang lain yang melihat akan merasa ingin...</p>
          <button type="button" class="btn btn-danger btn-sm">More Info <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
  </div>
</div>


<!--  DEWAN JURI -->

<div id="juri">
  <div class="container" style="max-width:900px">
    <div id="CarouselJuri" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#CarouselJuri" data-slide-to="0" class="active"></li>
          <li data-target="#CarouselJuri" data-slide-to="1"></li>
          <li data-target="#CarouselJuri" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px"><h2 class="text-center">JURI INDIE MOVIE COMPETITION</h2></div>
                    <a href="<?php echo site_url('juri/invect');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/triyanto.jpg" alt="juri" class="img-responsive text-center">
                      <h3>Triyanto "Genthong" Hapsoro</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/invect');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/ujel.png" alt="juri" class="img-responsive text-center">
                      <h3>Ujel Bausad</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
					         <a href="<?php echo site_url('juri/invect');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/edie.jpg" alt="juri" class="img-responsive text-center">
                      <h3>Eddie Cahyono</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px"><h2 class="text-center">JURI PHOTOGRAPHY CONTEST</h2></div>
                    <a href="<?php echo site_url('juri/fotografi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/doni.jpg" alt="juri" class="img-responsive text-center">
                      <h3>Nofria Doni F, M.Sn, A.FPSI</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/fotografi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/munir.png" alt="juri" class="img-responsive">
                      <h3>Misbachul Munir</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/fotografi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/pinto.jpg" alt="juri" class="img-responsive">
                      <h3>Nurpinto H</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>

                </div>
            </div>

            <div class="item">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px"><h2 class="text-center">JURI BATTLE OF POSTER DESIGN</h2></div>
                    <a href="<?php echo site_url('juri/bodi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/dhanank.jpg" alt="juri" class="img-responsive text-center">
                      <h3>Dhanank Pambayun</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/bodi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/ipung.jpg" alt="juri" class="img-responsive">
                      <h3>Ipung Kurniawan</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/bodi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/dedi.png" alt="juri" class="img-responsive">
                      <h3>Dedi Rokkinvisual</h3>
                      <button type="button" class="btn btn-danger btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                </div>
            </div>
      </div>
  </div>

  <!-- Sponsor
  <div id="sponsor">
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
  -->
  </div>
  </div>
</div>
  <div class="borderjuri"></div>

<!--  REGISTRASI -->
<div id="registration">
  <div class="container">
    <div class="daftar">
    <h1 style="color:#c0392b;font-weight:bold;font-size:50px">JANGAN LEWATKAN!</h1>
    <h2 style="color:#634B1E;font-weight:bold;">SHOW OFF 21-22 MEI 2016</h2>
    <h4>di Loop Station Yogyakarta</h4>(Jl. Trikora no.2, Yogyakarta)
    <p>Pameran, screening film, voting karya, hiburan, pembagian sertifikat</p>
    <h2 style="color:#634B1E;font-weight:bold;">AWARDING 30 MEI 2016</h2>
    <h4>di Taman Budaya Yogyakarta</h4>(Jl. Sriwedani No.1, Daerah Istimewa Yogyakarta, Indonesia)
    </div>
  </div>
</div>
<div class="borderred"></div>
<!--  BLOG -->
<div id="blog">
  <div class="container">
    <div class="col-md-8">
      <h1>ARSIP TEBAS</h1>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/01.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb01.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/02.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb02.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/03.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb03.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/04.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb04.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/05.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb05.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/06.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb06.jpg" alt="arsip" class="img-responsive"></a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/07.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb07.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/011.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb011.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/012.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb012.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/013.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb013.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/014.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb014.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/015.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb015.jpg" alt="arsip" class="img-responsive"></a>
        </div>

    </div>
    <div class="col-md-4 col-sm-12">
    <h1>TWITTER WIDGET</h1>
    <br>
	  <a class="twitter-timeline" href="https://twitter.com/TebasAward" data-widget-id="444397868204322816">Tweet oleh @TebasAward</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>
  </div>
</div>
<!--  VIDEO -->
<div class="videoborderatas"></div>
<div id="videos">
  <div class="container">
    <div class="col-md-3 col-xs-12 pull-right">
      <h2>NEW VIDEO</h2>
      <p>Support Official Information Video of TEBAS 2016. Please subscribe our youtube channel in <a href="" target="_blank" style="color:#fff"><b>THE BEST ANNUAL MULTIMEDIA SHOW</b></a>.</p><script src="https://apis.google.com/js/platform.js"></script>

<div class="g-ytsubscribe" data-channel="MediaVideoKoma" data-layout="default" data-count="default"></div>
    </div>
	<div class="col-md-3 col-xs-12">
	<a class='youtube' href="https://www.youtube.com/embed/YW128_bUa9I"><img src="http://img.youtube.com/vi/YW128_bUa9I/mqdefault.jpg" class="img-responsive" /></a>
      <h4>Teaser TEBAS 2016</h4>
      </a>

    </div>
    <div class="col-md-3 col-xs-12">
      <!--
      <a class='youtube' href="https://www.youtube.com/embed/CuRxuB10Byk"><img src="https://i.ytimg.com/vi_webp/CuRxuB10Byk/mqdefault.webp" class="img-responsive" />
      <h4>Video Pendaftaran TEBAS 2016</h4>
      </a>
	 -->
    </div>
    <div class="col-md-3 col-xs-12" style="visibility:hidden">
      <a class='youtube' href="#"><img src="#" class="img-responsive" />
      <h4>TEBAS 2014 Invitation</h4>
      </a>
    </div>
  </div>
</div>

<div class="videoborder"></div>

<div id="support">
  <div class="container">

      <div class="col-md-5" style="padding-bottom:10px;">
<!--
        <h2>SPONSORED BY</h2>
    <div class="col-md-6 col-sm-12 col-xs-12 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
  -->
    <div class="clearfix"></div>
      </div>
    <div class="col-md-3">

        <h2>SUPPORTED BY</h2>
    <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/amikom2.png" class="img-responsive" width="96%"></div>
        </div>
  <!--
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
-->




    <div class="clearfix"></div>
      </div>
      <div class="col-md-4">
        <h2>MEDIA PARTNER</h2>
        <div class="col-md-8 col-sm-8 col-xs-12 block">
          <div class=""><img src="assets/img/support/rbtv.png" class="img-responsive"></div>
        </div>
    <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/unisi.png" class="img-responsive"></div>
        </div>
<!--
    <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
  -->
      </div>

    <div class="clearfix"></div>
            <h2 align="center">ORGANIZED BY</h2>
        <div class="col-md-12 col-sm-12 block">
          <div class=""><img src="assets/img/support/koma.png" class="img-responsive" width="10%"></div>
        </div>

  </div>
</div>
