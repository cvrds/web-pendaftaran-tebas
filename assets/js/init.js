
$("#form-add").submit(function (e) {
//    $(".preloader-wrapper").addClass("active");
    var formData = new FormData($(this)[0]);
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        async: false,
        processData: false,
        success: function (data) {
            console.log(data);
//            $(".preloader-wrapper").removeClass("active");
            if (data.status == 1) {
                form.trigger('reset');
                toastr.success(data.message); // show response from the php script.
            }
            else {
                toastr.error(data.message); // show response from the php script.
            }
        }
    });
    e.preventDefault(); // avoid to execute the actual submit of the form.
});
