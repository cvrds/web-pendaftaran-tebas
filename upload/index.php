<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <!--JOTFORM-->
    <script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
    <script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.11871" type="text/javascript"></script>
    <script type="text/javascript">
        JotForm.init(function () {
            JotForm.clearFieldOnHide = "disable";
            JotForm.onSubmissionError = "jumpToFirstError";
        });
    </script>
    <link href="https://cdn.jotfor.ms/static/formCss.css?3.3.11871" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jotfor.ms/css/styles/nova.css?3.3.11871" />
    <link type="text/css" media="print" rel="stylesheet" href="https://cdn.jotfor.ms/css/printForm.css?3.3.11871" />
    <link type="text/css" rel="stylesheet" href="https://secure.jotform.me/themes/CSS/566a91c2977cdfcd478b4567.css?session=48100889289519" />
    <!--JOTFORM-->
    <title>UPLOAD KARYA</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/mystyle.css" />
    <link rel="stylesheet" type="text/css" href="css/AdminLTE.min.css">
    <script type="text/javascript" language="javascript">
        function alert1() {
            alert("Proses upload mungkin akan memakan waktu lama, jangan tutup halaman sebelum ada pemberitahuan selesai upload!");
        }
    </script>
</head>

<body>
    <header class="navbar navbar-inverse" role="banner">
        <div class="container">
            <div class="navbar-header" style="width:100%"> <a href="http://tebasaward.com/" class="branding">TEBAS AWARD</a>
                <div style="float:right;font-style: italic;  padding-top: 7px;" class="sub-brand"><a href="" class="branding" style="font-size: 25px;">RAGAM MEMORY KREATIVITAS</a></div>
            </div>
        </div>
    </header>
    <div class="container-fluid content">
        <div class="col-md-5">
            <!-- general form elements disabled -->
            <div class="box box-warning">
                <div class="box-header">
                    <h1 class="">Cara Pengumpulan Karya</h1> </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ol>
                        <li>Silahkan Login Akun TEBAS dan pilih menu profil --&gt; upload karya</li>
                        <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
                        <li>Pilih kategori lomba</li>
                        <li>File berformat .zip yang berisi karya, <i><b>file *.doc</b></i>, scan kartu identitas, scan surat pernyataan, scan bukti transaksi. </li>
                        <li><b>File *.doc</b> berisi nama lengkap, judul dan deskripsi karya.</li>
                        <li>Ukuran file .rar/.zip max 6MB</li>
                    </ol>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <div class="box box-warning">
                <div class="box-header">
                    <h1 class="">Cara Pengumpulan Karya</h1> </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ol>
                        <li>Silahkan Login Akun TEBAS dan pilih menu profil --&gt; upload karya</li>
                        <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
                        <li>Pilih kategori lomba</li>
                        <li>File berformat .zip yang berisi karya, <i><b>file *.doc</b></i>, scan kartu identitas, scan surat pernyataan, scan bukti transaksi. </li>
                        <li><b>File *.doc</b> berisi nama lengkap, judul dan deskripsi karya.</li>
                        <li>Ukuran file .rar/.zip max 6MB</li>
                    </ol>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-5">
            <!-- general form elements -->
            <div class="box box-primary">
                <table align="center">
                    <form class="jotform-form" action="https://submit.jotform.me/submit/60647877230461/" method="post" enctype="multipart/form-data" name="form_60647877230461" id="60647877230461" accept-charset="utf-8">
                        <input type="hidden" name="formID" value="60647877230461" />
                        <div class="form-all">
                            <ul class="form-section page-section">
                                <li id="cid_1" class="form-input-wide" data-type="control_head">
                                    <div class="form-header-group">
                                        <div class="header-text httal htvam">
                                            <h2 id="header_1" class="form-header">
              FORM UPLOAD KARYA PHOTOGRAPHY DAN BODI
            </h2> </div>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_textbox" id="id_3">
                                    <label class="form-label form-label-left form-label-auto" id="label_3" for="input_3"> No.Pendaftaran </label>
                                    <div id="cid_3" class="form-input jf-required">
                                        <input type="text" class=" form-textbox" data-type="input-textbox" id="input_3" name="q3_nopendaftaran" size="20" value="" required /> </div>
                                </li>
                                <li class="form-line" data-type="control_textbox" id="id_4">
                                    <label class="form-label form-label-left form-label-auto" id="label_4" for="input_4"> Nama </label>
                                    <div id="cid_4" class="form-input jf-required">
                                        <input type="text" class=" form-textbox" data-type="input-textbox" id="input_4" name="q4_nama" size="20" value="" required/> </div>
                                </li>
                                <li class="form-line" data-type="control_dropdown" id="id_5">
                                    <label class="form-label form-label-left form-label-auto" id="label_5" for="input_5"> Karya </label>
                                    <div id="cid_5" class="form-input jf-required">
                                        <select class="form-dropdown" style="width:150px" id="input_5" name="q5_lomba">
                                            <option value=""> </option>
                                            <option value="PHOTOGRAPHY CONTEST"> PHOTOGRAPHY CONTEST </option>
                                            <option value="BATTLE OF POSTER DESIGN"> BATTLE OF POSTER DESIGN </option>                           <option value="INVECT"> BATTLE OF POSTER DESIGN </option>

                                        </select>
                                    </div>
                                </li>
                                <li class="form-line jf-required" data-type="control_fileupload" id="id_6">
                                    <label class="form-label form-label-left form-label-auto" id="label_6" for="input_6"> Upload Karya <small>(format .rar/.zip & max 6MB)</small> <span class="form-required">
            *
          </span> </label>
                                    <div id="cid_6" class="form-input jf-required">
                                        <input class="form-upload validate[required]" type="file" id="input_6" name="q6_karyaDalam6" file-accept="zip, rar" file-maxsize="6024" file-minsize="0" file-limit="0" /> </div>
                                </li>
                                <li class="form-line" data-type="control_button" id="id_2">
                                    <div id="cid_2" class="form-input-wide">
                                        <div style="margin-left:156px" class="form-buttons-wrapper">
                                            <button id="input_2" type="submit" class="form-submit-button"> Kirim </button>
                                        </div>
                                    </div>
                                </li>
                                <li style="display:none"> Should be Empty:
                                    <input type="text" name="website" value="" /> </li>
                            </ul>
                        </div>
                        <input type="hidden" id="simple_spc" name="simple_spc" value="60647877230461" />
                        <script type="text/javascript">
                            document.getElementById("si" + "mple" + "_spc").value = "60647877230461-60647877230461";
                        </script>
                    </form>
                    <script type="text/javascript">
                        JotForm.ownerView = true;
                    </script>
                </table>
            </div>
        </div>
        <div class="col-md-5">
            <!-- general form elements -->
            <div class="box box-primary">
                <table align="center">
                    <form>
                        <div class="form-all">
                            <ul class="form-section page-section">
                                <li class="form-input-wide" data-type="control_head">
                                    <div class="form-header-group">
                                        <div class="header-text httal htvam">
                                            <h2 class="form-header">
                               FORM UPLOAD KARYA INVECT
                              </h2> </div>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_textbox">
                                    <label class="form-label form-label-left form-label-auto"> No.Pendaftaran </label>
                                    <div class="form-input ">
                                        <input type="text" class=" form-textbox" data-type="input-textbox" size="20" value="" required /> </div>
                                </li>
                                <li class="form-line" data-type="control_textbox">
                                    <label class="form-label form-label-left form-label-auto"> Nama </label>
                                    <div class="form-input ">
                                        <input type="text" class=" form-textbox" data-type="input-textbox" size="20" value="" required/> </div>
                                </li>
                                <li class="form-line" data-type="control_dropdown">
                                    <label class="form-label form-label-left form-label-auto"> Karya </label>
                                    <div class="form-input">
                                        <select class="form-dropdown" style="width:150px">
                                            <option value=""> </option>
                                            <option value="INDIE MOVIE COMPETITION"> INDIE MOVIE COMPETITION </option>
                                        </select>
                                    </div>
                                </li>
                                <li class="form-line" data-type="control_textbox">
                                    <label class="form-label form-label-left form-label-auto"> Link Film di Yutube </label>
                                    <div class="form-input ">
                                        <input type="text" class=" form-textbox" data-type="input-textbox" size="20" value="" required/> </div>
                                </li>
                                <li class="form-line" data-type="control_button">
                                    <div class="form-input-wide">
                                        <div style="margin-left:156px" class="form-buttons-wrapper">
                                            <button type="submit" class="form-submit-button"> Kirim </button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </form>
                </table>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="upload.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $('#inputINVECT').hide();
        $(function () {
            $("#pilihKarya").change(function () {
                if ($(this).val() == "INVECT") {
                    $("#inputINVECT").show();
                }
                else {
                    $("#inputINVECT").hide();
                }
            });
        });
    </script>
</body>

</html>
