-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 18 Apr 2015 pada 23.22
-- Versi Server: 5.5.41-cll-lve
-- Versi PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `komaorid_tebasaward`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_admin`
--

CREATE TABLE IF NOT EXISTS `t_admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `t_admin`
--

INSERT INTO `t_admin` (`id_admin`, `username`, `password`) VALUES
(1, 'tebas2015', 'b2c420e70bedf16d456a295eb9050082');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_peserta_bodi`
--

CREATE TABLE IF NOT EXISTS `t_peserta_bodi` (
  `id_peserta_bodi` int(11) NOT NULL AUTO_INCREMENT,
  `id_registrasi` int(11) NOT NULL,
  `no_pendf` varchar(50) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `bayar` varchar(50) NOT NULL,
  `berkas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_peserta_bodi`),
  KEY `id_registrasi` (`id_registrasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `t_peserta_bodi`
--

INSERT INTO `t_peserta_bodi` (`id_peserta_bodi`, `id_registrasi`, `no_pendf`, `judul`, `deskripsi`, `bayar`, `berkas`) VALUES
(9, 53, 'BODI009', 'LOGO PERUSAHAAN NATURAL', 'MENGGAMBARKAN KARYA DESAIN GRAFIS YANG ELEGAN DITUJUKAN UNTUK PERUSAAHAN YANG MEMBUTUHKAN LOGO ', 'belum', 'belum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_peserta_fotografi`
--

CREATE TABLE IF NOT EXISTS `t_peserta_fotografi` (
  `id_peserta_fotografi` int(11) NOT NULL AUTO_INCREMENT,
  `id_registrasi` int(11) NOT NULL,
  `no_pendf` varchar(50) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `bayar` varchar(50) NOT NULL,
  `berkas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_peserta_fotografi`),
  KEY `id_registrasi` (`id_registrasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `t_peserta_fotografi`
--

INSERT INTO `t_peserta_fotografi` (`id_peserta_fotografi`, `id_registrasi`, `no_pendf`, `judul`, `deskripsi`, `bayar`, `berkas`) VALUES
(13, 35, 'PC013', 'Loh..!! Mendunge peteng nak.', 'ibu yang menggendong anak, sedang berada ditengah\nalun-alun utara Jogja, saat mendung tebal menyelimuti langit Yogyakarta.', 'sudah', 'sudah'),
(14, 35, 'PC014', 'tetep rame', 'Masyarakat tetap meramaikan acara kirab budaya yang berada di Kraton Yogyakarta di acara Jogja Gumregah. meskipun mendung menyelimuti langit Yogyakarta.', 'sudah', 'sudah'),
(15, 51, 'PC015', 'indahnya alamku ', 'keindahan yang dimiliki indonesia yang berada didaerah jogjakarta', 'belum', 'belum'),
(16, 69, 'PC016', 'Jayapura hongkong ke 2', 'Kota yang di katakan sebagai Hongkong ke 2 yang mempunyai keindahan alamnya', 'belum', 'belum'),
(17, 69, 'PC017', 'Jayapura is my Town ', 'cendrawasih city\n', 'belum', 'belum'),
(18, 16, 'PC018', 'Kesetian Abdi Dalem', 'Sebuah Kesetian abdi dalem kepada sultan di kraton yogyakarta', 'belum', 'belum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_peserta_invect`
--

CREATE TABLE IF NOT EXISTS `t_peserta_invect` (
  `id_peserta_invect` int(11) NOT NULL AUTO_INCREMENT,
  `id_registrasi` int(11) NOT NULL,
  `no_pendf` varchar(50) NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `genre` varchar(100) NOT NULL,
  `bahasa` varchar(100) NOT NULL,
  `durasi` varchar(50) NOT NULL,
  `penonton` varchar(50) NOT NULL,
  `tahun` int(11) NOT NULL,
  `tpt_produksi` varchar(100) NOT NULL,
  `produser` varchar(100) NOT NULL,
  `sutradara` varchar(100) NOT NULL,
  `naskah` varchar(100) NOT NULL,
  `editor` varchar(100) NOT NULL,
  `pemeran` varchar(200) NOT NULL,
  `ditayangkan` varchar(10) NOT NULL,
  `karya_pertama` varchar(10) NOT NULL,
  `film1` varchar(100) NOT NULL,
  `tahun1` int(11) NOT NULL,
  `film2` varchar(100) NOT NULL,
  `tahun2` int(11) NOT NULL,
  `film3` varchar(100) NOT NULL,
  `tahun3` int(11) NOT NULL,
  `kompetisi1` varchar(200) NOT NULL,
  `kompetisi2` varchar(200) NOT NULL,
  `kompetisi3` varchar(200) NOT NULL,
  `prestasi1` varchar(200) NOT NULL,
  `prestasi2` varchar(200) NOT NULL,
  `prestasi3` varchar(200) NOT NULL,
  `backsound` text NOT NULL,
  `sinopsis` text NOT NULL,
  `bayar` varchar(50) NOT NULL,
  `berkas` varchar(50) NOT NULL,
  PRIMARY KEY (`id_peserta_invect`),
  KEY `id_registrasi` (`id_registrasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `t_peserta_invect`
--

INSERT INTO `t_peserta_invect` (`id_peserta_invect`, `id_registrasi`, `no_pendf`, `kategori`, `judul`, `genre`, `bahasa`, `durasi`, `penonton`, `tahun`, `tpt_produksi`, `produser`, `sutradara`, `naskah`, `editor`, `pemeran`, `ditayangkan`, `karya_pertama`, `film1`, `tahun1`, `film2`, `tahun2`, `film3`, `tahun3`, `kompetisi1`, `kompetisi2`, `kompetisi3`, `prestasi1`, `prestasi2`, `prestasi3`, `backsound`, `sinopsis`, `bayar`, `berkas`) VALUES
(2, 18, 'INV002', 'umum', 'Tanya Satu Atap', 'Drama', 'Indonesia', '17:13', 'semua', 2015, 'Yogyakarta', 'Saraswati Dara', 'M. Rizky Kurnia', 'Afif Reza', 'Ghifari Albar', 'Akas Dwi Jatmiko', 'pernah', 'tidak', 'Wake-Up', 2013, 'Harapan vs Kenyataan', 2013, 'Niat', 2013, 'Malang Film Festival', 'Lampung Film Festival', 'Eroupa Movie Screning', '', '', '', '1. Dan Diam - Layur\n2. 03:00 Am Ruins\n3. Float, Silence (Reprise)', 'Iwan Tidak pernah tau dimana keberadaan ibunya, dan ayahnya Dahlan tidak pernah memberi tau dimana ibunya. Iwan terus bertengkar dengan Dahlan akibat persoalan keberadaan dimana sosok Ibu yang sebenarnya.', 'sudah', 'sudah'),
(3, 32, 'INV003', 'umum', 'Dalam Kurung', 'Drama', 'Indonesia', '05:00', 'semua', 2014, 'Jakarta/Indonesia', 'Rian Riyadi/Afriani Respati', 'Rian Riyadi/Afriani Respati', 'Andriani Nova', 'Risky Anugrah', 'Kharina Putri Pratiwi', 'pernah', 'tidak', 'LAWAN! Bebaskan Perempuan Dari Kekerasan Seksual', 2014, 'Cinta Pohon', 2015, 'Bebas', 2015, 'Festival Film Pendek Plan Indonesia, Piala Kementrian Pemberdayaan Perempuan dan Perlindungan Anak 2014', 'Kompetisi Film Pendek CommCreation Universitas Bakrie 2015', 'European On Screen Film Festival 2015', 'Juara 1 Kategori Umum Festival Film Pendek Plan Indonesia Piala Kementrian Pemberdayaan Perempuan dan Perlindungan Anak 2014', 'Pemeran Utama Terbaik Kompetisi Film Pendek CommCreation Universitas Bakrie 2015', '', 'Remain Of The Day, Sad Memory (Yiruma), Sometimes Someone 2 (Yiruma)', 'Sekar, pelajar perempuan berusia 15 tahun tidak dapat memilih dilahirkan sebagai seorang perempuan. Setelah beranjak dewasa, Sekar menyadari tubuhnya adalah investasi. Sekar melihat sekeliling banyak yang seperti dirinya. Tapi Sekar akan melawan. Sekar ingin tubuhnya menjadi miliknya sendiri. ', 'belum', 'belum'),
(4, 33, 'INV004', 'umum', 'Manuk', 'Drama', 'Jawa', '17:39', 'semua', 2015, 'Yogyakarta, Indonesia', 'Fitriana Ambarwati', 'Ghalif Putra Sadewa', 'Fitriana Lestari', 'Mohamad Adhyaksa', 'Eni Mijil Sarjono', 'pernah', 'tidak', 'Hempas', 2014, 'Aminah', 2013, 'Sop Merah', 2014, '', '', '', '', '', '', '', 'Sumi dan Darto adalah pasangan suami istri yang sudah menikah selama lima tahun, tetapi belum mempunyai anak. Sumi sebagai ibu rumah tangga dan Darto bekerja sebagai pegawai keluarahan dan sangat hobi memelihara burung yang kemudian membuat Sumi cemburu.', 'belum', 'belum'),
(5, 34, 'INV005', 'pelajar', 'Tak Terlihat', 'Komedi Horror', 'Indonesia', '13:31', 'semua', 2013, 'Sukoharjo', 'Farid H', 'Wilman Alfarizy', 'Ghoris Arkan', 'Yunizar', 'Ghoris Arkan', 'pernah', 'tidak', 'sketsa konsulat', 2012, '', 0, '', 0, 'XXI Movie Competition', '', '', '', '', '', 'X ray dog', 'seorang anak yang memoliki dendam terpendam.....', 'belum', 'belum'),
(7, 33, 'INV007', 'umum', 'Hempas', 'Drama-Action', 'Indonesia', '14:47', 'semua', 2014, 'Yogyakarta, Indonesia', 'Dewi Puspita Sari', 'Ghalif Putra Sadewa', 'Tiara Sekar Ayu', 'Handri Saputra', 'Adam Maulana Y.', 'pernah', 'tidak', 'Manuk', 2015, 'Aminah', 2013, 'Sop Merah', 2014, 'Festival Film Creabo 2104', '', '', 'Official Selection', '', '', '', 'Arya, seorang laki laki lulusan strata satu pengangguran yang harus menghidupi dan merawat adiknya yang sedang sakit, Apit. Suatu pagi Arya terbangun karena sang adik kambuh dari sakitnya, dalam keadaan panik,  obat untuk sang adik telah habis dan Arya harus mendapatkan obat untuk adiknya.', 'belum', 'belum'),
(8, 49, 'INV008', 'umum', 'SP', 'Drama', 'Indonesia', '19:00', 'semua', 2015, 'Lampung, Indonesia', 'Ahmad Suhardi', 'Sirojuddin', 'Ferdy, Sirojuddin,Ahmad, Zainal', 'Sirojuddin', 'Zainal', 'belum', 'tidak', 'Jatisari First Blood Reloaded', 2012, '', 0, '', 0, '', '', '', '', '', '', 'The Beginning\nImmortality\nhttps://www.youtube.com/audiolibrary/music', 'John dan Jack yang sedari kecil berteman akrab tiba-tiba saling berjauhan karena Jack selalu asyik sendiri dengan semartphone barunya. Hingga suatu hari Jack tidak bisa menghubungi Jhon, dan jack pun tak menemukan jhon di rumahnya. Apa yang sedang terjadi?', 'belum', 'belum'),
(9, 50, 'INV009', 'umum', 'LYN', 'Drama', 'Jawa-Indonesia', '09:05', 'semua', 2015, 'Yogyakarta', 'Andi Budrah', 'M Reza Fahriyansyah', 'M Reza Fahriyansyah', 'M Reza Fahriyansyah', 'Eka Wahyu Primadani', 'belum', 'tidak', 'The Crazy One!!!', 2013, 'Ayam Arul', 2014, 'Autism', 2012, 'belum', 'belum', 'belum', 'official Selection Pesta Film Airlangga Surabaya', 'belum', 'belum', '-', 'Situasi di dalam angkot yang tidak pernah kita kira dan akan bertemu orang yang seperti apa', 'belum', 'belum'),
(10, 58, 'INV010', 'umum', 'Rejung', 'Drama', 'Indonesia', '19:06', 'semua', 2014, 'Baturaja', 'Videografi Unsri', 'Husain', 'Melinda Azhari', 'Kurnia Magcia', 'M. Ilham Prakoso', 'pernah', 'tidak', 'Today is Apes', 2012, 'Membeli Senyuman', 2013, '', 0, 'Sumsel Art Festival 2014', '', '', '', '', '', '', 'aku adalah seorang wanita yang kuat, mampu menepis angin untuk mempertahankan nyala apiku. Tapi tenyata aku lemah dengan keadaan ! disaat tiupan angin kecil mencoba menghabiskan nyala apiku,  yang aku pilih hanyalah mematikan diriku sendiri dan bukan memilih untuk memperjuangkannya hidup kembali.', 'belum', 'belum'),
(11, 59, 'INV011', 'umum', 'Rumah Batik', 'Drama', 'Indonesia', '12:41', 'semua', 2015, 'Indonesia', 'Mohammad Tritaufan Saputra', 'Raka Mahandhika', 'Raka Mahandhika', 'Baruna Setya Pratama', 'Aldy (Ateng)', 'pernah', 'tidak', 'Semangat Membuka Jendela Dunia', 2013, 'Dendam Arwah Sekolah', 2014, 'Lika-Liku Joki 3 in 1', 2015, 'AKUSO 2013 (Kemkominfo)', 'Creative Video Festival UIN', 'Alsease 2015', 'Juara Harapan 2 AKUSO 2013', 'Juara 2 Creative Video Festival UIN', 'Juara 2 Alsease 2015', 'Original Soundtrack Rumah Batik \n"Tanah Air - MUTRSaputra"', 'Film ini menceritakan tentang tiga orang pemuda yang memiliki jiwa nasionalisme tinggi sehingga mereka sangat memperjuangkan budaya asli Indonesia, yaitu Batik hingga terciptalah sebuah "Rumah Batik".', 'belum', 'belum'),
(12, 61, 'INV012', 'pelajar', 'BUKU RAGIL', 'FIKSI', 'JAWA', '10.00', 'semua', 2015, 'PATI JAWA TENGAH', 'sukis cinematography', 'adi saputra', 'adi saputra', 'reza wardana / andri kamarullah / adi saputra', 'RAGIL', 'pernah', 'tidak', 'MENCARI TERANG', 2012, '', 0, '', 0, 'FFPJ', '', '', 'JUARA 3 FFPJ', '', '', 'SULUK (KI ANOM SUROTO)\nBUBUY BULAN (JUBING K)\nFREEBACKSUOND.COM', 'Ragil, anak Desa yang memiliki keinginan untuk menjadi Dalang seperti Kakeknya. Ragil anak kecil yang selalu tak pernah lepas membawa buku peninggalan kakeknya yang berisi tata cara menjadi Dalang.', 'belum', 'belum'),
(13, 66, 'INV013', 'umum', 'ALIN', 'Drama', 'Indonesia - Palembang', '08:20', 'semua', 2014, 'Indralaya ', 'Videografiunsri', 'Rifki Zarkasih', 'Idandi Meida Jovanka', 'Rifqi Mardhani', 'Mia', 'pernah', 'tidak', 'Aku Ingin Sekolah', 2013, '', 0, '', 0, '', '', '', '', '', '', '', 'Alin lahir dari keturunan tionghoa, dia dilahirkan dan dibesarkan di kota Palembang. Alin adalah salah satu dari sekian banyak warga keturunan etnis yang ingin ikut serta dalam mengembangkan budaya daerah pada saat itu, akan tetapi  peraturan dan permainan politik pada zaman itu.', 'belum', 'belum'),
(14, 66, 'INV014', 'umum', 'Aku Ingin Sekolah', 'Drama', 'Indonesia', '13:41', 'semua', 2013, 'Indralaya', 'Videografiunsri', 'Rifki Zarkasih', 'Rima Rahmatika', 'Rifki Zarkasih', 'Fitrah Ilhamsyah', 'pernah', 'ya', 'Alin', 2014, '', 0, '', 0, 'Sumsel Gemilang Festival', '', '', '', '', '', '', 'Dodi adalah Sosok seorang pemuda yang memiliki keinginan kuat untuk memperoleh dan mengikuti jenjang pendidikan. Merupakan hal yang tak mungkin didapati oleh lelaki penuh semangat ini di sebabkan keadaan ekonomi keluarganya.', 'belum', 'belum'),
(16, 72, 'INV016', 'pelajar', 'THE IMPOSSIBLE OF DREAM', 'Fantasi', 'Indonesia', '20:00', 'semua', 2013, 'Lhokseumawe', 'Mustafa Kamal', 'Muhammad Arifandi', 'Muhammad Arifandi', 'Mustafa Kamal', 'Muhammad Iqbal', 'belum', 'tidak', '', 0, '', 0, '', 0, '', '', '', '', '', '', '', 'Rizi selalu dihantui oleh mimpi yang di anggapnya tidak nyata, akan tetapi rizi setengah percaya bahwa mimpi itu nyata. dunia mimpi itu menariknya dan rizi tidak menginginkan terus ada di dunia mimpinya. Rizi berusahan keluar dari mimpinya dengan cara dia sendiri.', 'belum', 'belum'),
(17, 73, 'INV017', 'pelajar', 'SELANGKAH', 'KOMEDI SATIR', 'INDONESIA', '05:00', '+17', 2015, 'JAKARTA / INDONESIA', 'JODY SURENDRA', 'RHEZA PRAMUDITA', 'RHEZA PRAMUDITA', 'IQBAL NURUL HUDA', 'FAISAL YUTAKA', 'pernah', 'tidak', 'DIMANA 17KU', 2014, 'AFTER EFFECT', 2014, '3 TPTL 2', 2009, 'UI IDEA FESTIVAL', 'FIFGROUP SHORT MOVIE COMPETITION', 'PEKAN KOMUNIKASI CREABO ', 'JUARA 3 DI UI IDEA FESTIVAL 2015', 'OFFICIAL SELECTION FIFGROUP SHORT MOVIE 2014', '', '', 'SEORANG PRIA YANG BARU SAJA LULUS KULIAH DAN HARUS BERHADAPAN DENGAN BANYAK TEKANAN ', 'belum', 'belum'),
(18, 86, 'INV018', 'umum', 'PERISAI HIDUP', 'ACTION/ LAGA', 'BAHASA INDONESIA', '15:00', '+17', 2014, 'JAKARTA', 'DIMAS R. AGUSTIAN', 'DIMAS R. AGUSTIAN', 'RIYAN TAUFIK RACHMAN', 'DIMAS R. AGUSTIAN', 'DIMAS R. AGUSTIAN', 'pernah', 'tidak', 'PRO (PROFESIONAL)', 2013, '', 0, '', 0, 'JOGJA SHORT FILM FESTIVAL 2014', '', '', 'BELUM ADA', '', '', 'PRO SCORES', 'Bima seorang polisi intelejen anti narkoba berusaha menyelamatkan putrinya dari tangan para mafia narkoba buronannya.', 'belum', 'belum'),
(19, 87, 'INV019', 'umum', 'Setegar Karang', 'Drama', 'Indonesia, Aceh', '15:17', 'semua', 2014, 'Banda Aceh/Aceh/Indonesia', 'Beni Arona', 'Beni Arona', 'Beni Arona', 'Mirza Anggara', 'Saifil Munandar', 'belum', 'tidak', 'Cerita Alam dari Pesisir', 2012, 'Berbeda (Itu) Nasib', 2013, 'Setegar Karang', 2014, 'Festival Video Edukasi 2013', 'Festival Film Indie Jogja 2013', 'Festival Video Edukasi 2014', 'Juara 1 Festival Video Edukasi 2013/ Film Berbeda (Itu) Nasib', 'Juara 5 Festival Film Indie Jogja 2013/ Film Berbeda (Itu) Nasib', 'Juara 2 Festival Video Edukasi 2014/ Film Setegar Karang', '', 'Setegar Karang menceritakan tentang kehidupan anak-anak di Pulau Sabang dalam melindungi lingkungan, terumbu karang dan meningkatkan daya tarik wisata di daerahnya. ', 'belum', 'belum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_registrasi`
--

CREATE TABLE IF NOT EXISTS `t_registrasi` (
  `id_registrasi` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jk` varchar(20) NOT NULL,
  `tpt_lahir` varchar(50) NOT NULL,
  `tgl_lahir` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `telp1` varchar(20) NOT NULL,
  `telp2` varchar(20) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `tgl_registrasi` datetime NOT NULL,
  `status_lomba` int(11) NOT NULL,
  PRIMARY KEY (`id_registrasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data untuk tabel `t_registrasi`
--

INSERT INTO `t_registrasi` (`id_registrasi`, `email`, `password`, `nama`, `jk`, `tpt_lahir`, `tgl_lahir`, `alamat`, `instansi`, `telp1`, `telp2`, `twitter`, `tgl_registrasi`, `status_lomba`) VALUES
(6, 'rahmadds94@gmail.com', '3fc0a7acf087f549ac2b266baf94b8b1', 'RAHMAD DIDIK SHOFFYANTO', 'Pria', 'BANTUL', '23/01/1994', 'BLALI SELOHARJO PUNDONG BANTUL YOGYAKARTA 55771', 'STMIK AMIKOM Yogyakarta', '08562556762', '', '@didik_onlymedia', '2015-03-25 18:34:19', 0),
(7, 'yashicaamalia@gmail.com', '5c2105fe56f79b3b627be9fd708a7a05', 'Yashica Amalia', 'Wanita', 'Tenggarong', '13/06/97', 'Jl. Stadion Gg. Permai No. 123 RT. 14 Loa Ipuh, Tenggarong, Kutai Kartanegara, Kalimantan Timur', 'SMKN 1 Tenggarong', '082254342786', '085652228613', '@yashicaamalia', '2015-03-25 21:13:51', 0),
(8, 'imamsyafii22041997@gmail.com', 'e23bc34ea86863c5c1cea9965aa74bf1', 'Imam syafi''i', 'Pria', 'klaten', '22/04/1997', 'Trono tempursari ngawen,klaten,jateng', 'Smk N 1 klaten ', '085729288347', '', '@imamsyaf11', '2015-03-26 02:01:36', 0),
(10, 'rihamvanahmad@gmail.com', '977712c6538b3bb66792b7281878ac17', 'MUHAMMAD SHIFAUR RIHAM', 'Pria', 'PATI', '02/03/1996', 'JL. KUSUMANEGARA, KEC. UMBULHARJO, KEL. MUJA MUJU, JL. KERTO NO. 19, YOGYAKARTA 55165 - DIY', 'AKADEMI KOMUNIKASI RADYA BINATAMA', '085772086596', '082337673169', '@rihamms1', '2015-03-26 09:40:52', 0),
(11, 'somaerde@gmail.com', '474a2d255b05fe6361e411551f28e3ce', 'Soma Romadona', 'Pria', 'Tegal', '22/03/1993', 'Jl.Gejayan CTX Gg.Hortensia No.6a RT02/RW04, Catur Tunggal, Depok Sleman,DIY', 'STMIK AMIKOM YOGYAKARTA', '085600343354', '', '@zomao', '2015-03-26 18:49:05', 0),
(12, 'ichamicimici@gmail.com', 'a22e85bcb4fbd85e6dfa89af75ea3633', 'chandra cloudia zein', 'Wanita', 'kebumen', '16/08/95', 'Jl. Ampera No. 10 Rt. 02/06,  kebumen, jawa tengah 54311', '', '089677166877', '085701215320', '@_ichamici', '2015-03-26 19:11:51', 0),
(14, 'giansiddiqw@gmail.com', 'a81021055da51261fdd5deaf2f6d61cd', 'Gian Siddiq Wicaksono', 'Pria', 'Tanjung Balai Karimun', '28/09/1996', 'Jalan Singa VI No. 17 RT/RW 02/06, Kalicari, Pedurungan, Semarang', '', '085725022499', '082314889898', '@GianSWMbahe', '2015-03-27 01:22:00', 0),
(15, 'muchamaddarmawan06@gmail.com', '3438f26dd264b925080dac5cef468fa7', 'Muchamad Darmawan', 'Pria', 'Serang', '04/12/1994', 'Kuncen II RT 006 RW 007 Seloboro,Salam,Magelang', 'STMIK AMIKOM', '089617672603', '089617672603', '@mawan_dar_dar', '2015-03-27 02:23:34', 0),
(16, 'nanda_jr_cip@yahoo.co.id', 'ac43724f16e9241d990427ab7c8f4228', 'satria nanda isnaini sani', 'Pria', 'tanah grogot kalimantan tinur', '03/06/1996', 'Jalan ahmad yani tanah grogot', 'Stimik Amikom', '082255648833', '', '@segilananda', '2015-03-27 05:06:02', 0),
(17, 'annas.taufik13@gmail.com', '2d980cbb5ac1ed0944d882177cf6eb35', 'Annas Taufik R.', 'Pria', 'Samarinda', '20/19/1998', 'Jln. Kakap No. 06 Rapak Mahang', 'SMKN 1 Tenggarong', '082172247749', '', '', '2015-03-27 05:44:53', 0),
(18, 'rizky_mrk12@ymail.com', '8a615029943cc2b7df52dd37a015f947', 'M. Rizky Kurnia', 'Pria', 'Bekasi', '03/04/1994', 'Perum. Alinda Kencana 2 Blok C4 No. 24 RT 7 RW 27 Kel. Kaliabang Tengah Kec.Bekasi Utara Kota Bekasi', 'Institut Seni Indonesia Yogyakarta', '0857824242968', '089649571042', '@rizkymrk12', '2015-03-29 08:35:46', 0),
(19, 'jovinuzz.leaf@gmail.com', '1e5b7384f759707fe172a680341272cd', 'Dhanny Prasetya', 'Pria', 'Kediri', '14/04/1985', 'Jalan Semampir 1/100\nKediri - Jawa Timur 64121', '', '0818301504', '', '@jovinuzz', '2015-03-29 10:53:36', 0),
(20, 'nareswarifidelis@gmail.com', '801a774b5d00f69ab96ac751798b46a6', 'Fidelis Dhayu', 'Wanita', 'Sleman', '17/04/1993', 'Brayut, Tanjung, Wukirsari, Cangkringan, Sleman, Yogyakarta 55583', 'Universitas Atma Jaya Yogyakarta', '081904111614', '085643005539', '@everyDhay', '2015-03-29 11:36:26', 0),
(21, 'kharis.fatkhurrochman@gmail.com', 'b42fd76d7a4f8f69e95e35accb7e0b54', 'Kharis Fatkhurrochman', 'Pria', 'Membalong', '03/03/1994', 'Jl. Kaswari no.76, RT 03/15. Mancasan Lor, Condong Catur, Depok, Sleman', 'STMIK AMIKOM Yogyakarta', '85758898176', '', '@kharis_fatkhur', '2015-03-29 17:59:18', 0),
(22, 'prihantoraharjo98@gmail.com', 'b754cdebfa34dc9c42947f9d67b4f4f4', 'Prihanto Raharjo', 'Pria', 'KLATEN', '15/11/1998', 'brangkali, sukorini, manisrenggo, klaten, jawa tengah', 'SMK N 1 KLATEN', '085643868337', '', '@Prihanto_R', '2015-03-30 05:05:30', 0),
(23, 'j.hany1507@gmail.com', '998a5197559d49bb9f963626fcc29571', 'Jasmine', 'Wanita', 'Bekasi', '15/07/1999', 'Jl zambrud m2/7 bekasi timur', 'SMAN 1 BEKASI', '08118407007', '', '@jaskawai', '2015-03-30 08:17:17', 0),
(24, 'adnanmajid14@gmail.com', 'e48a4413fe5b76bd5369cae7ec3b61ad', 'Adnam Majid', 'Pria', 'Karawang', '22/04/1996', 'Desa Dawuan Tengah kec. Cikampek Kab. Karawang', '', '08978767396', '085692562278', '@adnanmjd', '2015-03-30 11:36:33', 0),
(25, 'syaifulrangga@gmail.com', 'e00411020bc9f6a147cb77a9cfe780e5', 'Rangga Syaiful Agustin', 'Pria', 'Boyolali', '09/08/1995', 'Dero, Rt 01 / Rw 14, Condong Catur, Depok, Sleman Yogyakarta', 'STMIK AMIKOM YOGYAKARTA', '089508713513', '', '@syaifulrangga', '2015-03-30 14:50:26', 0),
(26, 'raqkafauzi@gmail.com', 'c635e2c8fa15985b09386db4daab1ec2', 'Raqka Fauzi R', 'Pria', 'Jember', '24/07/1995', 'MACANAN DN 3/379 BAUSASRAN, DANUREJAN - YOGYAKARTA', '', '089672683638', '088216476554', '@raka_95', '2015-03-30 21:12:53', 0),
(27, 'dennysriopamungkas@gmail.com', '003ae6cbc2318650aed33239a8a8d127', 'dennys rio pamungkas', 'Pria', 'brebes', '25/05/1994', 'Jl.soragan ngestiharjo no 177 bantul yogyakarta', '', '085725583581', '085729533376', '@dennyspamungkas', '2015-03-31 00:55:45', 0),
(28, 'agungboiler11@gmail.com', 'ff6f4a9cd3a4b03cdc841f2e6ab55fcf', 'agung ', 'Pria', 'temanggung 06071994', '31/03/2015', 'Jln nuri r.6 perum sidoarum blok 3 godean sleman diy', 'smkn 1 godean', '085700000904', '085700000904', '', '2015-03-31 01:08:34', 0),
(29, 'adhitantons@gmail.com', '9f05aa4202e4ce8d6a72511dc735cce9', 'Adhitanto Nabil Shobirin', 'Pria', 'Jakarta', '29/03/1998', 'Pondok Pucung Indah 1 Jalan Merak D4 No. 77 Pondok Aren 15229 Tangsel, Banten', 'SMA Negeri 87 Jakarta', '08118829788', '082114841417', '@namagueitu', '2015-03-31 09:21:23', 0),
(30, 'bariparamarta@yahoo.com', 'a6892fd12e18477c871bdd6c4cc0b0d7', 'Bari Paramarta Islam', 'Pria', 'Tasikmalaya', '08/08/1984', 'Tawangsari, Kadilanggon, Wedi, Klaten', '', '087734988848', '', '@paramartabari', '2015-03-31 21:40:51', 0),
(31, 'yosua.bhima@yahoo.co.id', '9a7d4026cc2de23c35cc6246a8dd49a9', 'yosuabhima', 'Pria', 'semarang', '13/08/1995', 'Borobudur utara IV no 6 semarang', '', '085727127571', '0247609362', '@yosuabhima', '2015-04-01 01:23:40', 0),
(32, 'rianrestie.riyadi@gmail.com', 'f3d0af62080ea083d32aa12ddd1ad03e', 'Rian Riyadi', 'Wanita', 'Jakarta', '05/12/1994', 'Jalan H. Mawi No. 31 Blok B RT 04/05 Desa Waru, Parung, Bogor, Jawa Barat', '', '085718029808', '085717504432', '@riyadiirian', '2015-04-01 05:54:06', 0),
(33, 'budimsky@gmail.com', 'c48c2a0f95c9addd8dfa2871b1eac187', 'Arief Budiman', 'Pria', 'Bogor', '27/09/1994', 'Perum Palemsewu Baru No.P2, Sewon, Bantul', 'Institut Seni Indonesia Yogyakarta', '085695743961', '', '@eye_square', '2015-04-02 06:46:32', 0),
(34, 'rifki.7997@gmail.com', '8e98dab36a8721863d686cc648ab0b7d', 'rifki fauzi', 'Pria', 'sleman', '07/09/1997', 'Dsn Jlamprang Pandowoharjo Sleman rt02/10 no36 sleman DIY ', 'PPMI Assalaam', '08159959796', '08161641059', '@pakharr', '2015-04-02 22:20:06', 0),
(35, 'faizal.proses@gmail.com', '77a47c5c4cd18469b8013934c464634e', 'Faizal habibie', 'Pria', 'Surabaya', '14/10/1992', 'Jl.Swadaya 1, Karang Asem Rw.12 no.101 Depok, Sleman, Yogyakarta', '', '082291336136', '082291336136', '@FaaHabb', '2015-04-03 06:55:39', 0),
(36, 'bima.crawling@gmail.com', 'dc1275f68c485b43a55cc572e0e6bed9', 'Bima Aditiawan Putra', 'Pria', 'Sleman', '03/08/1990', 'Pemukti Baru, rt 05/02, Tlogo, Prambanan, Klaten', '', '085729151591', '', '@bimaHowl', '2015-04-04 10:29:37', 0),
(37, 'bwenang@gmail.com', 'a877d4cfd9b8f6ad43d683479dbe3c1d', 'wenang budiargo', 'Pria', 'gunungkidul', '29 september 1994', 'jatisari, playen, gunungkidul RT16 RW04', '', '085729310015', '085729310015', '@mbah_wenang', '2015-04-04 11:18:28', 0),
(38, 'wadahkawan@gmail.com', 'e4c29b18615fe7cd435ae7e812e3e2d7', 'Willybrodus Caesario', 'Pria', 'Depok', '07/11/1994', 'Jl.Anyelir 2, no.279 ,Perumnas Condongcatur, Sleman Yogyakarta', 'Universitas Atmajaya Yogyakarta', '087865615655', '', '@webego', '2015-04-04 12:13:47', 0),
(39, 'abdurrahmanfaiz20@gmail.com', '325449f0d3c2bbf04ce2b7a1d1c9a22d', 'Abdurrahman Faiz', 'Pria', 'Pasuruan', '20/10/1999', 'Kota Pasuruan, Jawa Timur', '', '089613475888', '', '@rahman_faiz', '2015-04-04 12:36:11', 0),
(40, 'aghits@live.com', 'be9a2f08b15993531a4018c017f3049b', 'AGHITS NUR WAHIDIANSYAH', 'Pria', 'MALANG', '25/05/1996', 'Jl. Semangka, Talangsuko, Kec. Turen, Kab. Malang', '', '089655316717', '', '@aghitsss', '2015-04-04 18:25:18', 0),
(41, 'jalaluddin.mahally@gmail.com', '836df915d7e05b185cd91e497741bede', 'Jalaluddin Mahally', 'Pria', 'Lamongan', '25/08/1993', 'Jalan Sumargo Gang Anggrek No 22 Lamongan', '', '08973819771', '', '@LaludMahally', '2015-04-05 00:05:56', 0),
(42, 'yohanniskalaadiw@gmail.com', '9eb6edf7aee5ad29d18002f2b86fa44b', 'Yohan Niskala Adi Waskitha', 'Pria', 'Rembang', '18/12/1993', 'jl. Banowati Tengah 6 no 18 Semarang', 'Universitas Dian Nuswantoro Semarang', '085727104735', '-1', '@YohanNiskala', '2015-04-05 10:00:14', 0),
(43, 'indonesia.skate@yahoo.com', 'b6dbd13b7d1920a5e7018860de01cc2d', 'badai', 'Pria', 'malang', '06/04/1994', 'jl danau poso g2f13\nsawojajar,malang,jawa timur', 'PKBI , Uniersitas Negri Malang', '085954503037', '0', '@badai_rizky', '2015-04-06 05:41:29', 0),
(44, 'irfanadh@gmail.com', 'ea19ad7fae8fabdabaaf4aa528a5d740', 'Irfan Adhitya Putra', 'Pria', 'Tangerang', '18/05/1996', 'Kp.Gaga Jl.Warga Indah 2 RT.02/01 No.58 Kel.Larangan Selatan Kec. Larangan Kota Tangerang', 'Universitas Budi Luhur', '081218021444', '089687655574', '@irfanadhitya18', '2015-04-06 08:21:49', 0),
(45, 'rnhdeloie@gmail.com', '72a8a0a990a78f6dd143010849d446ae', 'Rainhard Eloie', 'Pria', 'Malang', '26/03/2015', 'Jalan Prigen No 2 ', 'SMK Cor Jesu Malang', '087859794530', '481948', '@rainhardeloie', '2015-04-07 03:20:20', 0),
(46, 'ahmadnashiruddinl@yahoo.com', '337aa46e6b29e3f820e69b1fb77d1d82', 'Ahmad Nashiruddin L', 'Pria', 'Gunungkidul', '29/12/1994', 'Karangmojo 1, Karangmojo, Gunungkidul', '', '087738077017', '', '@ANLnajir', '2015-04-07 05:59:47', 0),
(47, 'abdulgg46@gmail.com', '5bf9d021157d55c54efbd1f5bd1806d9', 'abdul gani', 'Pria', 'pontianak', '23/01/1994', 'Kepuh,kepuharjo,cangkringan', '', '087839751781', '', '@abdulgghifari', '2015-04-07 08:46:25', 0),
(48, 'wildan_yanuart@yahoo.co.id', '4f30ef367451bbf5aa9f69a27f91eb21', 'M.Wildan Yanuar', 'Pria', 'makassar', '27/01/1994', 'Perum bambu kuning blok b 14 nomor 14 puskopkar. batu aji Batam - Indonesia', '', '089631639120', '089620711342', '@willdanyanuwart', '2015-04-07 09:45:21', 0),
(49, 'ahmad_suhardi@yahoo.co.id', '196e9d684ba516cc1c6feee8e46b6665', 'Jatisariku', 'Pria', 'Jatisari ', '27/10/2011', 'Dusun V Jatisari No.31 RT.46 Desa Jatimulyo Kecamatan jati Agung Lampung Selatan.', '', '08992289527', '', '@jatisariku', '2015-04-07 21:31:32', 0),
(50, 'rezamamat@gmail.com', '85ff41f00a618935a6e819a63be87e6e', 'Mohammad Reza Fahriyansyah', 'Pria', 'Jakarta', '14/7/1993', 'Ds Saraban, selatan kampus ISI YK, Sewon, Bantul, Yogyakarta', 'ISI YK', '08567971798', '', '@reza_mamat', '2015-04-08 06:44:23', 0),
(51, 'fevrianhidayat@rocketmail.com', '225443c13e3a32f712ccb641fb76ffac', 'fevrian ', 'Pria', 'betung ', '08/02/1995', 'jl urip sumoharjo no 57', 'daarul qur''an boarding school', '085269494143', '', '@fevriyanhidayat', '2015-04-08 09:09:43', 0),
(52, 'paguyubansemprul@gmail.com', 'a8e52217c48d055fb98e2732c587d056', 'Catur Prasetyono', 'Pria', 'Karanganyar', '30/03/1988', 'Jl. Tengah Rt 25 Rw 08 Pagubugan Kulon Binangun Cilacap', 'SMK Manggala Tama Binangun', '087889932627', '', '', '2015-04-08 23:21:49', 0),
(53, 'maulananuralam12@gmail.com', 'f37f6766837c95e224b6e196cf3fce24', 'maulana nur alam', 'Pria', 'bekasi', '11/12/1996', 'jalan mangkubumi no 28 rt 01/05 Kecamatan adipala kabupaten cilacap', 'STIMIK AMIKOM', '081214750175', '', '', '2015-04-09 00:43:27', 0),
(54, 'ikhwan.syahlani@yahoo.com', '2e700c46e0dc3770d07a606a5f6d92b6', 'ikhwan', 'Pria', 'berastagi', '2/2/1981', 'jalan trimurti betastagi', 'Kosongkan jika tidak ada', '082165284890', '', '@tebasaward', '2015-04-09 01:00:53', 0),
(55, 'yogaperwira98@gmail.com', 'deacf76ffb2265f5c145e530eb5dc6e6', 'tripama yoga perwira', 'Pria', 'klaten', '24 maret 1998', 'tempursari,ngawen,klaten.', '', '085743263573', '085743263573', '@yogatripama', '2015-04-09 05:45:21', 0),
(56, 'fajrindiana@yahoo.co.id', 'f927f62ddb254161ff33bedb6fbb4b9d', 'fajrin diana putri', 'Wanita', 'cirebon', '27/02/1999', 'Jln.K.bagus rangin , blok.gandu sari RT.3 RW.1 , Desa wiyong kecamatan susukan kabupaten cirebon', 'SMA NEGERI 1 PALIMANAN', '087729070573', '081214233431', '@fajrin_diana', '2015-04-09 06:59:36', 0),
(57, 'nul.shinta@gmail.com', 'fa512b7d88099071af1b8ce2e7cab0ac', 'Shinta Dewi Ratnasari', 'Wanita', 'Sleman', '23/01/1991', 'Perum Soka Asri Permai Blok A.13 RT.07/RW.03 Purwomartani Kalasan Sleman DIY 55571', '', '081804310186', '', '@nulngganul', '2015-04-09 08:44:20', 0),
(58, 'checentreu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Husain', 'Pria', 'Jambi', '09/10/94', 'Jalan Raya Palembang KM 32 Kab. Ogan ilir sumatera Selatan', 'Univ. Sriwijaya', '085789112649', '', '@Checentreu', '2015-04-09 16:07:03', 0),
(59, 'dessiyanatria@gmail.com', '66114709f95aa4fdc17d399e972c1025', 'Mohammad Tritaufan Saputra', 'Pria', 'Jakarta', '08/08/1997', 'Perumahan Bukit Indah Blok D19 No. 8, Sarua Indah - Ciputat\nTangerang Selatan 15414', 'MX Film Production', '089642975202', '089616172830', '@MUTRSaputra', '2015-04-10 00:49:18', 0),
(60, 'adhitya.ramadhana@rocketmail.com', '47d3dc8e380b7f85c1e195b511af7bf3', 'Aditya Ramadhana', 'Pria', 'Balikpapan', '19/01/98', 'JL. KS TUBUN NO 75A KELURAHAN API API', 'SMA YAYASAN PUPUK KALTIM', '081256010889', '', '@adastira', '2015-04-10 08:55:23', 0),
(61, 'albertkss@gmail.com', '344cb06405fd007c69cfc0baff2728c4', 'sukis', 'Pria', 'grobogan', '02/03/1981', 'gajahmati pati', 'SMA PGRI 2 KAYEN PATI', '085727672025', '085226193725', '', '2015-04-10 18:53:57', 0),
(62, 'anton_demonart@yahoo.com', 'c9e82d9046090a971bead99030f8e4d8', 'anton gustiana', 'Pria', 'Bandung', '16/08/1991', 'Jl.mustika 1 no.28/21 rt 01/06 Bandung', 'STSI Bandung', '087825872177', '', '@Antongustiana_', '2015-04-11 00:00:13', 0),
(63, 'evan.amikom@gmail.com', '7335173e6828022838dfe12d14025606', 'AMIRUDIN KHORUL HUDA', 'Pria', 'Pekanbaeu', '23/09/1994', 'Bukit Lipai, Kec.Batang Cenaku, Kab. Indragiri Hulu. RIAU', '', '085658244334', '085278363120', '', '2015-04-11 01:36:40', 0),
(64, 'mentari19.MP@gmail.com', 'ff44578144ea34001a06e72ba0ce0514', 'Roselina Linggarani Mentari Puteri', 'Wanita', 'Gunungkidul', '19/01/1996', 'Gading III, RT 001, RW 003, Gading, Playen, Gunungkidul', 'STMIK Amikom Yogyakarta ', '087838195131', '', '@mentari_poetri', '2015-04-11 21:08:52', 0),
(65, 'muhalghifari39@gmail.com', '737717d6a707bee87286f9e37e5dcc90', 'm.alghifari', 'Pria', '08/10/1999', '', 'Perum.koprindag blok D.18 desa sumber jaya ,tambun selatan,kabupaten bekasi', 'smk telekomunikasi telesandi bekasi', '082210254677', '', '@muhalghifari39', '2015-04-12 03:21:50', 0),
(66, 'videografiunsri@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Rifki Zarkasih', 'Pria', 'Linggau', '25/07/1991', 'Dusun IV Desa Lawang Agung Rupit dan Jalan Raya Palembang KM 32 Kelurahan Timbangan Sumatera Selatan\n', 'Univ. Sriwijaya', '082280216962', '085789112649', '@videografiunsri', '2015-04-12 05:48:17', 0),
(67, 'faza.liverpudlian@gmail.com', '7bd881ecf41775aac78b4fe86ba25e90', 'Faza Ayyasi', 'Pria', 'Banjarmasin', '07/12/1995', 'Malangrejo', 'Amikom', '085624786556', '', '@fazaayyasi', '2015-04-12 07:29:35', 0),
(68, 'gapley1@gmail.com', '6449d4b70a15125369cf97f049fc4e8c', 'ADITYA SINATRIO', 'Pria', 'bekasi', '22/07/94', 'kec bekasi utara Harapan jaya jl.nakula no 5 rt 07 rw 20 ', 'mmtc yogyakarta', '085773776450', '', '@gagaponly', '2015-04-12 08:39:10', 0),
(69, 'michaeltanur@gmail.com', '972924e2a86fcdfadd3b33da9696083b', 'Michael Tanur', 'Pria', 'jayapura 4 maret 1996', '04/03/1996', 'Jln.Polimak Batu Karang ', 'Universitas Cendrawasih', '85244139135', '85244139135', '@michaeltanur', '2015-04-13 03:35:04', 0),
(70, 'ericknob4777@gmail.com', '977991ac06c96754916effe1cf043198', 'Erika Riambada', 'Pria', 'Bantul', '30/06/1990', 'Juwangen RT.06/RW.02, Purwomartani, Kalasan, Sleman, Yogyakarta', 'STMIK Amikom Yogyakarta', '08562927774', '', '@xtradax', '2015-04-13 08:01:08', 0),
(71, 'gefachrizal@gmail.com', '3df42d60ea5a9e692ca169ff57a30619', 'Gumilang Fachrizal', 'Pria', 'Gunung Kidul', '29/04/1995', 'Karakan Sidomoyo Godean Sleman Yogyakarta', 'Wacana Studio', '089672033432', '', '@gefachrizal', '2015-04-13 11:20:35', 0),
(72, 'iqbalshahlagi@gmail.com', 'afce15760b9ecf2b56986ba3b976a40f', 'Muhammad Iqbal', 'Pria', 'Alue Ie Puteh', '06/01/1993', 'jalan medan banda aceh depan kantor koramil baktiya dusun T. Raja Muhammad desa Alue Ie Puteh kec. baktiya kab. aceh utara prov. NAD', 'SMK Negeri 1 Lhokseumawe', '082360763377', '085920462001', '@iqbaalshah', '2015-04-13 22:03:10', 0),
(73, 'rhezapramudita31@gmail.com', '850be816c3d9bed7287812075906d7f5', 'RHEZA PRAMUDITA', 'Pria', 'JAKARTA', '31/05/1992', 'JL. SWADAYA 7 NO.6 RT 05/024 KALIABANG TENGAH - BEKASI UTARA (17125)', 'BINA SARANA INFORMATIKA', '085289495108', '02188986023', '', '2015-04-15 11:54:35', 0),
(74, 'oi2210av@gail.com', '876cea68befd813ebc90f3dc1e5a8a03', 'Orinta Kemal Pahlevi', 'Pria', 'Sleman', '21/03/1994', 'Sambilegi , Maguwoharjo , Yogyakarta', 'STMIK AMIKOM', '085643330974', '085747966640', '@eivel21', '2015-04-15 21:41:47', 0),
(75, 'egaaja@yahoo.com', '99250ac720ba40913763337c62567ff6', 'ega ferianada', 'Pria', 'surabaya', '24/09/1998', 'jl lebak timur 3b 30 surabaya', '', '083856395011', '', '@egasahabatnoah', '2015-04-15 22:30:20', 0),
(76, 'putribembem8@gmail.com', '4b9d93ef8c3c15c47bde46791d6c5af8', 'putri novita firdaus ', 'Wanita', 'pasuruan ', '15/08/1997', 'lingkungan bakalan no.26 rt.03 rw.01 kel.pagak kec.beji kab.pasuruan prov. Jawa timur kode pos 67154', 'madrasah aliyah negeri bangil ', '081913447448', '', '@putribembem8', '2015-04-16 07:02:38', 0),
(77, 'regi.reggi@gmail.com', 'c9448aae462c60b44a8838a31b1ba534', 'Regi Alwanda', 'Pria', 'Bogor', '01/01/1994', 'Jln.Raya Cileungsi-Jonggol No.25 Ds.Sukamanah Kec.Jonggol Kab.Bogor  ', 'Universitas Esa Unggul Jakarta', '082298012729', '08812366746', '@reggiast', '2015-04-17 03:31:10', 0),
(78, 'windi.kasturi@gmail.com', 'e7d15fc033c5b70c109f2303d0021514', 'Khodijah Windi Kasturi', 'Wanita', 'Gunungkidul', '06/02/1996', 'Jl. AMD 60 RT 02/03, Sumber Lor, Ponjong, Ponjong, Gunungkidul, D.I. Yogyakarta 55892', '', '085759333086', '081392078988', '@wndika', '2015-04-17 04:04:43', 0),
(79, 'sgramah@yahoo.com', 'bb3ba5950e61b74933b0455719a24805', 'Ramah Sugihati', 'Wanita', 'Kendal', '11/02/1997', 'Desa Kalirejo Kecamatan Grabag Kabupaten Purworejo', 'SMA N 2 Purworejo', '085728361375', '081578813525', '@amicaleramah', '2015-04-17 04:50:41', 0),
(80, 'vfxmasterlampung@gmail.com', 'c9c5ff0a88c94fd4bdb09e7f378a283b', 'Eko Prabowo', 'Pria', 'yogyakarta', '13/02/1974', 'Bas Radio, jalan ranggalawe, tugu kuning, unit2, tulang bawang, lampung', '', '085738772961', '', '', '2015-04-17 06:28:05', 0),
(81, 'rizky.hasanuddin@students.amikom.ac.id', 'f8701e5e12e11940da9fa9d57d44d8bf', 'Rizky Syahputra Hasanuddin', 'Pria', 'Dili', '14/11/1995', 'No 49 b, dusun nglarang, padukuhan malangrejo, Ngemplak, Sleman, Yogyakarta', 'STMIK Amikom', '081338456255', '083840300700', '@blingkit', '2015-04-17 09:18:43', 0),
(82, 'ardawi55@gmail.com', '03db69515ebc833e88eb99d6ef2783de', 'putri ardawi', 'Wanita', 'surabaya', '03/02/1997', 'jl. garuda 13/29 rewwin, waru, sidoarjo', 'smkn 1 surabaya', '085931106700', '085706012042', '@ardawip', '2015-04-17 17:38:18', 0),
(83, 'husainabiyyu@gmail.com', 'a241b640894aca929a18ea4d1d50e97f', 'husain abiyyu', 'Pria', 'sukoharjo', '14/06/1997', 'Wonotawang rt 8 bangunjiwo kasihan bantul', 'sma n 5 yogyakarta', '082326333326', '', '@husainabiyyu', '2015-04-17 20:39:54', 0),
(84, 'alfaya48@gmail.com', 'dcf0c7c1aa74c4982caab311d09fd28f', 'AHMAD MIQDAD ALFAYA', 'Pria', 'Pati', '14/04/1995', 'kampung Karangasem no. 59, RT 06 RW 12, Desa Condong catur, Kec. Depok, Kab. Sleman', 'STMIK AMIKOM YOGYAKARTA', '085713153881', '', '@ChenInfinity', '2015-04-18 02:47:46', 0),
(85, 'oktanahendra21@gmail.com', 'db2ce2982f69d495544ef61ffd517674', 'Wahyu Okta Nahendra', 'Pria', 'sijunjung', '06/10/1992', 'Sambilegi lor maguwoharjo, sleman yogyakarta', '', '081374509448', '085725743344', '@nahendra21', '2015-04-18 03:11:20', 0),
(86, 'dimasrizkiagustian@gmail.com', 'e77ae8bd6e707f79f886225823bffd8d', 'Dimas Rizki Agustian', 'Pria', 'Jakarta', '06/08/1992', 'Jl. Mardani Raya No. 18, 001/005, Johar Baru, Jakarta Pusat, 10560', 'BORI PICTURES', '0214223947', '085714040032', '@bori_pictures', '2015-04-18 08:41:53', 0),
(87, 'beni.arona@gmail.com', 'fb6202fe2becbda76be06963b80569eb', 'Beni Arona', 'Pria', 'Medan', '26/06/1981', 'Desa Lambitra - Darussalam, Aceh Besar', 'Karya Kita Kreatif', '085277438819', '', '@BeniArona', '2015-04-18 09:59:21', 0),
(88, 'ahmadrosadi73@gmail.com', 'fbc9d49187ac0aedaccbecf2ba3b7d05', 'Ahmad Rosadi', 'Pria', 'Parindu', '15/12/1994', 'Jl. Riam No 34 Rt 04/Rw 02, Kel. Bunut, Kec. Kapuas, Kab. Sanggau, Kalimantan Barat', '', '089618012351', '0', '@ahmadrosadi73', '2015-04-18 10:23:19', 0),
(89, 'anotheridwan@gmail.com', 'dbdd8a723dfdf3e1949a60ec0f9235b4', 'Rahma Anisah', 'Wanita', 'Jakarta', '02/04/1996', 'Jalan Cililin Raya no. 10 RT 015 RW 006, Kebayoran Baru - Jakarta Selatan 12170', 'Institut Pertanian Bogor', '085212080005', '082123977974', '@BlueRanisa', '2015-04-18 23:09:55', 0);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `t_peserta_bodi`
--
ALTER TABLE `t_peserta_bodi`
  ADD CONSTRAINT `t_peserta_bodi_ibfk_1` FOREIGN KEY (`id_registrasi`) REFERENCES `t_registrasi` (`id_registrasi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_peserta_fotografi`
--
ALTER TABLE `t_peserta_fotografi`
  ADD CONSTRAINT `t_peserta_fotografi_ibfk_1` FOREIGN KEY (`id_registrasi`) REFERENCES `t_registrasi` (`id_registrasi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_peserta_invect`
--
ALTER TABLE `t_peserta_invect`
  ADD CONSTRAINT `t_peserta_invect_ibfk_1` FOREIGN KEY (`id_registrasi`) REFERENCES `t_registrasi` (`id_registrasi`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
