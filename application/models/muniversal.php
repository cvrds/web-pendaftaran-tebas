<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MUniversal extends CI_Model {

	public function get($table,$field1,$value1){
		$this->db->order_by($field1,$value1);
		return $this->db->get($table)->result();
	}
	public function getRow($table,$field,$value){
		$this->db->where($field,$value);
		return $this->db->get($table)->row();
	}
	public function getWhere($table,$field1,$value1,$field2,$value2){
		$this->db->where($field1,$value1);
		$this->db->order_by($field2,$value2);
		return $this->db->get($table)->result();
	}
	public function insert($table,$data){
		$this->db->insert($table,$data);
	}
	public function insertReturn($table,$data){
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	public function insertDate($table,$data,$tableDate){
		$this->db->set($tableDate,'NOW()', FALSE );
		$this->db->insert($table,$data);
	}
	public function update($table,$field,$value,$data){
		$this->db->where($field,$value);
		return $this->db->update($table,$data);
	}
	public function delete($table,$field,$value){
		$this->db->where($field,$value);
		$this->db->delete($table);
	}

	// SPESIFIK MODEL
	public function getPesertaRow($table,$field,$value){
		$this->db->where($field,$value);
		$this->db->join('t_registrasi',"t_registrasi.id_registrasi=$table.id_registrasi");
		return $this->db->get($table)->row();
	}
	public function getWhereOr($table,$field1,$value1,$field2,$value2,$order){
		$this->db->where($field1,$value1);
		$this->db->or_where($field2,$value2);
		$this->db->order_by($order,'asc');
		$this->db->join('t_registrasi',"t_registrasi.id_registrasi=$table.id_registrasi");
		return $this->db->get($table)->result();
	}
	public function getWhereAnd($table,$field1,$value1,$field2,$value2,$order){
		$this->db->where($field1,$value1);
		$this->db->where($field2,$value2);
		$this->db->order_by($order,'asc');
		$this->db->join('t_registrasi',"t_registrasi.id_registrasi=$table.id_registrasi");
		return $this->db->get($table)->result();
	}
}
