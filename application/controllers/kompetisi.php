<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kompetisi extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}
	public $nav = array(
		'm_beranda' => "",
		'm_regis' 	=> "",
		'm_arsip'	=> "",
		'm_faq'		=> "",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""

		);

	public function invect(){
		if($this->auth->is_logged_in() == true){
			redirect('kompetisi/pendaftaranInvect');
		}else{
			$nav = $this->nav;
			$data['nav'] = $nav;
			$this->template->set('title','INDIE MOVIE COMPETITION - TEBAS AWARD 2017');
			$this->template->load('index','v-komp-invect-login',$data);
		}
	}
	public function fotografi(){
		if($this->auth->is_logged_in() == true){
			redirect('kompetisi/pendaftaranFotografi');
		}else{
			$nav = $this->nav;
			$data['nav'] = $nav;
			$this->template->set('title','PHOTOGRAPHY CONTEST - TEBAS AWARD 2017');
			$this->template->load('index','v-komp-fotografi-login',$data);
		}
	}
	public function poster(){
		if($this->auth->is_logged_in() == true){
			redirect('kompetisi/pendaftaranBODI');
		}else{
			$nav = $this->nav;
			$data['nav'] = $nav;
			$this->template->set('title','BATTLE OF DIGITAL ILLUSTRATION - TEBAS AWARD 2017');
			$this->template->load('index','v-komp-bodi-login',$data);
		}
	}
	public function pendaftaranInvect(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['peserta'] = $this->mUniversal->getRow('t_registrasi','id_registrasi',$this->session->userdata('id_registrasi'));
		$this->template->set('title','INDIE MOVIE COMPETITION - TEBAS AWARD 2017');
		$this->template->load('index','v-komp-invect',$data);
	}
	public function pendaftaranFotografi(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['peserta'] = $this->mUniversal->getRow('t_registrasi','id_registrasi',$this->session->userdata('id_registrasi'));
		$this->template->set('title','PHOTOGRAPHY CONTEST - TEBAS AWARD 2017');
		$this->template->load('index','v-komp-fotografi',$data);
	}
	public function pendaftaranBODI(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['peserta'] = $this->mUniversal->getRow('t_registrasi','id_registrasi',$this->session->userdata('id_registrasi'));
		$this->template->set('title','BATTLE OF DIGITAL ILLUSTRATION - TEBAS AWARD 2017');
		$this->template->load('index','v-komp-bodi',$data);
	}
}
