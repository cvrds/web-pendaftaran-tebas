<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}
	public $nav = array(
		'm_beranda' => "",
		'm_regis' 	=> "",
		'm_arsip'	=> "",
		'm_faq'		=> "class='active'",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""

		);

	public function index(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','FREQUENTLY ASKED QUESTIONS - TEBAS AWARD 2017');
		$this->template->load('index','v-faq',$data );
	}
}
