<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Juri extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}
	public $nav = array(
		'm_beranda' => "",
		'm_regis' 	=> "",
		'm_arsip'	=> "",
		'm_faq'		=> "",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""

		);

	public function invect(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','JURI INVECT - TEBAS AWARD 2017');
		$this->template->load('index','v-juri-invect',$data);
	}
	public function fotografi(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','JURI PHOTOGRAPHY CONTEST- TEBAS AWARD 2017');
		$this->template->load('index','v-juri-fotografi',$data);
	}
	public function poster(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','JURI BODI - TEBAS AWARD 2017');
		$this->template->load('index','v-juri-bodi',$data);
	}
}
