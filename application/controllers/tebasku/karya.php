<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Karya extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}

   	public function karyainvect(){

        $this->db->where('jenis_lomba', 'invect');
        $data['karya_invect'] = $this->db->get('t_karya')->result_array();

//        echo json_encode($data);
//        exit();

   		$this->template->load('admin/index','admin/v-karya-invect',$data);
   	}

}
