<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}

   	public function index(){
   		$this->load->view('admin/login');
   	}
   	public function do_login(){
		$username = $this->input->post('username');
        $password = $this->input->post('password');
        $success = $this->auth->do_login_admin($username,$password);
        if($success){
           	redirect('tebasku/beranda');
        }else{
           	$notif = "Username or Password not valid";
           	$this->session->set_userdata('notif',$notif);
           	redirect('tebasku/login');
        }
	}
	public function logout(){
		if($this->auth->is_logged_in_admin() == true){
	    	$this->auth->do_logout();
	   	}
	   	redirect('tebasku/login');
	}
}
