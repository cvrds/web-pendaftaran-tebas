<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bodi extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
	}

	public function fix(){
		$data['peserta'] = $this->mUniversal->getWhereAnd('t_peserta_bodi','bayar','sudah','berkas','sudah','id_peserta_bodi');
		$this->template->load('admin/index','admin/v-bodi-fix',$data);
	}
	public function pending(){
		$data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_bodi','bayar','belum','berkas','belum','id_peserta_bodi');
		$this->template->load('admin/index','admin/v-bodi-pending',$data);
	}
	public function edit($id,$id_bodi){
		$data['id'] = $id;

		if(isset($_POST['submit'])){
			$bodi = array(
				'judul'			=> $this->input->post('judul'),
				'deskripsi'		=> $this->input->post('deskripsi'),
				'bayar'			=> $this->input->post('bayar'),
				'berkas'		=> $this->input->post('berkas')
				);
			$this->mUniversal->update('t_peserta_bodi','id_peserta_bodi',$id_bodi,$bodi );
			$notif = "<div class='alert alert-success text-center'> Bodi Data Successfully updated </div>";
	    	$this->session->set_flashdata('notif',$notif);
	    	redirect('tebasku/bodi/edit/'.$id.'/'.$id_bodi );
		}else{
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_bodi','id_peserta_bodi',$id_bodi);
			$this->template->load('admin/index','admin/v-edit-bodi',$data);
		}
	}
	public function updateBerkas($id){
		$berkas = $this->input->post('berkas');
		$data = array('berkas' => $berkas );
		$this->mUniversal->update('t_peserta_bodi','id_peserta_bodi',$id,$data);
	}
	public function updateBayar($id){
		$bayar = $this->input->post('bayar');
		$data = array('bayar' => $bayar );
		$this->mUniversal->update('t_peserta_bodi','id_peserta_bodi',$id,$data);
	}
	public function hapus($page,$id){
      $this->mUniversal->delete('t_peserta_bodi','id_peserta_bodi',$id);
      redirect('tebasku/bodi/'.$page );
   }
}
