<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}

   	public function index(){
         $data['invect_fix'] = $this->mUniversal->getWhereAnd('t_peserta_invect','bayar','sudah','berkas','sudah','id_peserta_invect');
   		$data['invect_pending'] = $this->mUniversal->getWhereOr('t_peserta_invect','bayar','belum','berkas','belum','id_peserta_invect');
         $data['bodi_fix'] = $this->mUniversal->getWhereAnd('t_peserta_bodi','bayar','sudah','berkas','sudah','id_peserta_bodi');
   		$data['bodi_pending'] = $this->mUniversal->getWhereOr('t_peserta_bodi','bayar','belum','berkas','belum','id_peserta_bodi');
         $data['fotografi_fix'] = $this->mUniversal->getWhere('t_peserta_fotografi','bayar','sudah','id_peserta_fotografi','asc');
   		$data['fotografi_pending'] = $this->mUniversal->getWhere('t_peserta_fotografi','bayar','belum','id_peserta_fotografi','asc');
   		$this->template->load('admin/index','admin/v-beranda',$data);
   	}
      public function downloadInvect(){
         $data['peserta'] = $this->mUniversal->getWhereAnd('t_peserta_invect','bayar','sudah','berkas','sudah','id_peserta_invect');
         $this->load->view('admin/downloadInvect',$data);
      }
      public function downloadFotografi(){
         $data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_fotografi','bayar','sudah','bayar','sudah','id_peserta_fotografi','asc');
         $this->load->view('admin/downloadFotografi',$data);
      }
      public function downloadBodi(){
         $data['peserta'] = $this->mUniversal->getWhereAnd('t_peserta_bodi','bayar','sudah','berkas','sudah','id_peserta_bodi');
         $this->load->view('admin/downloadBodi',$data);
      }
      public function downloadInvectPending(){
         $data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_invect','bayar','belum','berkas','belum','id_peserta_invect');
         $this->load->view('admin/downloadInvectPending',$data);
      }
      public function downloadFotografiPending(){
         $data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_fotografi','bayar','belum','bayar','belum','id_peserta_fotografi','asc');
         $this->load->view('admin/downloadFotografiPending',$data);
      }
      public function downloadBodiPending(){
         $data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_bodi','bayar','belum','berkas','belum','id_peserta_bodi');
         $this->load->view('admin/downloadBodiPending',$data);
      }

      // UPDATE STATUS DATA REGISTRASI
      public function statusDataRegister_dev(){
         $data_regis = $this->mUniversal->get('t_registrasi','id_registrasi','asc');
         foreach ($data_regis as $row) {
            // cek ikut serta data regis
            $cek_invect = $this->mUniversal->getRow('t_peserta_invect','id_registrasi',$row->id_registrasi );
            $cek_foto = $this->mUniversal->getRow('t_peserta_fotografi','id_registrasi',$row->id_registrasi );
            $cek_bodi = $this->mUniversal->getRow('t_peserta_bodi','id_registrasi',$row->id_registrasi );

            if(count($cek_invect) != 0 || count($cek_foto) != 0 || count($cek_bodi) != 0){
               $data_status = array(
                  'status_lomba' => "1"
                  );
               $this->mUniversal->update('t_registrasi','id_registrasi',$row->id_registrasi,$data_status );
            }
         }
         echo "CEK STATUS DATA REGISTRASI BERHASIL";
      }
      public function downloadRegisterNoCompetition(){
         $data['register'] = $this->mUniversal->getWhere('t_registrasi','status_lomba','0','id_registrasi','asc');
         $this->load->view('admin/downloadRegNoCompetition',$data);
      }
}
