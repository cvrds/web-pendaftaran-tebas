<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}

   	public function index(){
      $data['peserta'] = $this->mUniversal->get('t_registrasi','id_registrasi','asc');
   	  $this->template->load('admin/index','admin/v-peserta',$data);
   	}
    public function edit($id){
      $data['id'] = $id;

      $password = $this->input->post('password');

      $this->load->library('form_validation');
      if(!empty($password)){
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('conf_pass', 'Password Confirmation', 'trim|required|matches[password]');
      }
      $this->form_validation->set_rules('telp1', 'Phone Number 1', 'trim|numeric|required');
      $this->form_validation->set_rules('telp2', 'Phone Number 2', 'trim|numeric');
      $this->form_validation->set_error_delimiters('<div class="alert alert-danger text-center">', '</div>');

      if($this->form_validation->run() == false){
        $data['peserta'] = $this->mUniversal->getRow('t_registrasi','id_registrasi',$id);
        $this->template->load('admin/index','admin/v-edit-peserta',$data);
      }else{
        $regis = array(
          'nama'      => $this->input->post('nama'),
          'jk'        => $this->input->post('jk'),
          'tpt_lahir' => $this->input->post('tpt_lahir'),
          'tgl_lahir' => $this->input->post('tgl_lahir'),
          'alamat'    => $this->input->post('alamat'),
          'instansi'  => $this->input->post('instansi'),
          'telp1'     => $this->input->post('telp1'),
          'telp2'     => $this->input->post('telp2'),
          'twitter'   => $this->input->post('twitter')
          );
        $this->mUniversal->update('t_registrasi','id_registrasi',$id,$regis );

        if(!empty($password)){
          $regis = array('password' => md5($password) );
          $this->mUniversal->update('t_registrasi','id_registrasi',$id,$regis );
        }

        $notif = "<div class='alert alert-success text-center'> Profile Successfully updated </div>";
        $this->session->set_flashdata('notif',$notif);
        redirect('tebasku/register/edit/'.$id );
      }
    }
    public function hapus($id){
      $this->mUniversal->delete('t_registrasi','id_registrasi',$id);
      redirect('tebasku/register');
    }
}
