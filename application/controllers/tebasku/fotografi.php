<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Fotografi extends CI_Controller {



	public function __construct(){

       	parent::__construct();

      	session_start();

	}



	public function fix(){

		$data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_fotografi','bayar','sudah','bayar','sudah','id_peserta_fotografi');

		$this->template->load('admin/index','admin/v-fotografi-fix',$data);

	}

	public function pending(){

		$data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_fotografi','bayar','belum','bayar','belum','id_peserta_fotografi');

		$this->template->load('admin/index','admin/v-fotografi-pending',$data);

	}

   public function edit($id,$id_foto){

      $data['id'] = $id;



      if(isset($_POST['submit'])){

         $foto = array(
           'judul'			=> $this->input->post('judul'),
           'deskripsi'		=> $this->input->post('deskripsi'),
           'bayar'			=> $this->input->post('bayar'),
           'berkas'		    => $this->input->post('berkas')
         );

         $this->mUniversal->update('t_peserta_fotografi','id_peserta_fotografi',$id_foto,$foto );

         $notif = "<div class='alert alert-success text-center'> Photography Contest Data Successfully updated </div>";

         $this->session->set_flashdata('notif',$notif);

         redirect('tebasku/fotografi/edit/'.$id.'/'.$id_foto );

      }else{

         $data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_fotografi','id_peserta_fotografi',$id_foto);

         $this->template->load('admin/index','admin/v-edit-fotografi',$data);

      }

   }

   public function updateBayar($id){

      $bayar = $this->input->post('bayar');

      $data = array('bayar' => $bayar );

      $this->mUniversal->update('t_peserta_fotografi','id_peserta_fotografi',$id,$data);

   }

   public function hapus($page,$id){

      $this->mUniversal->delete('t_peserta_fotografi','id_peserta_fotografi',$id);

      redirect('tebasku/fotografi/'.$page );

   }

}
