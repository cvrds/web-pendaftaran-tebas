<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invect extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}

   	public function fix(){
   		$data['peserta'] = $this->mUniversal->getWhereAnd('t_peserta_invect','bayar','sudah','berkas','sudah','id_peserta_invect');
   		$this->template->load('admin/index','admin/v-invect-fix',$data);
   	}
   	public function pending(){
   		$data['peserta'] = $this->mUniversal->getWhereOr('t_peserta_invect','bayar','belum','berkas','belum','id_peserta_invect');
   		$this->template->load('admin/index','admin/v-invect-pending',$data);
   	}
   	public function edit($id,$id_invect){
		$data['id'] = $id;

		if(isset($_POST['submit'])){
			$invect = array(
				'kategori'			=> $this->input->post('kategori'),
				'judul'				=> $this->input->post('judul'),
				'genre'				=> $this->input->post('genre'),
				'bahasa'			=> $this->input->post('bahasa'),
				'durasi'			=> $this->input->post('durasi'),
				'penonton'			=> $this->input->post('penonton'),
				'tahun'				=> $this->input->post('tahun'),
				'tpt_produksi'		=> $this->input->post('daerah'),
				'produser'			=> $this->input->post('produser'),
				'sutradara'			=> $this->input->post('sutradara'),
				'naskah'			=> $this->input->post('naskah'),
				'editor'			=> $this->input->post('editor'),
				'pemeran'			=> $this->input->post('pemeran'),
				'ditayangkan'		=> $this->input->post('penayangan'),
				'karya_pertama'		=> $this->input->post('karyapertama'),
				'film1'				=> $this->input->post('film1'),
				'tahun1'			=> $this->input->post('thn1'),
				'film2'				=> $this->input->post('film2'),
				'tahun2'			=> $this->input->post('thn2'),
				'film3'				=> $this->input->post('film3'),
				'tahun3'			=> $this->input->post('thn3'),
				'kompetisi1'		=> $this->input->post('kompetisi1'),
				'kompetisi2'		=> $this->input->post('kompetisi2'),
				'kompetisi3'		=> $this->input->post('kompetisi3'),
				'prestasi1'			=> $this->input->post('prestasi1'),
				'prestasi2'			=> $this->input->post('prestasi2'),
				'prestasi3'			=> $this->input->post('prestasi3'),
				'backsound'			=> $this->input->post('backsound'),
				'sinopsis'			=> $this->input->post('sinopsis'),
				'bayar'				=> $this->input->post('bayar'),
				'berkas'			=> $this->input->post('berkas')
				);
			$this->mUniversal->update('t_peserta_invect','id_peserta_invect',$id_invect,$invect );
			$notif = "<div class='alert alert-success text-center'> Invect Data Successfully updated </div>";
	    	$this->session->set_flashdata('notif',$notif);
	    	redirect('tebasku/invect/edit/'.$id.'/'.$id_invect );
		}else{
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_invect','id_peserta_invect',$id_invect);
			$this->template->load('admin/index','admin/v-edit-invect',$data);
		}
   	}
   	public function updateBerkas($id){
		$berkas = $this->input->post('berkas');
		$data = array('berkas' => $berkas );
		$this->mUniversal->update('t_peserta_invect','id_peserta_invect',$id,$data);
	}
	public function updateBayar($id){
		$bayar = $this->input->post('bayar');
		$data = array('bayar' => $bayar );
		$this->mUniversal->update('t_peserta_invect','id_peserta_invect',$id,$data);
	}
	public function hapus($page,$id){
		$this->mUniversal->delete('t_peserta_invect','id_peserta_invect',$id);
		redirect('tebasku/invect/'.$page );
	}
}
