<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}
	public $nav = array(
		'm_beranda' => "class='active'",
		'm_regis' 	=> "",
		'm_arsip'	=> "",
		'm_faq'		=> "",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""

		);

	public function index(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','TEBAS AWARD 2017');
		$this->template->load('index','v-beranda',$data );
	}
	public function login(){
		$email = $this->input->post('email');
        $password = $this->input->post('password');
        $success = $this->auth->do_login($email,$password);
        if($success){
           	redirect('registrasi');
        }else{
           	echo "<script>";
	    	echo "alert('Maaf, email dan password Anda tidak cocok.');";
	    	echo "window.history.go(-1);";
	    	echo "</script>";
        }
	}
	public function logout(){
		if($this->auth->is_logged_in() == true){
	    	$this->auth->do_logout();
	   	}
	   	redirect('beranda');
	}
}
