<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arsip extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}
	public $nav = array(
		'm_beranda' => "",
		'm_regis' 	=> "",
		'm_arsip'	=> "class='active'",
		'm_faq'		=> "",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""

		);

	public function index(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','ARSIP - TEBAS AWARD 2017');
		$this->template->load('index','v-blog',$data );
	}
}
