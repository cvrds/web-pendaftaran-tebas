<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();
   	}
	public $nav = array(
		'm_beranda' => "",
		'm_regis' 	=> "class='active'",
		'm_arsip'	=> "",
		'm_faq'		=> "",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""
		);

	public function index(){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$this->template->set('title','REGISTRASI - TEBAS AWARD 2017');
		$this->template->load('index','v-registrasi',$data);
	}
	public function doRegistrasi(){
		$this->load->library('form_validation');
	    $this->form_validation->set_rules('password', 'Password', 'trim|required');
	    $this->form_validation->set_rules('conf_pass', 'Password Confirmation', 'trim|required|matches[password]');
	    $this->form_validation->set_rules('telp1', 'Phone Number 1', 'trim|numeric|required');
	    $this->form_validation->set_rules('telp2', 'Phone Number 2', 'trim|numeric');
	    $this->form_validation->set_error_delimiters('<div class="alert alert-danger text-center">', '</div>');

	    if ($this->form_validation->run() == false) {
	    	echo "<script>";
	    	echo "alert('Maaf, Masih ada data yang salah. Silahkan ulangi dan isi sesuai dengen ketentuan.');";
	    	echo "window.history.go(-1);";
	    	echo "</script>";
	    }else{
	    	$cek_email = $this->mUniversal->getWhere('t_registrasi','email',$this->input->post('email'),'id_registrasi','asc');
	    	if(count($cek_email) == 0){
	    		$regis = array(
			    	'email' 	=> $this->input->post('email'),
			    	'password'	=> md5($this->input->post('password')),
			    	'nama'		=> $this->input->post('nama'),
			    	'jk'		=> $this->input->post('jk'),
			    	'tpt_lahir'	=> $this->input->post('tpt_lahir'),
			    	'tgl_lahir'	=> $this->input->post('tgl_lahir'),
			    	'alamat'	=> $this->input->post('alamat'),
			    	'instansi'	=> $this->input->post('instansi'),
			    	'telp1'		=> $this->input->post('telp1'),
			    	'telp2'		=> $this->input->post('telp2'),
			    	'twitter'	=> $this->input->post('twitter')
		    		);
		    	$this->mUniversal->insertDate('t_registrasi',$regis,'tgl_registrasi' );

		    	// AUTHENTICATION LOGIN
		    	$email = $this->input->post('email');
	        	$password = $this->input->post('password');
	        	$success = $this->auth->do_login($email,$password);
	        	if($success){
	            	redirect('registrasi');
	         	}else{
	            	echo "<script>";
		    		echo "alert('Maaf, email dan password Anda tidak cocok.');";
		    		echo "window.history.go(-1);";
		    		echo "</script>";
	         	}
	    	}else{
	    		echo "<script>";
		    	echo "alert('Maaf, Email Anda sudah terdaftar. Silahkan login atau menggunakan email lain!');";
		    	echo "window.history.go(-1);";
		    	echo "</script>";
	    	}
	    }
	}
	public function daftarInvect(){
		$invect = array(
			'id_registrasi'		=> $this->input->post('id_regis'),
			'kategori'			=> $this->input->post('kategori'),
			'judul'				=> $this->input->post('judul'),
			'genre'				=> $this->input->post('genre'),
			'bahasa'			=> $this->input->post('bahasa'),
			'durasi'			=> $this->input->post('durasi'),
			'penonton'			=> $this->input->post('penonton'),
			'tahun'				=> $this->input->post('tahun'),
			'tpt_produksi'		=> $this->input->post('daerah'),
			'produser'			=> $this->input->post('produser'),
			'sutradara'			=> $this->input->post('sutradara'),
			'naskah'			=> $this->input->post('naskah'),
			'editor'			=> $this->input->post('editor'),
			'pemeran'			=> $this->input->post('pemeran'),
			'ditayangkan'		=> $this->input->post('penayangan'),
			'karya_pertama'		=> $this->input->post('karyapertama'),
			'film1'				=> $this->input->post('film1'),
			'tahun1'			=> $this->input->post('thn1'),
			'film2'				=> $this->input->post('film2'),
			'tahun2'			=> $this->input->post('thn2'),
			'film3'				=> $this->input->post('film3'),
			'tahun3'			=> $this->input->post('thn3'),
			'kompetisi1'		=> $this->input->post('kompetisi1'),
			'kompetisi2'		=> $this->input->post('kompetisi2'),
			'kompetisi3'		=> $this->input->post('kompetisi3'),
			'prestasi1'			=> $this->input->post('prestasi1'),
			'prestasi2'			=> $this->input->post('prestasi2'),
			'prestasi3'			=> $this->input->post('prestasi3'),
			'backsound'			=> $this->input->post('backsound'),
			'sinopsis'			=> $this->input->post('sinopsis'),
			'bayar'				=> "belum",
			'berkas'			=> "belum"
			);
		$id = $this->mUniversal->insertReturn('t_peserta_invect',$invect);

		if($id < 10){ $kode = "INV00".$id; }
		elseif($id < 100){ $kode = "INV0".$id; }
		else{ $kode = "INV".$id; }

		$pendf = array('no_pendf' => $kode );
		$this->mUniversal->update('t_peserta_invect','id_peserta_invect',$id,$pendf);
		$_id 	= tebas_encrypt($id);
		redirect('registrasi/sukses/invect/'.$_id );
	}
	public function daftarFotografi(){
		$foto = array(
			'id_registrasi'	=> $this->input->post('id_regis'),
			'judul'			=> $this->input->post('judul'),
			'deskripsi'		=> $this->input->post('deskripsi'),
			'bayar'			=> "belum",
			'berkas'		=> "belum"
			);
		$id = $this->mUniversal->insertReturn('t_peserta_fotografi',$foto);


		if($id < 10){ $kode = "PC00".$id; }
		elseif($id < 100){ $kode = "PC0".$id; }
		else{ $kode = "PC".$id; }

		$pendf = array('no_pendf' => $kode );
		$this->mUniversal->update('t_peserta_fotografi','id_peserta_fotografi',$id,$pendf);

		$_id 	= tebas_encrypt($id);
		redirect('registrasi/sukses/fotografi/'.$_id );
	}
	public function daftarBodi(){
		$bodi = array(
			'id_registrasi'	=> $this->input->post('id_regis'),
			'judul'			=> $this->input->post('judul'),
			'deskripsi'		=> $this->input->post('deskripsi'),
			'bayar'			=> "belum",
			'berkas'		=> "belum"
			);
		$id = $this->mUniversal->insertReturn('t_peserta_bodi',$bodi);

		if($id < 10){ $kode = "BODI00".$id; }
		elseif($id < 100){ $kode = "BODI0".$id; }
		else{ $kode = "BODI".$id; }

		$pendf = array('no_pendf' => $kode );
		$this->mUniversal->update('t_peserta_bodi','id_peserta_bodi',$id,$pendf);

		$_id 	= tebas_encrypt($id);
		redirect('registrasi/sukses/bodi/'.$_id );
	}
	public function sukses($komp,$id){
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;
		$data['komp'] = $komp;
		$this->template->set('title','TEBAS AWARD 2017');
		$this->template->load('index','v-registrasi-download',$data);
	}
	public function download($komp,$id){
		//1. cek ada session nggak?
		//2. cek session sama dengan pendaftar?
		// 3. kalo gnggak ada redirect ke home


		if($this->auth->is_logged_in()==false AND !$this->auth->is_logged_in_admin()){
			redirect(base_url());
		}

		$id_session = $this->session->userdata('id_registrasi');

		$id=tebas_decrypt ($id);
		if($komp == "invect"){
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_invect','id_peserta_invect',$id);
			$this->load->view('v-download-invect',$data);
		}elseif($komp == "fotografi"){
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_fotografi','id_peserta_fotografi',$id);

			if ($id_session!=$data['peserta']->id_registrasi AND $this->auth->is_logged_in_admin()==false)
				redirect(base_url());

			$this->load->view('v-download-fotografi',$data);
		}else{
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_bodi','id_peserta_bodi',$id);
			$this->load->view('v-download-bodi',$data);
		}
	}
}
