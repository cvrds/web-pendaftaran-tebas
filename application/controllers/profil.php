<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct(){
       	parent::__construct();
      	session_start();

      	// redirect kalo belum login
      	if($this->auth->is_logged_in()==false AND !$this->auth->is_logged_in_admin() ) {
      		redirect(base_url());
      	}

   	}

   	private function profile_auth( $id ) {

      	// admin atau bukan
      	if ( !$this->auth->is_logged_in_admin() ) {
      		if ( $this->session->userdata('id_registrasi') != $id ) {
      			redirect(base_url());
      		}
      	}
   	}

	public $nav = array(
		'm_beranda' => "",
		'm_regis' 	=> "",
		'm_arsip'	=> "",
		'm_faq'		=> "",
		'm_kontak'	=> "",
		'm_jadwal'	=> ""

		);

	public function peserta($id){
		$id=tebas_decrypt($id);
		$this->profile_auth($id);
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;
		$data['invect'] = $this->mUniversal->getWhere('t_peserta_invect','id_registrasi',$id,'id_peserta_invect','asc');
		$data['fotografi'] = $this->mUniversal->getWhere('t_peserta_fotografi','id_registrasi',$id,'id_peserta_fotografi','asc');
		$data['bodi'] = $this->mUniversal->getWhere('t_peserta_bodi','id_registrasi',$id,'id_peserta_bodi','asc');
		$this->template->set('title','PROFIL - TEBAS AWARD 2017');
		$this->template->load('index','v-profil',$data );
	}

	public function upload($id){
        $data['base_url'] = base_url();
		$id=tebas_decrypt($id);
		$this->profile_auth($id);
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;


		$this->template->set('title','PROFIL - TEBAS AWARD 2017');
		$this->template->load('index','v-upload',$data );
	}

    public function add() {
        if ( !$this->auth->is_logged_in()) {
            redirect(base_url());
        }

        if($_POST){
            $no_pend = $this->input->post('no_pend');
            $nama = $this->input->post('nama');
            $jenis_lomba = $this->input->post('jenis_lomba');
            $kategori = $this->input->post('kategori');
            $link_karya = $this->input->post('link_karya');
            $link_trailer = $this->input->post('link_trailer');
            $doc_karya = $this->input->post('doc_karya');

            $config['allowed_types']    = 'rar|zip';
            $config['max_size']         = 6000;
            $config['upload_path']      = "uploads/{$jenis_lomba}/{$kategori}";
            $new_name = $no_pend.'_'.$nama;
            $config['file_name'] = $new_name;

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('doc_karya'))
            {
                $data = [
                    'status'    => 0,
                    'message'   => 'Upload Gagal! Karena '. $this->upload->display_errors(),
                ];
                header('Content-Type: application/json');
                echo json_encode($data);
                exit();
            }
            else
            {
                $data_upload = $this->upload->data();
                $doc_karya = $data_upload['file_name'];
            }

            if($jenis_lomba == 'invect') {
                $link_karya = $this->input->post('link_karya');
                parse_str(@parse_url($link_karya)['query'], $query);
                $link_karya = @$query['v'];

                $link_trailer = $this->input->post('link_trailer');
                parse_str(@parse_url($link_trailer)['query'], $query);
                $link_trailer = @$query['v'];

                $data = array(
                    'no_pend' => $no_pend,
                    'nama' => $nama,
                    'jenis_lomba' => $jenis_lomba,
                    'kategori' => $kategori,
                    'link_karya' => $link_karya,
                    'link_trailer' => $link_trailer,
                    'doc_karya' => $doc_karya,
                );
            }

            else {
               $data = array(
                    'no_pend' => $no_pend,
                    'nama' => $nama,
                    'jenis_lomba' => $jenis_lomba,
                    'doc_karya' => $doc_karya,
                );
            }


            $this->db->insert('t_karya',$data);

            $data = [
                'status'    => 1,
                'message'   => 'Data Berhasil Di Input',
            ];

            header('Content-Type: application/json');
            echo json_encode($data);
            exit();
        }
        else {
            $id=tebas_decrypt($id);
            $this->profile_auth($id);
            $nav = $this->nav;
            $data['nav'] = $nav;
            $data['id'] = $id;


            $this->template->set('title','PROFIL - TEBAS AWARD 2017');
            $this->template->load('index','v-upload',$data );

            header('Content-Type: application/json');
            echo json_encode($data);
            exit();
        }

    }

	public function edit($id){
		$id=tebas_decrypt($id);
		$this->profile_auth($id);
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;

		$password = $this->input->post('password');

		$this->load->library('form_validation');
		if(!empty($password)){
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
	    	$this->form_validation->set_rules('conf_pass', 'Password Confirmation', 'trim|required|matches[password]');
		}
	    $this->form_validation->set_rules('telp1', 'Phone Number 1', 'trim|numeric|required');
	    $this->form_validation->set_rules('telp2', 'Phone Number 2', 'trim|numeric');
	    $this->form_validation->set_error_delimiters('<div class="alert alert-danger text-center">', '</div>');

	    if($this->form_validation->run() == false){
			$data['peserta'] = $this->mUniversal->getRow('t_registrasi','id_registrasi',$id);
			$this->template->set('title','TEBAS AWARD 2017');
			$this->template->load('index','v-profil-edit',$data );
	    }else{
	    	$regis = array(
		    	'nama'		=> $this->input->post('nama', true),
		    	'jk'		=> $this->input->post('jk'),
		    	'tpt_lahir'	=> $this->input->post('tpt_lahir'),
		    	'tgl_lahir'	=> $this->input->post('tgl_lahir'),
		    	'alamat'	=> $this->input->post('alamat'),
		    	'instansi'	=> $this->input->post('instansi'),
		    	'telp1'		=> $this->input->post('telp1'),
		    	'telp2'		=> $this->input->post('telp2'),
		    	'twitter'	=> $this->input->post('twitter')
	    		);
	    	$this->mUniversal->update('t_registrasi','id_registrasi',$id,$regis );

	    	if(!empty($password)){
	    		$regis = array('password' => md5($password) );
	    		$this->mUniversal->update('t_registrasi','id_registrasi',$id,$regis );
	    	}

	    	$notif = "<div class='alert alert-success text-center'> Profile Successfully updated </div>";
	    	$this->session->set_flashdata('notif',$notif);
	    	redirect('profil/edit/'.tebas_encrypt($id) );
	    }
	}
	public function editInvect($id,$id_invect){
		$id= tebas_decrypt($id);
		$id_invect= tebas_decrypt($id_invect);
		$this->profile_auth($id);
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;

		if(isset($_POST['submit'])){
			$invect = array(
				'kategori'			=> $this->input->post('kategori'),
				'judul'				=> $this->input->post('judul'),
				'genre'				=> $this->input->post('genre'),
				'bahasa'			=> $this->input->post('bahasa'),
				'durasi'			=> $this->input->post('durasi'),
				'penonton'			=> $this->input->post('penonton'),
				'tahun'				=> $this->input->post('tahun'),
				'tpt_produksi'		=> $this->input->post('daerah'),
				'produser'			=> $this->input->post('produser'),
				'sutradara'			=> $this->input->post('sutradara'),
				'naskah'			=> $this->input->post('naskah'),
				'editor'			=> $this->input->post('editor'),
				'pemeran'			=> $this->input->post('pemeran'),
				'ditayangkan'		=> $this->input->post('penayangan'),
				'karya_pertama'		=> $this->input->post('karyapertama'),
				'film1'				=> $this->input->post('film1'),
				'tahun1'			=> $this->input->post('thn1'),
				'film2'				=> $this->input->post('film2'),
				'tahun2'			=> $this->input->post('thn2'),
				'film3'				=> $this->input->post('film3'),
				'tahun3'			=> $this->input->post('thn3'),
				'kompetisi1'		=> $this->input->post('kompetisi1'),
				'kompetisi2'		=> $this->input->post('kompetisi2'),
				'kompetisi3'		=> $this->input->post('kompetisi3'),
				'prestasi1'			=> $this->input->post('prestasi1'),
				'prestasi2'			=> $this->input->post('prestasi2'),
				'prestasi3'			=> $this->input->post('prestasi3'),
				'backsound'			=> $this->input->post('backsound'),
				'sinopsis'			=> $this->input->post('sinopsis')
				);
			$this->mUniversal->update('t_peserta_invect','id_peserta_invect',$id_invect,$invect );
			$notif = "<div class='alert alert-success text-center'> Invect Data Successfully updated </div>";
	    	$this->session->set_flashdata('notif',$notif);
	    	redirect('profil/editInvect/'.tebas_encrypt($id).'/'.tebas_encrypt($id_invect) );
		}else{
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_invect','id_peserta_invect',$id_invect);
			$this->template->set('title','EDIT - TEBAS AWARD 2017');
			$this->template->load('index','v-komp-invect-edit',$data );
		}
	}
	public function editFotografi($id,$id_foto){
		$id = tebas_decrypt($id);
		$id_foto = tebas_decrypt($id_foto);
		$this->profile_auth($id);
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;

		if(isset($_POST['submit'])){
			$foto = array(
                'judul'			=> $this->input->post('judul'),
				'deskripsi'		=> $this->input->post('deskripsi')
            );
			$this->mUniversal->update('t_peserta_fotografi','id_peserta_fotografi',$id_foto,$foto );
			$notif = "<div class='alert alert-success text-center'> Photography Contest Data Successfully updated </div>";
	    	$this->session->set_flashdata('notif',$notif);
	    	redirect('profil/editFotografi/'.tebas_encrypt($id).'/'.tebas_encrypt($id_foto) );
		}else{
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_fotografi','id_peserta_fotografi',$id_foto);
			$this->template->set('title','EDIT - TEBAS AWARD 2017');
			$this->template->load('index','v-komp-fotografi-edit',$data );
		}
	}
	public function editBodi($id,$id_bodi){
		$id = tebas_decrypt($id);
		$id_bodi = tebas_decrypt($id_bodi);
		$this->profile_auth($id);
		$nav = $this->nav;
		$data['nav'] = $nav;
		$data['id'] = $id;

		if(isset($_POST['submit'])){
			$bodi = array(
				'judul'			=> $this->input->post('judul'),
				'deskripsi'		=> $this->input->post('deskripsi')
				);
			$this->mUniversal->update('t_peserta_bodi','id_peserta_bodi',$id_bodi,$bodi );
			$notif = "<div class='alert alert-success text-center'> Bodi Data Successfully updated </div>";
	    	$this->session->set_flashdata('notif',$notif);
	    	redirect('profil/editBodi/'.tebas_encrypt($id).'/'.tebas_encrypt($id_bodi) );
		}else{
			$data['peserta'] = $this->mUniversal->getPesertaRow('t_peserta_bodi','id_peserta_bodi',$id_bodi);
			$this->template->set('title','EDIT - TEBAS AWARD 2017');
			$this->template->load('index','v-komp-bodi-edit',$data );
		}
	}
	public function hapus($komp,$id_regis,$id_peserta){
		$id_regis=tebas_decrypt($id_regis);
		$id_peserta=tebas_decrypt($id_peserta);
		if($komp == "invect"){
			$this->mUniversal->delete('t_peserta_invect','id_peserta_invect',$id_peserta);
		}elseif($komp == "fotografi"){
			$this->mUniversal->delete('t_peserta_fotografi','id_peserta_fotografi',$id_peserta);
		}else{
			$this->mUniversal->delete('t_peserta_bodi','id_peserta_bodi',$id_peserta);
		}
		redirect('profil/peserta/'.tebas_encrypt($id_regis) );
	}
}
