<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
  <div class="container bodi">
    <div class="col-md-12">
       <h2 class="caption">BODI</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2 isi-form" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h4 style="color:#FFF">Karya yang dihasilkan merupakan illustrasi dari suatu mainan. Contohnya kelereng, gasing, congklak, yoyo, Tamiya, tamagochi dan mainan era 90'an lainnya. Diharapkan, hasil karya illustrasi ini mengandung pesan yang dapat tersampaikan terutama untuk anak - anak di zaman sekarang.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
              <li>Lomba ini berlaku untuk umum.</li>
              <li>Peserta lomba adalah perorangan.</li>
              <li> Karya harus orisinil dan belum pernah menjuarai kategori lomba TEBAS.</li>
              <li>Karya tidak diperkenankan mengandung unsur SARA (Suku, Agama, dan RAS) , tidak mengandung unsur pornografi serta tidak <li>bertentangan dengan ketentuan perundangan yang berlaku di Indonesia.</li>
              <li>Diperkenankan mengirim lebih dari 1 (satu) karya.</li>
              <li>Karya yang dikumpulkan dalam bentuk softcopy (CMYK/RGB), diupload / diunggah ke tebasaward.com dengan ketentuan ukuran A3, high-resolution/300 dpi , format *.JPG/JPEG dengan ukuran minimal 2MB dan maksimal 6MB dan disertai dengan deskripsi karya.</li>
              <li> Peserta wajib memahami dan mematuhi seluruh ketentuan & syarat lomba seperti yang tercantum di halaman ini.</li>
              <li>Hasil karya dilarang mengikuti perlombaan sejenis selama acara TEBAS berlangsung.</li>
              <li>Panitia berhak menggunakan karya peserta dan pemenang lomba untuk kegiatan promosi dan publikasi tanpa memberikan kompensasi <li>apapun dengan hak cipta karya tetap dipegang oleh pemilik karya.</li>
              <li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.</li>
              <li>Keputusan juri tidak bisa diganggu gugat.</li>
              <li>Ketentuan yang belum di atur dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
              <li> Hasil akhir bukan karya tangan (sketsa manual), harus melalui proses olah digital.</li>
              <li> Digital Illustrasi dipersilahkan menggunakan Bitmap/Vektor.</li>
              <li> Karya dipersilahkan menggunakan media photo/image (dicantumkan identitas photo tersebut, sebagai karya pribadi atau hasil download dari mana sumbernya), 3D modelling, illustrasi manual, atau gabungan ketiganya.</li>
              <li> IDR : 20K per Karya yang dikirimkan</li>


          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Biaya Pendaftaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Biaya Pendaftaran Battle Of Digital Illustration adalah :<br>
                - Katagori Umum&nbsp;:&nbsp; Rp. 20.000,-
            </li>
            <li>Biaya pendaftaran diatas berlaku untuk 1 karya dan dikalikan sesuai jumlah karya.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh karya Battle Of Digital Illustration yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh karya peserta yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, Karya peserta akan masuk pada tahap penilaian Juri.</li>
            <li>Semua karya akan di tampilkan di lokasi pameran TEBAS (SHOW UP).</li>
            <li>Pemenang kategori Battle Of Digital Illustration terfavorit diambil dari semua karya yang lolos tahap awal penjurian. Polling like dilakukan di <a href="https://www.facebook.com/TEBASAWARD" target="_blank"> <b>Fanspage TEBAS AWARD</b> </a> pada saat SHOW UP TEBAS 2017 berlangsung.</li>
            <li>Pemenang akan diumumkan dalam acara TEBAS AWARDS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Juara I</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Juara II</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Juara Favorit</td>
          <td>Trophy + Sertifikat</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Cara Pengumpulan Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">
          <ol>
            <li>Silahkan Login Akun TEBAS dan pilih menu profil --> upload karya</li>
            <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
            <li>Pilih kategori lomba</li>
            <li>File berformat rar/zip yang berisi :
             <ul>
             <li>Karya</li>
             <li><i><b>file *.doc (berisi nama lengkap, judul dan deskripsi karya)</b></i></li>
             <li>Scan Kartu Identitas (KTM/KTP)</li>
             <li>Surat Pernyataan (Gambar/Scan)</li>
             <li>Bukti Transaksi (Gambar/Scan)</li>
             </ul>

             </li>
            <li>Ukuran file .rar/.zip max 6MB</li>
          </ol>
          </div>
          </div>
        </div>
		    <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSix" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSix">
          <div class="panel-body text-left">
            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Battle Of Digital Illustration = Rp. 20.000,- <br>
                Nomor HP Anda = 085 6288 2396<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 10.396</i>
              </li>
              <li>Kirim ke nomor rekening MUAMALAT 5370004949<br>AN : Fanny Septiani.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 085 6288 2396 (Chako) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Arif Rusmandani#10.396</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim.</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>

          </div>
          </div>
        </div>

      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center" style="color:#FFF"><label>FORMULIR PENDAFTARAN<br />BATTLE OF DIGITAL ILLUSTRATION</label></h3>
      <h4 class="text-center" style="color:#FFF">Mainan Masa Kecilku</h4><br /><br />

      <form action="<?php echo site_url('registrasi/daftarBodi');?>" method="POST">
      <div class="col-md-12 col-xs-12 pull-left h-f1">
        <label>A. DATA PENDAFTAR</label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        1. Nama
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        2. Tempat Tanggal Lahir
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->tpt_lahir;?>" class="form-control" placeholder="Tempat Lahir" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->tgl_lahir;?>" class="form-control" placeholder="1992/12/23" id="dp" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        3. Alamat
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" maxlength="300" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat;?></textarea>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        4. Instansi/sekolah
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->instansi;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        5. No. Telephone (Utama)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" value="<?php echo $peserta->telp1;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        6. No. Telephone (Sekunder)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" value="<?php echo $peserta->telp2;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        7. Email
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="email" value="<?php echo $peserta->email;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        8. Akun Instagram
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        9. Judul Karya
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="judul" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        10. Deskripsi Karya
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" name="deskripsi" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"></textarea>
      </div>

      <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
        <input type="hidden" name="id_regis" value="<?php echo $peserta->id_registrasi;?>">
        <button type="submit" class="btn btnlogin pd-btn"> Daftar </button>
        &nbsp;&nbsp;&nbsp;
        <button type="reset" class="btn btn-danger pd-btn"> Batal </button>
      </div>


      </form>
    </div>

  </div>
</div>
