<!-- FORM -->
<div id="mainkontenjuri">
  <div class="container">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <br>
        <h1 style="font-size:60px">DEWAN JURI</h1>
        <h2 style="color:#8A0000">Photography Contest</h2>
        <br><br>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
      <img src="<?php echo base_url();?>assets/img/i-pc.png" alt="tebas2014" class="img-responsive">
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/risman.png" alt="juri" class="img-responsive pull-right" style="margin:20px" width="25%">
      <h2>Risman Marah</h2>
      <p>Lahir di Bukittinggi, 3 Mei 1951.
        Pendidikan SR, SMP, dan Sekolah Seni Rupa Indonesia Padang tamat 1970.
        Tahun 1971 melanjutkan kuliah ke Jurusan SeniLukis, Sekolah Tinggi Seni Rupa Indonesia “ASRI” Yogyakarta (sekarang:  Institut Seni Indonesia Yogyakarta). Setelah lulus di STSRI “ASRI” Yogyakarta diangkat menjadi staf pengajar di sana.
        Pendidikan terakhir di Pascasarjana Institut Seni Indonesia Yogyakarta 2005-2008 Minat Utama Penciptaan Seni Fotografi.
.</p>
      <ul>
        <li>Mendirikan Fakultas Seni Media Rekam, Institut Seni Indonesia Yogyakarta pada tahun 1994 dan menjadi dekan selama dua periode (1994-2003).</li>
        <li>Pernah mengajar fotografi di Universitas Atma Jaya Yogyakarta, UPN Yogyakarta, MSD Yogyakarta, D3 Komunikasi UGM Yogyakarta.</li>
        <li>Menjadi Penguji Ahli Fotografi di UiTM Shah Alam, Kuala Lumpur Malaysia 2010-2014.</li>
        <li>Banyak mengerjakan liputan fotografi untuk berbagai kementerian untuk dijadikan buku profile Kementerian Riset dan Teknologi RI, Kementerian Sosial RI, dll.</li>
        <li>Dewan Juri Lomba Fotografi, menjadi Penceramah dan Pemberi Workshop Fotografi di berbagai Perguruan Tinggi dan Komunitas Fotografi.</li>
        <li>Sebagai penceramah fotografi di Kapal Pemuda Nusantara Sail Raja Ampat 2013, dan Sail Tomini 2014.</li>
        <li>Menjadi kurator untuk Buku-buku Fotografi yang diterbitkan oleh PT Kemilau Indonesia Jakarta.</li>
        <li>Pensiun sebagai PNS dan Dosen Fotografi ISI Yogyakarta terhitung 1 Juni 2016.</li>
      </ul>
    </div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/munir.png" alt="juri" class="img-responsive pull-left" style="margin:20px" width="25%">
      <h2>Misbachul Munir</h2>
      TTL : Magelang 11-01-1977<br>
      Pendidikan : Teknik Geologi UPN Veteran Jogjakarta
      <p><b>Prolog:</b></p>
      <ul>
        <li>Sarjana Teknik Geologi yang memulai Bidang Fotografi sejak</li>
        <li>1996. Memutuskan untuk resign dari Dunia Ekplorasi Minyak dan Gas</li>
        <li>2005 untuk lebih berkonsentrasi di bidang Fotografi. Aktif menulis untuk artikel tentang fotografi & travelling di beberapa Majalah dalam dan luar negeri.</li>
        <li>Kontributor Reader Digest Indonesia</li>
        <li>Kontributor Pelita - PERTAMINA Lintas Warta Magazine</li>
        <li>Kontributor CHIPS Foto Video Gramedia Group</li>
        <li>Kontributor Horizon Magazine British Petroleum</li>
        <li>Kontributor Exposure Magazine</li>
        <li>Moderator fotografer[dot]net</li>
      </ul>
      <p><b>Photo Contest :</b></p>
      <ul>
        <li>Juara 1 Canon Photomarathon 2015 Bali</li>
        <li>Juara 1 Canon Marathon 2009 - Trip To Japan</li>
        <li>Juara 2 Lomba Foto Satwa Taman buah Mekarsari</li>
        <li>Juara 2 Lomba Foto Ulang Tahun Jakarta 2007</li>
      </ul>
      <p><b>Pengajar Fotografi workshop:</b></p>
        <ul>
         <li>Extreme Sport Digital Imaging Margocity Depok</li>
          <li>Club Fotografi Bea Cukai - FOCUS</li>
          <li>Bank Mandiri Photo Club</li>
          <li>LENSA Club Universitas Ahmad Dahlan</li>
          <li>PT.Technip Indonesia photo club</li>
        </ul>
    </div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/budiprast.jpg" alt="juri" class="img-responsive pull-right" style="margin:20px" width="25%">
      <h2>Budi Prast</h2>
      <p>Pekerjaan : Pewarta  Foto Kabare Magazine 2005 – Sekarang<br>
      <b>PENGALAMAN ORGANISASI :</b><br>
     <ul>
         <li>HISFA (Himpunan Seni Foto Amatir Yogyakarta) - Pengurus (2012 - Sekarang)</li>
         <li>PFI (Pewarta Foto Indonesia) Yogyakarta - Anggota (2005 -Sekarang)</li>
     </ul>
     <b>PENGHARGAAN LOMBA FOTO</b><br>
     <ul>
         <li>Finalis Lomba Foto Indonesia Piala Presiden 2014.</li>
         <li>Juara 1 Tema 1 Canon Photomarathon 2014.</li>
         <li>Juara 1 Lomba Foto TAHURA Yogyakarta 2014.</li>
         <li>Juara 1 Rally Photo Kotagede “TEBAS” Yogyakarta 2014.</li>
         <li>Juara 1 Rally Photo Eksplor Waduk Sermo Teraswarta Kulon Progo DIY 2014.</li>
         <li>Juara 1 lomba Foto Satwa Liar BKSDA Jawa Tengah 2013.</li>
         <li>Juara 1 & Favorit Lomba foto Budaya Yogyakarta by Gudang Digital Yogyakarta 2013.</li>
         <li>Juara 1 Tabloid Cempaka Female Fun Walk Photo Contest  Semarang.</li>
         <li>Juara 2 Lomba Rally Photo Mal Ciputra Jakarta.</li>
         <li>Juara 2 lomba foto Arsitektur  dalam acara Pekan Arsitektur  Indonesia (IAI) 2013.</li>
         <li>Juara 2 Lomba Foto Saparan Rebo Pungkasan,bendung khayangan Kulon Progo.</li>
         <li>Juara 2 Lomba Foto Haul ke 88 Saptohoedojo 2013.</li>
         <li>Juara 2 Lomba Foto Model Clasic Auto Fest 2013 di JEC.</li>
         <li>Juara 2 Lomba foto Model on the spot Uvindo Yogyakarta 2013.</li>
         <li>Juara 2 Lomba Foto Bedog Arts Festifal IV 2012 Yogyakarta.</li>
         <li>Juara 2 Loma foto Satwa & Model Di Taman Safari  Indonesia II 2011.</li>
         <li>Juara 2 Lomba Foto Lingkungan, Jepege with Yayasan Koesnadi Hardjosumantri UGM.</li>
     </ul>

      </p>
    </div>
  </div>
</div>
