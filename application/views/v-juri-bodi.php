<!-- FORM -->
<div id="mainkontenjuri">
  <div class="container">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <br>
        <h1 style="font-size:60px">DEWAN JURI</h1>
        <h2 style="color:#8A0000">Battle Of Digital Illustration</h2>
        <br><br>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
      <img src="<?php echo base_url();?>assets/img/i-bodi.png" alt="tebas2014" class="img-responsive">
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/rahmat.jpg" alt="juri" class="img-responsive pull-right" style="margin:20px" width="25%">
      <h2>Rahmat Kurniawan</h2>
        <p><b>Rahmat Kurniawan,</b> Berangkat dari kecintaannya akan menggambar, Rahmat Kurniawan a.k.a Roompoetliar mengekspresikan kecintaannya
        terhadap budaya Indonesia melalui karya seni visual. Rahmat menggabungkan tema lokal dengan seni modern yang
        menghasilkan karya-karya unik dan sangat inspiratif. ~ www.desainstudio.com.</p>
        <b>PENGALAMAN, PROFESI, ORGANISASI</b><br>
        <ul>
            <li>2014, Event Promotion, Sukribo Exhibition, Bentara Budaya YK.</li>
            <li>2014, Website Design, SPEDAGI Temanggung.</li>
            <li>2014, Wheatpaste YOGO ANGONGGO YOGI serentak 7 kota, Kampuz Jalanan YK.</li>
            <li>2014, Digital Illustration (jury), Tebas Award.</li>
            <li>2014, Graphic Designer, JUJUR BARENGAN KPK, Malioboro YK.</li>
            <li>2014, Illustrator, Event Promotion, FESTIVAL KESENIAN BIKERZ, Candi Ijo YK.</li>
        </ul>
        <b>PENGHARGAAN</b><br>
        <ul>
            <li>2013, Hastalia Studio Branding, Bronze, Pinasthika Award</li>
            <li>2013, CIP ADARO, Silver, Pinasthika Award</li>
            <li>2013, FKY25, Silver, Pinasthika Award</li>
            <li>2013, Zpirit Garuda-Topanerz Bike Week, Gold, Pinasthika Award</li>
            <li>2013, Kalender Kebangsaan, Silver, Pinasthika Award</li>
            <li>2013, Agency of The Year, Pinasthika Award</li>
            <li>2014, Juru Gambar PUNAKAWAN nguri-nguri kebudayaan jalanan, Fest. Kesenian Bikerz</li>
        </ul>
        <b>PAMERAN DAN PUBLIKASI</b><br>
        <ul>
            <li>2013, Creative Speaker, Diskompeace UNS Solo</li>
            <li>2013, Petruk Moco Buku - wheatpaste di jalanan Kediri Jatim</li>
            <li>2013, Rumah Garuda Exhibition, Sellie cafe YK</li>
            <li>2014, Exlusive TEES Design, 20thn Boomerang</li>
            <li>2014, Rupanada - Kreavi X Jogjaforce, Jogja Gallery YK</li>
            <li>2014, Creative Speaker, Behance Review, JDV YK</li>
            <li>2014, Gareng ‘Write on the Wall - Wheatpaste memperingati hari literasi sedunia, Kediri</li>
            <li>2014, FKB 3 Wheatpaste ‘PUNAKAWAN GUGUR GUNUNG’, Bukit Padaz Lereng Gamping Candi Ijo, YK</li>
        </ul>

    </div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/gepeng.png" alt="juri" class="img-responsive pull-left" style="margin:20px" width="25%">
      <h2>Rahmat Tri Basuki</h2>
        <p><b>Rahmat Tri Basuki a.k.a Gepeng Rahmat Njawani</b>, freelancher graphic designer at njawani.</p>
        <b>KEAHLIAN :</b><br>
        <p>Graphic Design - Logo Design - Illustration Digital - Illustration Vector - Illustration Book - Illustration Children's - Books T-shirt - Graphics Digital - Imaging Vector - Design Vector Advertising.</p>
        <b>PENDIDIKAN :</b><br>
        <ul>
            <li>Indonesian Institute of Art.</li>
            <li>Bachelor of Science (B.S.), Visual Communication Design, 2004 – 2010.</li>
        </ul>
    </div>

    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/dedi.png" alt="juri" class="img-responsive pull-right" style="margin:20px" width="25%">
      <h2>Dedi Rokkinvisual</h2>
      <p><b>Dedi Rokkinvisual</b>, lelaki berstatus DropOut dari jurusan Desain Komunikasi Visual ini adalah mantan Creative Head di PT. Petakumpet. Setelah berkecimpung di advertising agency selama 9 tahun, memutuskan untuk mendirikan Rokkinvisual Studio demi explorasi sisi idealismenya dalam berkarya serta sebagai wadah belajar bersama. Bersama "Komunitas Belajar Bikin Iklan : Otak Atik Otak / KBBI OAO" yang telah didirikannya tahun 2012 bersama Dosen dan beberapa mahasiswa UMY jurusan Komunikasi angkatan 2010. Meskipun selalu mementingkan sebuah proses daripada sekedar output, sebanyak 80-an penghargaan di berbagai festival kreatif dalam dan luar negeri telah diraihnya. Berguna & Berbeda adalah prinsip yang selalu dia pegang hingga kini. Kelompok Belajar Bikin Iklan OAO membuat Sejarah. 5 Tahun berdiri, 4 Kali berturut-turut menjadi Agency Of The Year Baskara Pinasthika Creativestival 2013 | 2014 | 2015 | 2016. 25 Piala dibawa pulang tahun ini.
</p>
    </div>
  </div>
</div>
