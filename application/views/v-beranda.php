<!--  HEADER -->
<div id="header">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%;visibility: hidden;" alt="header">
  <div class="container header">

    <div id="Carousel" class="carousel slide sliderlimit">
        <ol class="carousel-indicators">
          <li data-target="#Carousel" data-slide-to="0" class="active"></li>
          <li data-target="#Carousel" data-slide-to="1"></li>
          <!-- SLIDE 3
          <li data-target="#Carousel" data-slide-to="2"></li>
          SLIDE 3 -->
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                            <img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive">
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <h1 style="background: -webkit-linear-gradient(#00CCFF, #FFFF00);-webkit-background-clip: text; -webkit-text-fill-color: transparent;">
                          <b>
                          TEBAS 2017
                            <!-- <div><span style="color:#ff0000;">T</span><span style="color:#ff4000;">E</span><span style="color:#ff7f00;">B</span><span style="color:#ffff00;">A</span><span style="color:#80ff00;">S</span><span style="color:#00ff00;"> </span><span style="color:#00ffff;">2</span><span style="color:#0080ff;">0</span><span style="color:#0000ff;">1</span><span style="color:#8b00ff;">7</span></div> -->
                          </b>
                          </h1>
                          <h3>
                            <div><span style="color:#ff0000;">R</span><span style="color:#ff2000;">A</span><span style="color:#ff4000;">G</span><span style="color:#ff5f00;">A</span><span style="color:#ff7f00;">M</span><span style="color:#ff9f00;"> </span><span style="color:#ffbf00;">M</span><span style="color:#ffdf00;">E</span><span style="color:#ffff00;">M</span><span style="color:#bfff00;">O</span><span style="color:#80ff00;">R</span><span style="color:#40ff00;">I</span><span style="color:#00ff00;"> </span><span style="color:#00ff55;">K</span><span style="color:#00ffaa;">R</span><span style="color:#00ffff;">E</span><span style="color:#00bfff;">A</span><span style="color:#0080ff;">T</span><span style="color:#0040ff;">I</span><span style="color:#0000ff;">V</span><span style="color:#2300ff;">I</span><span style="color:#4600ff;">T</span><span style="color:#6800ff;">A</span><span style="color:#8b00ff;">S</span></div>
                          </h3>
                          <p style="color:#FFF;">TEBAS (The Best Annual Multimedia Show) lomba multimedia akbar dari Jogjakarta hadir kembali di tahun 2017 dengan tema RAGAM MEMORI KREATIVITAS. Mengulas kembali memori masyarakat tentang permainan anak - anak zaman dahulu baik permainan tradisional maupun modern pada masa-nya melalui karya multimedia yang bertujuan menginspirasi masyarakat untuk memperkenalkan, melestarikan dan menginovasi permainan yang sudah mulai ditinggalkan.</p>
                          <a class='youtube' href="https://www.youtube.com/embed/02SgT9xdI4Q"><button type="button" class="btn btn-tebas btn-lg">VIDEO TEASER</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
							       <br><br>
                            <img src="<?php echo base_url();?>assets/img/koma_white.png" alt="logo" class="img-responsive">
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <h1 style="color:#FFF">KOMA</h1>
                          <h3 style="color:#FFF;">KOMUNITAS MULTIMEDIA AMIKOM</h3>
                          <p style="color:#FFF;">KOMA adalah organisasi mahasiswa yang bergerak dan mewadahi kreatifitas dalam bidang Multimedia. Ada empat divisi di KOMA : Graphic Design, Web Design, Broadcast dan Vfx Animation. KOMA -  Tempatnya Kreator Audio Visual Amikom yang Menghasilkan Karya dengan Kreatifitas Tanpa Batas. </p>
                          <a href="http://koma.or.id" target="blank"><button type="button" class="btn btn-tebas btn-lg">WEB KOMA</button></a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- SLIDE 3
            <div class="item">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-md-5 col-sm-5 col-xs-12 pull-right">
                            <img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive">
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                          <h1>TEBAS 2016</h1>
                          <h3>GORESAN PESAN PERUBAHAN</h3>
                           <p>TEBAS 2016 bertemakan "Goresan Pesan Perubahan" Kritikan yang tidak membatasi pola pikir seseorang untuk dapat menghasilkan pemikiran baru yang bertujuan, mengedukasi, memotivasi, dan menginspirasi yang dituangkan ke dalam karya multimedia untuk perubahan masa depan yang lebih baik dan tidak menimbulkan kritikan baru.</p>
                          <a href="<?php echo site_url('faq');?>"><button type="button" class="btn btn-default btn-lg">MORE INFO</button></a>
                        </div>
                    </div>
                </div>
            </div>
            SLIDE 3 -->
          </div>
      </div>
  </div>
  <div class="footerhead">
    <div style="padding-bottom:35px;">
      <br><br>
      <a href="#turun-limit" title="Turun" class="totop"><span class="glyphicon glyphicon-circle-arrow-down" style="margin-top:3px"></span></a>
    </div>
    <!--<img src="<?php echo base_url();?>assets/img/footerhead.png" class="img-responsive" style="width:100%" alt="foothead">-->
  </div>
  </div>
</div>

<!--  KOMPETISI -->
<div id="turun-limit" syle="margin-top:100px"></div>
<div id="competition">
  <div class="container">
      <h2 class="caption judul-c">COMPETITION</h2>
      <a href="<?php echo site_url('kompetisi/invect');?>">
      <div class="col-md-4 icon">
        <img src="<?php echo base_url();?>assets/img/i-invect.png" alt="invect" class="img-responsive" width="80%">
        <h2>Indie Movie Competition</h2>
        <h4>Tema : Bebas (Non Dokumenter)</h4>
        <p>Karya yang dihasilkan bertemakan bebas tapi bukan film dokumenter.</p>
        <button type="button" class="btn btn-lomba btn-sm">More Info <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/fotografi');?>">
      <div class="col-md-4 icon lombapc">
        <img src="<?php echo base_url();?>assets/img/i-pc.png" alt="photo" class="img-responsive" width="80%">
        <h2>Photography Contest</h2>
        <h4>Tema : Aku dan Permainan Masa Kecilku</h4>
        <p>Karya yang dihasilkan menggambarkan suasana permainan anak - anak pada era 90'an...</p>
        <button type="button" class="btn btn-lomba btn-sm">More Info <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/poster');?>">
      <div class="col-md-4 icon lombabodi">
          <img src="<?php echo base_url();?>assets/img/i-bodi.png" alt="bodi" class="img-responsive" width="80%">
          <h2>Battle Of Digital Illustration</h2>
          <h4>Tema : Mainan Masa Kecilku</h4>
          <p>Karya yang dihasilkan merupakan illustrasi dari suatu mainan. Contohnya kelereng...</p>
          <button type="button" class="btn btn-lomba btn-sm">More Info <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
  </div>
</div>


<!--  DEWAN JURI -->

<div id="juri">
  <div class="container" style="max-width:900px">
    <div id="CarouselJuri" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#CarouselJuri" data-slide-to="0" class="active"></li>
          <li data-target="#CarouselJuri" data-slide-to="1"></li>
          <li data-target="#CarouselJuri" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px"><h2 class="text-center">JURI INDIE MOVIE COMPETITION</h2></div>
                    <a href="<?php echo site_url('juri/invect');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/triyanto.jpg" alt="juri" class="img-responsive text-center">
                      <h3>Triyanto Hapsoro</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/invect');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/greg.png" alt="juri" class="img-responsive text-center">
                      <h3>Greg Arya</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
					         <a href="<?php echo site_url('juri/invect');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/adrian.jpg" alt="juri" class="img-responsive text-center">
                      <h3>Adrian Jonathan Pasaribu</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px"><h2 class="text-center">JURI PHOTOGRAPHY CONTEST</h2></div>
                    <a href="<?php echo site_url('juri/fotografi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/munir.png" alt="juri" class="img-responsive text-center">
                      <h3>Misbachul Munir</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/fotografi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/budiprast.jpg" alt="juri" class="img-responsive">
                      <h3>Budi Prast</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/fotografi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/risman.png" alt="juri" class="img-responsive">
                      <h3>Risman Marah</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>

                </div>
            </div>

            <div class="item">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:20px"><h2 class="text-center">JURI BATTLE OF DIGITAL ILLUSTRATOR</h2></div>
                    <a href="<?php echo site_url('juri/bodi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/dedi.png" alt="juri" class="img-responsive text-center">
                      <h3>Dedi Rokkinvisual</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/bodi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/rahmat.jpg" alt="juri" class="img-responsive">
                      <h3>Rahmat Kurniawan</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                    <a href="<?php echo site_url('juri/bodi');?>">
                    <div class="col-md-4 col-sm-4 profil-juri">
                      <img src="<?php echo base_url();?>assets/img/gepeng.png" alt="juri" class="img-responsive">
                      <h3>Rahmat Tri Basuki</h3>
                      <button type="button" class="btn btnlogin btn-sm">Profil Juri <span class="glyphicon glyphicon-chevron-right"></span></button>
                      <br><br>
                    </div>
                    </a>
                </div>
            </div>
      </div>
  </div>

  <!-- Sponsor
  <div id="sponsor">
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
    <div class="col-md-3 col-sm-3 block">
      <div class="ads"><b>sponsor</b><br>one block</div>
    </div>
  -->
  </div>
  </div>
</div>
<!--   <div class="borderjuri"></div>
 -->
<!--  REGISTRASI -->
<div id="registration">
  <div class="container">
    <div class="daftar">
    <h1 style="color:#00ADFF;font-weight:bold;font-size:50px">JANGAN LEWATKAN!</h1>
    <h2 style="color:#00CCFF;font-weight:bold;">SHOW UP 13-14 MEI 2017</h2>
    <h4>di Loop Station Yogyakarta</h4>(Jl. Trikora no.2, Yogyakarta)
    <p>Pameran, screening film, voting karya, hiburan, pembagian sertifikat</p>
    <h2 style="color:#00CCFF;font-weight:bold;">AWARDING 22 MEI 2017</h2>
    <h4>di Taman Budaya Yogyakarta</h4>(Jl. Sriwedani No.1, Daerah Istimewa Yogyakarta, Indonesia)
    </div>
  </div>
</div>
<!-- <div class="borderred"></div> -->
<!--  BLOG -->
<div id="blog">
  <div class="container">
    <div class="col-md-8">
      <h1>ARSIP TEBAS</h1>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/01.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb01.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/02.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb02.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/03.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb03.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/04.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb04.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/05.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb05.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/06.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb06.jpg" alt="arsip" class="img-responsive"></a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/07.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb07.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/011.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb011.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/012.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb012.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/013.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb013.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/014.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb014.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/015.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb015.jpg" alt="arsip" class="img-responsive"></a>
        </div>

    </div>
    <div class="col-md-4 col-sm-12">
    <h1>TWITTER WIDGET</h1>
    <br>
	  <a class="twitter-timeline" href="https://twitter.com/TebasAward" data-widget-id="444397868204322816">Tweet oleh @TebasAward</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>
  </div>
</div>
<!--  VIDEO -->
<!--<div class="videoborderatas"></div>-->
<div id="videos">
  <div class="container">
    <div class="col-md-3 col-xs-12 pull-right">
      <h2>NEW VIDEO</h2>
      <p>Support Official Information Video of TEBAS 2017. Please subscribe our youtube channel in <a href="" target="_blank" style="color:#fff"><b>THE BEST ANNUAL MULTIMEDIA SHOW</b></a>.</p><script src="https://apis.google.com/js/platform.js"></script>

<div class="g-ytsubscribe" data-channel="MediaVideoKoma" data-layout="default" data-count="default"></div>
    </div>
	<div class="col-md-3 col-xs-12">
	<a class='youtube' href="https://www.youtube.com/embed/02SgT9xdI4Q"><img src="http://img.youtube.com/vi/02SgT9xdI4Q/mqdefault.jpg" class="img-responsive" /></a>
      <h4>Teaser TEBAS 2017</h4>
      </a>

    </div>
    <!-- <div class="col-md-3 col-xs-12">

      <a class='youtube' href="https://www.youtube.com/embed/VuDyF5AraSU"><img src="https://i.ytimg.com/vi_webp/VuDyF5AraSU/mqdefault.webp" class="img-responsive" />
      <h4>Video Tutorial Pendaftaran TEBAS 2017</h4>
      </a>

    </div> -->

  </div>
</div>

<!--<div class="videoborder"></div>-->

<div id="support">
  <div class="container">
    <div class="col-md-3">

        <h2>SUPPORTED BY</h2>
    <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
<!--
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/unstrat.png" class="img-responsive"></div>
        </div>

        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/fourcolours.png" class="img-responsive"></div>
        </div>
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/potrait.png" class="img-responsive"></div>
        </div>
        <div class="col-md-6 col-sm-4 block">
          <div class=""><img src="assets/img/support/amo.png" class="img-responsive"></div>
        </div>

 -->



    <div class="clearfix"></div>
      </div>


      <div class="col-md-5" style="padding-bottom:10px;">

        <h2>SPONSORED BY</h2>
    <div class="col-md-6 col-sm-12 col-xs-12 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
          <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
<!--
        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/ayamuntu.png" class="img-responsive"></div>
        </div>
          <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/movieboxx.png" class="img-responsive"></div>
        </div>
         <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/nickmates.png" class="img-responsive"></div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/explosives.png" class="img-responsive"></div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 block">
          <div><img src="assets/img/support/sangkakala.png" class="img-responsive"></div>
        </div>
 -->
    <div class="clearfix"></div>
      </div>

      <div class="col-md-4">
        <h2>MEDIA PARTNER</h2>
         <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
    <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

    <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/space.jpg" class="img-responsive"></div>
        </div>
  <!--
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/jogjastudent.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/jizfm.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/pyyy.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/harianjogja.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/seputarevent.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/jogjahits.png" class="img-responsive"></div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/mqfm.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/kecilb.png" class="img-responsive"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6 block">
          <div class=""><img src="assets/img/support/eventapaaja.png" class="img-responsive"></div>
        </div> -->
  <!--
  -->
      </div>

    <div class="clearfix"></div>
            <h2 align="center">ORGANIZED BY</h2>
        <div class="col-md-12 col-sm-12 block">
          <div class=""><img src="assets/img/support/koma.png" class="img-responsive" width="20%"></div>
        </div>

  </div>
</
