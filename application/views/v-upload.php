<?php $this->auth->restrict();?>
    <!-- FORM -->
    <div id="mainkonten">
        <div class="container">
            <div class="col-md-12">
                <h2 class="caption">PROFIL</h2> </div>
            <div class="col-md-12" style="background:rgba(224, 224, 224, 0.5);border-radius:5px">
                <div class="row">
                    <div class="col-md-3">
                        <h2>MENU</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($id));?>">Home</a></li>
                            <li><a href="<?php echo site_url('profil/edit/'.tebas_encrypt($id));?>">Profile</a></li>
                            <li class="active"><a href="<?php echo site_url('profil/upload/'.tebas_encrypt($id));?>">Upload Karya</a></li>
                            <li><a href="<?php echo site_url('beranda/logout');?>">Logout</a></li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <h2>Upload Karya</h2>
                        <div class="panel-body" style="background:#fff;margin-bottom:20px">
                            <?php echo validation_errors();?>
                            <?php if($this->session->flashdata('notif')){ echo $this->session->flashdata('notif'); }?>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              Cara Upload Karya
                                            </a>
                                         </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <ol>
                                                <li>Silahkan Login Akun TEBAS dan pilih menu profil --> upload karya</li>
                                                <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
                                                <li><b>Untuk lomba BODI dan FOTO :</b> <br>
                                                File berformat .zip yang berisi karya, file *.doc (berisi nama lengkap, judul dan deskripsi karya), scan kartu identitas, scan surat pernyataan, scan bukti transaksi.<br>
                                                <b>Untuk lomba INVECT :</b> <br>
                                                File berformat .zip yang berisi file Synopsis Film, Biodata lengkap format *.doc, scan kartu identitas, scan surat pernyataan, scan bukti transaksi dan surat rekomendasi (tuntuk kategori Pelajar).
                                                </li>
                                                <li>Pilih kategori lomba</li>
                                                <li>Ukuran file .rar/.zip max 6MB</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <form id="form-add" action="<?php echo base_url();?>profil/add" method="post" enctype="multipart/form-data" onsubmit="return false;">
                                <div class="form-group">
                                    <label for="usr">Nomor Pendaftaran:</label>
                                    <input type="text" name="no_pend" class="form-control" id="usr" required>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Nama Pembuat:</label>
                                    <input type="text" name="nama" class="form-control" id="pwd" required>
                                </div>
                                <div class="form-group">
                                    <label for="pilihkarya">Pilih Kategori Lomba:</label>
                                    <select class="form-control" name="jenis_lomba" id="pilihkarya">
                                        <option selected hidden=""></option>
                                        <option value="photography">Photography Contest</option>
                                        <option value="bodi">Battle Of Digital Illustration (BODI)</option>
                                        <option value="invect">Indie Movie Competition (INVECT)</option>
                                    </select>
                                </div>

                                <div class="form-group" id="kategori">
                                    <label for="pilihkarya">Pelajar / Umum :</label>
                                    <select class="form-control" id="pilihkarya" name="kategori">
                                        <option selected hidden=""></option>
                                        <option value="pelajar">INVECT PELAJAR</option>
                                        <option value="umum">INVECT UMUM</option>
                                    </select>
                                </div>

                                <div class="form-group" id="karya">
                                    <label for="pwd">Link Youtube (Karya):</label>
                                    <input type="text" class="form-control" name="link_karya" id="pwd">
                                </div>
                                <div class="form-group" id="trailer">
                                    <label for="pwd">Link Youtube (Trailer):</label>
                                    <input type="text" class="form-control" name="link_trailer" id="pwd">
                                </div>

                                <div class="form-group">
                                    <label>Upload Karya:</label>
                                    <input type="file" class="form-control" name="doc_karya" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="form-control primary fwhite ladda-button btn-upload" data-style="expand-left"><span class="ladda-label">UPLOAD KARYA</span></button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
