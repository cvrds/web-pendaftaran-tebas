<!-- FORM -->
<div id="mainkontenjuri">
  <div class="container">
    <div class="col-md-9 col-sm-9 col-xs-12">
        <br>
        <h1 style="font-size:60px">DEWAN JURI</h1>
        <h2 style="color:#8A0000">Indie Movie Competition</h2>
        <br><br>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
      <img src="<?php echo base_url();?>assets/img/i-invect.png" alt="tebas2014" class="img-responsive">
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/triyanto.jpg" alt="juri" class="img-responsive pull-right" style="margin:20px" width="25%">
      <h2>Triyanto Hapsoro</h2>
      <p>Triyanto Hapsoro. Nama panggilan : Genthong, asli  Yogyakarta. Memulai berkarir di dunia audio visual sejak tahun 1992. Pernah bekerja di Studio Audio Visual Puskat Yogyakarta di bagian produksi dan training (1995-2001). Trainer untuk karyawan beberapa stasiun televisi: RCTI, TPI, Lativi, TV7. Bermain di beberapa sinetron,ftv, layar lebar (mulai tahun 1994).Menyutradarai dan memproduseri beberapa film dokumenter dan film cerita non komersial. Mendirikan Production House Axis Media Inside Jogja (2002-2011) . Saat ini menjadi freelancer di Dapur Film dan mendirikan Jogjawood Vision (2011).</p>
    <ul>
      FILMOGRAFI<br>
        Beberapa filmografi karya Triyanto Hapsoro:

        <li>Videoklip "Samudera Debu"- Candra Malik, produksi Jogjawood Vision, as line producer (Juni 2012)</li>
        <li>Videoklip "Fatwa Rindu"-Candra Malik, produksi Ranah Rindu Management dan Jogjawood Vision, as producer, script writer, director (September 2012)</li>
        <li>Film Pendek "Cerita Di Balik Pintu", produksi Ekspresinema, as DoP (Desember 2012)</li>
        <li>Film pendek sejarah "Sebelum Serangan Fadjar", produksi Dinas Kebudayaan Prov DIY & Sanggit Citra Films, Director (Juli 2014)</li>
        <li>Film pendek sejarah "Basiyo mBarang Kahanan", produksi Dinas Kebudayaan Prov DIY & Sanggit Citra Production, Director (Maret-Juni 2015)</li>
        <li>Iklan Layanan Masyarakat DivHumas Mabes Polri "Kewaspadaan Warga", produksi Divisi Humas mabes POLRI, Director of Photography (Februari 2016)</li>
    </ul>
    </div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/greg.png" alt="juri" class="img-responsive pull-left" style="margin:20px" width="25%">
      <h2>Greg Arya</h2>
      <p>GREGORIUS  ARYA  DHIPAYANA,  MSN. LECTURER  –  FILM  EDITOR</p>
        <ul>
            <b>PENGALAMAN  KERJA</b><br>
            <li>Film Editor – FOURCOLOURS FILMS (2005 – Sekarang)</li>
            <li>Dosen Tetap – JURUSAN TV dan FILM, FSMR ISI YK (2010 – Sekarang)</li>
            <li>Film Editor – HOMPYMPAA ARTWORKS (2014 – Sekarang)</li>
            <li>Dosen Tidak Tetap – JOGJA FILM ACADEMY (2015 – Sekarang)</li>
        </ul>
            <b>EDITOR  VIDEOGRAPHY</b><br>
        <ul>
            <li>Public  Service  Advertisement  for  "PURA  PERSADA" (2013)</li>
            <li>Public  Service  Advertisement  for  "PAN" (2013)</li>
            <li>TV  Commercial  for  Bank  Rakyat  Indonesia (2015)</li>
            <li>Viral  Video  for  Bank  Mandiri (2016)</li>
            <li>TV  Commercial  for  Bank  Mandiri (2016)</li>
        </ul>
        <b>EDITOR FILMOGRAPHY</b><br>
        <ul>
            <li>Feature  Film  – Siti (2014)</li>
            <li>Feature  Film  –  Para  Pemburu  Gajah  (co-editor) (2014)</li>
            <li>Documentary  Film  –  Jogja  Istimewa (2014)</li>
            <li>Short  Film  –  Semalam  Anak  Kita  Pulang (2015)</li>
            <li>Documentary  Film  –  Kembang  7  Rupa (2015)</li>
            <li>Feature  Film  – Pesantren  Impian (2016)</li>
            <li>Short  Film  – Kami  Tidak  Takut (2016)</li>
            <li>Web  Series  –  Murid5 (2016)</li>
        </ul>
    </div>
    <div class="col-md-12 profil-juri cv-juri text-justify">
      <img src="<?php echo base_url();?>assets/img/adrian.jpg" alt="juri" class="img-responsive pull-right" style="margin:20px" width="25%">
      <h2>Adrian Jonathan Pasaribu</h2>
        <p>Writer & Film Journalist<br>
        Salah satu pendiri Cinema Poetica. Ia percaya dialektika tidak saja mengatur hajat hidup warga, tapi juga perkara asmara. Dari 2007 sampai 2010, mondar-mandir sebagai pengurus program di Kinoki, bioskop alternatif di Yogyakarta. Sempat terlibat di filmindonesia.or.id sebagai anggota redaksi, Festival Film Solo sebagai kurator, dan Berlinale Talent Campus 2013 sebagai kritikus film.
        </p>
        <ul>
            <b>PENGALAMAN</b><br>
            <li>Editor in Chief - Cinema Poetica (2010 - Sekarang)</li>
            <li>Writer - filmindonesia.or.id (2011 - 2014)</li>
            <li>Curator - Festival Film Solo (2012 - 2014)</li>
            <li>Editor - Fovea Magazine (2012 - 2013)</li>
            <li>Curator - ARKIPEL International Documentary & Experimental Film Festival (2013)</li>
            <li>Program Manager - Kinoki (2007 - 2010)</li>
        </ul>
        <ul>
            <b>PENDIDIKAN</b><br>
            <ul>
                <li>Universitas Gadjah Mada (UGM) - S1 Communication Study (2011)</li>
                <li>Graduated with a honor degree and a thesis titled "Family Deconstruction in Film Noir: A Narrative Analysis"</li>
            </ul>
        </ul>
</div>
  </div>
</div>
