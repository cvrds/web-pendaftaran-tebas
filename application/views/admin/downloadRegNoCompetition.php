<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Daftar_registrasi_no_lomba.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<html>
<head>
	<title></title>
</head>
<body>

<table border="1" cellpadding="5">
	<thead>
	<tr>
		<th colspan="9">Daftar Registrasi yg Belum Ikut Lomba <br><br></th>
	</tr>
	<tr>
		<th>No</th>
		<th>Email</th>
		<th>Nama</th>
		<th>JK</th>
		<th>TTL</th>
		<th>Alamat</th>
		<th>Instansi</th>
		<th>Telepon</th>
		<th>Twitter</th>
	</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach ($register as $row) { ?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $row->email;?></td>
			<td><?php echo $row->nama;?></td>
			<td><?php echo $row->jk;?></td>
			<td><?php echo $row->tpt_lahir;?>, <?php echo $row->tgl_lahir;?></td>
			<td><?php echo $row->alamat;?></td>
			<td><?php echo $row->instansi;?></td>
			<td><?php echo $row->telp1;?></td>
			<td><?php echo $row->twitter;?></td>
		</tr>
	<?php $no++; }?>
	</tbody>
</table>

</body>
</html>
