<!DOCTYPE html>
<html lang="en">
<head>
      <title>Tebas Award 2017</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">
      <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">
      <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/bootflat.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet" media="screen">

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/js/html5shiv.js"></script>
        <script src="<?php echo base_url();?>assets/js/respond.min.js"></script>
      <![endif]-->
	<style>
	body {background:-webkit-radial-gradient(center center, circle cover, #451394, #100326);}
	h5 {color:#444}
	</style>
</head>
  <body>
    <div class="container" style="max-width:900px;margin:auto;margin-top:10%;background:rgba(231, 231, 231, 0.5);padding:50px 20px;border-radius:8px">
		<div class="col-md-6">
			<img src="<?php echo base_url();?>assets/img/tebas.png" class="img-responsive">
		</div>
		<div class="col-md-6">
		  <form class="form-signin" action="<?php echo site_url('tebasku/login/do_login');?>" method="POST" role="form" style="color:#eee;text-align:center;max-width:300px;margin:auto;padding:20px 10px;border-radius:5px;">

			<?php if($this->session->userdata('notif')){ echo "<h5>".$this->session->userdata('notif')."</h5>"; }?>

			<input type="text" name="username" class="form-control" style="margin-bottom:5px" placeholder="Username" required>
			<input type="password" name="password" class="form-control" placeholder="Password" required>
			<br>
			<button class="btn btn-lg btn-danger btn-block" type="submit">Sign in</button>
		  </form>
		</div>
    </div>

  </body>
</html>
