<div class="col-md-9">
    <h2>EDIT INDIE MOVIE COMPETITION</h2>
	<div style="padding:20px;background:#fff;margin-bottom:20px">

		  <?php if($this->session->flashdata('notif')){ echo $this->session->flashdata('notif'); }?>

		  <form role="form" action="<?php echo site_url('tebasku/invect/edit/'.$id.'/'.$peserta->id_peserta_invect );?>" method="POST">
		  <div class="col-md-5 col-xs-12 pull-left">
			Pilih Kategori
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left">
		  	<?php if($peserta->kategori == "umum"){ $umum = "checked"; $pelajar = ""; }else{ $umum = ""; $pelajar = "checked"; }?>
			<label><input type="radio" name="kategori" <?php echo $umum;?> value="umum" class=""> UMUM </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="kategori" <?php echo $pelajar;?> value="pelajar" class=""> PELAJAR </label>
		  </div>

		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			<label>A. DATA PENDAFTAR</label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			1. Nama
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			2. Instansi/sekolah
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->instansi;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			3. Alamat
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" maxlength="300" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat;?></textarea>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			4. No. Telephone (Utama)
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" value="<?php echo $peserta->telp1;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			5. No. Telephone (Sekunder)
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" value="<?php echo $peserta->telp2;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			6. Email
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="email" value="<?php echo $peserta->email;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			7. Akun Instagram
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award" disabled>
		  </div>
		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			<label>B. DATA FILM</label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			1. Judul Film
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="judul" value="<?php echo $peserta->judul;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			2. Genre Film
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="genre" value="<?php echo $peserta->genre;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			3. Bahasa
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="bahasa" value="<?php echo $peserta->bahasa;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			4.  Durasi
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="durasi" value="<?php echo $peserta->durasi;?>" required="" class="form-control" placeholder="contohnya 15:32">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			5.  Karegori Penonton
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
		  	<?php if($peserta->penonton == "semua"){ $semua = "checked"; $dewasa = ""; }else{ $semua = ""; $dewasa = "checked"; }?>
			<label><input type="radio" name="penonton" class="" <?php echo $semua;?> value="semua" required=""> Semua Usia </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="penonton" class="" <?php echo $dewasa;?> value="+17" required=""> Dewasa (+17) </label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			6. Tahun Produksi
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" name="tahun" value="<?php echo $peserta->tahun;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			7.  Kota/Daerah/Negara Produksi
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="daerah" value="<?php echo $peserta->tpt_produksi;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			8. Produser
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="produser" value="<?php echo $peserta->produser;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			9. Sutradara
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="sutradara" value="<?php echo $peserta->sutradara;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			10. Penulis Naskah
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="naskah" value="<?php echo $peserta->naskah;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			11. Editor
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="editor" value="<?php echo $peserta->editor;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			12. Pemeran Utama
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="pemeran" value="<?php echo $peserta->pemeran;?>" required="" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			13. Pernah Ditayangkan
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
		  	<?php if($peserta->ditayangkan == "pernah"){ $pernah = "checked"; $belum = ""; }else{ $pernah = ""; $belum = "checked"; }?>
			<label><input type="radio" name="penayangan" value="pernah" <?php echo $pernah;?> class=""> Pernah </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="penayangan" value="belum" <?php echo $belum;?> class=""> Belum Pernah </label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			14. Karya Pertama
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
		  	<?php if($peserta->karya_pertama == "ya"){ $ya = "checked"; $tidak = ""; }else{ $ya = ""; $tidak = "checked"; }?>
			<label><input type="radio" name="karyapertama" value="ya" <?php echo $ya;?> class=""> Ya </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="karyapertama" value="tidak" <?php echo $tidak;?> class=""> Tidak </label>
		  </div>
		  <div class="col-md-12 col-xs-12 pull-right h-f1">
			<i class="color-i">Film Lain Yang Pernah Dibuat</i>
		  </div>

		  <div class="col-md-12 col-xs-12 pull-right h-f2">
			<div class="col-md-8 col-xs-8 pull-left h-f2">
			  <input type="text" name="film1" value="<?php echo $peserta->film1;?>" class="form-control" placeholder="Judul Film">
			</div>
			<div class="col-md-4 col-xs-4 pull-left h-f2">
			  <input type="number" name="thn1" value="<?php echo $peserta->tahun1;?>" class="form-control" placeholder="Tahun">
			</div>
		  </div>
		  <div class="col-md-12 col-xs-12 pull-right h-f2">
			<div class="col-md-8 col-xs-8 pull-left h-f2">
			  <input type="text" name="film2" value="<?php echo $peserta->film2;?>" class="form-control" placeholder="Judul Film">
			</div>
			<div class="col-md-4 col-xs-4 pull-left h-f2">
			  <input type="number" name="thn2" value="<?php echo $peserta->tahun2;?>" class="form-control" placeholder="Tahun">
			</div>
		  </div>
		  <div class="col-md-12 col-xs-12 pull-right h-f2">
			<div class="col-md-8 col-xs-8 pull-left h-f2">
			  <input type="text" name="film3" value="<?php echo $peserta->film3;?>" class="form-control" placeholder="Judul Film">
			</div>
			<div class="col-md-4 col-xs-4 pull-left h-f2">
			  <input type="number" name="thn3" value="<?php echo $peserta->tahun3;?>" class="form-control" placeholder="Tahun">
			</div>
		  </div>
		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			15. Kompetisi yang pernah diikuti
		  </div>
		  <div class="col-md-11 col-xs-12 pull-right h-f2">
			<input type="text" name="kompetisi1" value="<?php echo $peserta->kompetisi1;?>" class="form-control">
		  </div>
		  <div class="col-md-11 col-xs-12 pull-right h-f2">
			<input type="text" name="kompetisi2" value="<?php echo $peserta->kompetisi2;?>" class="form-control">
		  </div>
		  <div class="col-md-11 col-xs-12 pull-right h-f2">
			<input type="text" name="kompetisi3" value="<?php echo $peserta->kompetisi3;?>" class="form-control">
		  </div>
		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			16. Prestasi yang pernah dicapai
		  </div>
		  <div class="col-md-11 col-xs-12 pull-right h-f2">
			<input type="text" name="prestasi1" value="<?php echo $peserta->prestasi1;?>" class="form-control">
		  </div>
		  <div class="col-md-11 col-xs-12 pull-right h-f2">
			<input type="text" name="prestasi2" value="<?php echo $peserta->prestasi2;?>" class="form-control">
		  </div>
		  <div class="col-md-11 col-xs-12 pull-right h-f2">
			<input type="text" name="prestasi3" value="<?php echo $peserta->prestasi3;?>" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			17. List Backsound
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" name="backsound" rows="4" style="margin-bottom:10px;"><?php echo $peserta->backsound;?></textarea>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			18. Sinopsis Film
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" name="sinopsis" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"><?php echo $peserta->sinopsis;?></textarea>
		  </div>

		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			<label>C. STATUS</label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left">
			Karya
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left">
		  	<?php if($peserta->berkas == "sudah"){ $sudah = "checked"; $belum = ""; }else{ $sudah = ""; $belum = "checked"; }?>
			<label><input type="radio" name="berkas" <?php echo $sudah;?> value="sudah"> Sudah </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="berkas" <?php echo $belum;?> value="belum"> Belum </label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left">
			Kontribusi
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left">
		  	<?php if($peserta->bayar == "sudah"){ $sudahh = "checked"; $belumm = ""; }else{ $sudahh = ""; $belumm = "checked"; }?>
			<label><input type="radio" name="bayar" <?php echo $sudahh;?> value="sudah"> Sudah </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="bayar" <?php echo $belumm;?> value="belum"> Belum </label>
		  </div>

		  <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
			<button type="submit" name="submit" class="btn btn-success pd-btn"> Update </button>
			&nbsp;&nbsp;&nbsp;
			<a onclick="history.go(-1);"><input type="button" class="btn btn-danger pd-btn" value="Batal"></a>
		  </div>
		  </form>
		  <div class="clearfix"></div>
	</div>
</div>
