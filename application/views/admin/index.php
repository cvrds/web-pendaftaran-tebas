<?php if($this->auth->is_logged_in_admin() == true){?>

<!DOCTYPE html>

<html>

    <head>

      <title>Tebas Award 2017</title>

      <meta name="viewport" content="width=device-width, initial-scale=1.0">

	  <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">

	  <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">

      <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">

      <link href="<?php echo base_url();?>assets/css/bootflat.css" rel="stylesheet" media="screen">

      <link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" media="screen">

      <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" media="screen">

      <link rel="stylesheet" href="<?php echo base_url();?>assets/datatable/jquery.dataTables_themeroller.css" type="text/css">

      <link href="<?php echo base_url();?>assets/datatable/smoothness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
      <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet" media="screen">



      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

      <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

      <script src="<?php echo base_url('assets/datatable/jquery.dataTables.min.js') ?>"></script>

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

      <!--[if lt IE 9]>

        <script src="<?php echo base_url();?>assets/js/html5shiv.js"></script>

        <script src="<?php echo base_url();?>assets/js/respond.min.js"></script>

      <![endif]-->

    </head>

    <body>

<!-- FORM -->

<div id="mainkonten" style="margin-top:0!important" class="admin-page">

  <div class="container">

    <div class="col-md-12">

       <h2 class="caption">ADMIN</h2>

    </div>

    <div class="col-md-12" style="background:rgba(224, 224, 224, 0.5);border-radius:5px">

      <div class="row">

      <div class="col-md-3">

        <h2>MENU</h2>

        <div class="panel-group" id="accordion">

        <div class="panel panel-default">

        <a href="<?php echo site_url('tebasku/beranda');?>" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-home" style="margin-right:5px"></span>Beranda

            </h4>

          </div>

        </a>

        </div>

		<div class="panel panel-default">

        <a href="<?php echo site_url('tebasku/register');?>" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-user" style="margin-right:5px"></span>User Akun Peserta

            </h4>

          </div>

        </a>

        </div>

        <div class="panel panel-default">

        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-film" style="margin-right:5px"></span>Indie Movie Competition

            </h4>

          </div>

        </a>

          <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">

            <div class="panel-body" id="submenu">

              <a href="<?php echo site_url('tebasku/invect/fix');?>">Peserta Fix</a> <br>

              <a href="<?php echo site_url('tebasku/invect/pending');?>">Peserta Tunda</a>

            </div>

          </div>

        </div>

        <div class="panel panel-default">

          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-camera" style="margin-right:5px"></span>Photography Contest

            </h4>

          </div>

          </a>

          <div id="collapseThree" class="panel-collapse collapse" style="height: 0px;">

            <div class="panel-body" id="submenu">

              <a href="<?php echo site_url('tebasku/fotografi/fix');?>">Peserta Fix</a> <br>

              <a href="<?php echo site_url('tebasku/fotografi/pending');?>">Peserta Tunda</a>

            </div>

          </div>

        </div>

        <div class="panel panel-default">

		  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-picture" style="margin-right:5px"></span>Battle Of Digital Illustration

            </h4>

          </div>

          </a>

          <div id="collapseFour" class="panel-collapse collapse" style="height: 0px;">

            <div class="panel-body" id="submenu">

              <a href="<?php echo site_url('tebasku/bodi/fix');?>">Peserta Fix</a> <br>

              <a href="<?php echo site_url('tebasku/bodi/pending');?>">Peserta Tunda</a>

            </div>

          </div>

        </div>

		<div class="panel panel-default">

		  <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-hdd" style="margin-right:5px"></span>Karya Peserta Tebas

            </h4>

          </div>

          </a>

          <div id="collapseFive" class="panel-collapse collapse" style="height: 0px;">

            <div class="panel-body" id="submenu">

              <a href="<?php echo site_url('tebasku/karya/karyaphoto');?>">Photography Contest</a> <br>

              <a href="<?php echo site_url('tebasku/karya/karyabodi');?>">Battle Of Digital Illustration</a><br>
              <a href="<?php echo site_url('tebasku/karya/karyainvect');?>">Indie Movie Competition</a>

            </div>

          </div>

        </div>

        <div class="panel panel-default">

		  <a href="<?php echo site_url('tebasku/login/logout');?>" class="collapsed">

          <div class="panel-heading">

            <h4 class="panel-title">

                <span class="glyphicon glyphicon-off" style="margin-right:5px"></span>Logout

            </h4>

          </div>

          </a>

        </div>

      </div>

      </div>

	  <!-- DYNAMIC CONTENT -->

      <?php echo $contents; ?>

	  <!-- DYNAMIC CONTENT -->

    </div>

    </div>

		<div class="clearfix"></div>

		<p style="color:#fae442;margin:20px 0">Copyright &copy; 2017. Komunitas Multimedia Amikom. All Right Reserved</p>

  </div>

</div>





      <!-- Include all compiled plugins (below), or include individual files as needed -->

      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>



    </body>

</html>

<?php }else{ redirect('tebasku/login'); }?>
