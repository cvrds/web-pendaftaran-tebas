<?php

$invect_fix = count($invect_fix); $invect_pending = count($invect_pending);

$fotografi_fix = count($fotografi_fix); $fotografi_pending = count($fotografi_pending);

$bodi_fix = count($bodi_fix); $bodi_pending = count($bodi_pending);

?>

<div class="col-md-9">

    <h2>STATISTIC</h2>

	<div class="row">

		<div class="col-md-4">

			<div class="ads" style="margin-bottom:10px">

			<h4>Peserta INVECT</h4>

			<b>Total : <?php echo $invect_fix+$invect_pending;?></b><br><br>

			<center>

			<table style="text-align:left">

				<tr><td>Peserta FIX</td><td>:&nbsp;</td>

					<td><?php echo $invect_fix;?> org</td>

				</tr>

				<tr><td>Peserta Pending &nbsp;</td><td>:&nbsp;</td>

					<td><?php echo $invect_pending;?> org</td>

				</tr>

			</table>

			</center>

			<br>

			<a href="<?php echo site_url('tebasku/beranda/downloadInvect');?>">

			<button type="button" class="btn btn-danger btn-sm" style="margin-bottom:5px">Download Peserta Fix</button></a>

			<br>

			<a href="<?php echo site_url('tebasku/beranda/downloadInvectPending');?>">

			<button type="button" class="btn btn-danger btn-sm">Download Peserta Tunda</button></a>

			<br><br>

			</div>

		</div>

		<div class="col-md-4">

			<div class="ads" style="margin-bottom:10px">

			<h4>Peserta PHOTOGRAPHY CONTEST</h4>

			<b>Total : <?php echo $fotografi_fix+$fotografi_pending;?></b><br><br>

			<center>

			<table style="text-align:left">

				<tr><td>Peserta FIX</td><td>:&nbsp;</td>

					<td><?php echo $fotografi_fix;?> org</td>

				</tr>

				<tr><td>Peserta Pending &nbsp;</td><td>:&nbsp;</td>

					<td><?php echo $fotografi_pending;?> org</td>

				</tr>

			</table>

			</center>

			<br>

			<a href="<?php echo site_url('tebasku/beranda/downloadFotografi');?>">

			<button type="button" class="btn btn-danger btn-sm" style="margin-bottom:5px">Download Peserta Fix</button></a>

			<br>

			<a href="<?php echo site_url('tebasku/beranda/downloadFotografiPending');?>">

			<button type="button" class="btn btn-danger btn-sm">Download Peserta Tunda</button></a>

			<br><br>

			</div>

		</div>

		<div class="col-md-4">

			<div class="ads" style="margin-bottom:10px">

			<h4>Peserta DIGITAL ILLUSTRATION</h4>

			<b>Total : <?php echo $bodi_fix+$bodi_pending;?></b><br><br>

			<center>

			<table style="text-align:left">

				<tr><td>Peserta FIX</td><td>:&nbsp;</td>

					<td><?php echo $bodi_fix;?> org</td>

				</tr>

				<tr><td>Peserta Pending &nbsp;</td><td>:&nbsp;</td>

					<td><?php echo $bodi_pending?> org</td>

				</tr>

			</table>

			</center>

			<br>

			<a href="<?php echo site_url('tebasku/beranda/downloadBodi');?>">

			<button type="button" class="btn btn-danger btn-sm" style="margin-bottom:5px">Download Peserta Fix</button></a>

			<br>

			<a href="<?php echo site_url('tebasku/beranda/downloadBodiPending');?>">

			<button type="button" class="btn btn-danger btn-sm">Download Peserta Tunda</button></a>

			<br><br>

			</div>

		</div>

	</div>

	<div class="row">

		<div class="col-md-12">

			<a href="<?php echo site_url('tebasku/beranda/downloadRegisterNoCompetition');?>">

			<button class="btn btn-danger btn-block">

				Download Data Register yg Belum Ikut Lomba Sama Sekali

			</button>

			</a>

		</div>

	</div>

</div>
