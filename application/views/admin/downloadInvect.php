<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Daftar_peserta_invect_fix.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<html>
<head>
	<title></title>
</head>
<body>

<table border="1" cellpadding="5">
	<thead>
	<tr>
		<th colspan="30">Daftar Peserta Invect Fix <br><br></th>
	</tr>
	<tr>
		<th>No</th>
		<th>No.Pendf</th>
		<th>Email</th>
		<th>Nama</th>
		<th>JK</th>
		<th>TTL</th>
		<th>Alamat</th>
		<th>Instansi</th>
		<th>Telepon</th>
		<th>Twitter</th>
		<th>Kategori</th>
		<th>Judul</th>
		<th>Genre</th>
		<th>Bahasa</th>
		<th>Durasi</th>
		<th>Penonton</th>
		<th>Thn Produksi</th>
		<th>Tmpt Produksi</th>
		<th>Produser</th>
		<th>Sutradara</th>
		<th>Naskah</th>
		<th>Editor</th>
		<th>Pemeran</th>
		<th>Pernah Tayang</th>
		<th>Karya Pertama</th>
		<th>Film Lain</th>
		<th>Kompetisi Lain</th>
		<th>Prestasi</th>
		<th>Backsound</th>
		<th>Sinopsis</th>
	</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach ($peserta as $row) { ?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $row->no_pendf;?></td>
			<td><?php echo $row->email;?></td>
			<td><?php echo $row->nama;?></td>
			<td><?php echo $row->jk;?></td>
			<td><?php echo $row->tpt_lahir;?>, <?php echo $row->tgl_lahir;?></td>
			<td><?php echo $row->alamat;?></td>
			<td><?php echo $row->instansi;?></td>
			<td><?php echo $row->telp1;?></td>
			<td><?php echo $row->twitter;?></td>
			<td><?php echo $row->kategori;?></td>
			<td><?php echo $row->judul;?></td>
			<td><?php echo $row->genre;?></td>
			<td><?php echo $row->bahasa;?></td>
			<td><?php echo $row->durasi;?></td>
			<td><?php echo $row->penonton;?></td>
			<td><?php echo $row->tahun;?></td>
			<td><?php echo $row->tpt_produksi;?></td>
			<td><?php echo $row->produser;?></td>
			<td><?php echo $row->sutradara;?></td>
			<td><?php echo $row->naskah;?></td>
			<td><?php echo $row->editor;?></td>
			<td><?php echo $row->pemeran;?></td>
			<td><?php echo $row->ditayangkan;?></td>
			<td><?php echo $row->karya_pertama;?></td>
			<td><?php echo "$row->film1 ($row->tahun1), $row->film2 ($row->tahun2), $row->film3 ($row->tahun3)";?></td>
			<td><?php echo "$row->kompetisi1, $row->kompetisi2, $row->kompetisi3";?></td>
			<td><?php echo "$row->prestasi1, $row->prestasi2, $row->prestasi3";?></td>
			<td><?php echo $row->backsound;?></td>
			<td><?php echo $row->sinopsis;?></td>
		</tr>
	<?php $no++; }?>
	</tbody>
</table>

</body>
</html>
