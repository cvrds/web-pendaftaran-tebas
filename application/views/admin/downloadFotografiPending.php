<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Daftar_peserta_fotografi_pending.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<html>
<head>
	<title></title>
</head>
<body>

<table border="1" cellpadding="5">
	<thead>
	<tr>
		<th colspan="12">Daftar Peserta Fotografi Pending <br><br></th>
	</tr>
	<tr>
		<th>No</th>
		<th>No.Pendf</th>
		<th>Email</th>
		<th>Nama</th>
		<th>JK</th>
		<th>TTL</th>
		<th>Alamat</th>
		<th>Instansi</th>
		<th>Telepon</th>
		<th>Twitter</th>
		<th>Kamera</th>
		<th>Bayar</th>
	</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach ($peserta as $row) { ?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $row->no_pendf;?></td>
			<td><?php echo $row->email;?></td>
			<td><?php echo $row->nama;?></td>
			<td><?php echo $row->jk;?></td>
			<td><?php echo $row->tpt_lahir;?>, <?php echo $row->tgl_lahir;?></td>
			<td><?php echo $row->alamat;?></td>
			<td><?php echo $row->instansi;?></td>
			<td><?php echo $row->telp1;?></td>
			<td><?php echo $row->twitter;?></td>
			<td><?php echo $row->kamera;?></td>
			<td><?php echo $row->bayar;?></td>
		</tr>
	<?php $no++; }?>
	</tbody>
</table>

</body>
</html>
