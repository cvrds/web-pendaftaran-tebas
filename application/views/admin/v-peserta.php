<script type="text/javascript" charset="utf-8">
  var oTable;

  $(document).ready(function() {
    oTable = $('#peserta').dataTable({
    "sPaginationType": "full_numbers",
    "bJQueryUI": true,
    "bSortClasses": false,
    "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
    "iDisplayLength": 25,
    });
  });
</script>
<div class="col-md-9" style="padding-bottom:20px">
    <h2>User Akun Peserta</h2>
	<div class="row">
		<div class="col-md-12">
			<table id="peserta">
    			<thead>
    				<tr>
    					<th>No</th>
    					<th>Email</th>
    					<th>Nama</th>
    					<th>No. Hp</th>
    					<th>Panel</th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php $no = 1; foreach ($peserta as $row) { ?>
    					<tr>
    						<td><?php echo $no;?></td>
    						<td><?php echo $row->email;?></td>
    						<td><?php echo $row->nama;?></td>
    						<td><?php echo $row->telp1;?></td>
    						<td>
    							<a href="<?php echo site_url('tebasku/register/edit/'.$row->id_registrasi);?>">
    								<span class="glyphicon glyphicon-pencil"></span></a>
    							<a href="<?php echo site_url('tebasku/register/hapus/'.$row->id_registrasi);?>" onClick="javascript:return confirm('Anda yakin ingin menghapusnya?');">
    								<span class="glyphicon glyphicon-remove"></span></a>
    						</td>
    					</tr>
    				<?php $no++; }?>
    			</tbody>
    		</table>
		</div>
	</div>
</div>
