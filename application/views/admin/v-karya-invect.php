<script type="text/javascript" charset="utf-8">
  var oTable;

  $(document).ready(function() {
    oTable = $('#peserta').dataTable({
    "sPaginationType": "full_numbers",
    "bJQueryUI": true,
    "bSortClasses": false,
    "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
    "iDisplayLength": 25,
    });
  });
</script>
<div class="col-md-9" style="padding-bottom:20px">
    <h2>Karya Invect</h2>
	<div class="row">
		<div class="col-md-12">
			<table id="peserta">
    			<thead>
    				<tr>
    					<th>No</th>
    					<th>No Peserta</th>
    					<th>Nama</th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php $no = 1; foreach ($karya_invect as $row) { ?>
    					<tr>
    						<td><?php echo $no;?></td>
    						<td><?php echo $row->no_pend;?></td>
    						<td><?php echo $row->nama;?></td>
    					</tr>
    				<?php $no++; }?>
    			</tbody>
    		</table>
		</div>
	</div>
</div>
