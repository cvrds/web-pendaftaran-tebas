<div class="col-md-9">
    <h2>EDIT BATTLE OF DIGITAL ILLUSTRATION</h2>
	<div style="padding:20px;background:#fff;margin-bottom:20px">

		<?php if($this->session->flashdata('notif')){ echo $this->session->flashdata('notif'); }?>

		<form action="<?php echo site_url('tebasku/bodi/edit/'.$id.'/'.$peserta->id_peserta_bodi );?>" method="POST">
		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			<label>A. DATA PENDAFTAR</label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			1. Nama
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			2. Tempat Tanggal Lahir
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->tpt_lahir;?>" class="form-control" placeholder="Tempat Lahir" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->tgl_lahir;?>" class="form-control" placeholder="1992/12/23" id="dp" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			3. Alamat
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" maxlength="300" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat;?></textarea>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			4. Instansi/sekolah
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->instansi;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			5. No. Telephone (Utama)
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" value="<?php echo $peserta->telp1;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			6. No. Telephone (Sekunder)
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" value="<?php echo $peserta->telp2;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			7. Email
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="email" value="<?php echo $peserta->email;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			8. Akun Instagram
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			9. Judul Karya
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="judul" required="" value="<?php echo $peserta->judul;?>" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			10. Deskripsi Karya
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" name="deskripsi" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"><?php echo $peserta->deskripsi;?></textarea>
		  </div>
		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			<label>B. STATUS</label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left">
			Karya
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left">
		  	<?php if($peserta->berkas == "sudah"){ $sudah = "checked"; $belum = ""; }else{ $sudah = ""; $belum = "checked"; }?>
			<label><input type="radio" name="berkas" <?php echo $sudah;?> value="sudah"> Sudah </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="berkas" <?php echo $belum;?> value="belum"> Belum </label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left">
			Kontribusi
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left">
		  	<?php if($peserta->bayar == "sudah"){ $sudahh = "checked"; $belumm = ""; }else{ $sudahh = ""; $belumm = "checked"; }?>
			<label><input type="radio" name="bayar" <?php echo $sudahh;?> value="sudah"> Sudah </label>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<label><input type="radio" name="bayar" <?php echo $belumm;?> value="belum"> Belum </label>
		  </div>

		  <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
			<button type="submit" name="submit" class="btn btn-success pd-btn"> Update </button>
			&nbsp;&nbsp;&nbsp;
			<a onclick="history.go(-1);"><input type="button" class="btn btn-danger pd-btn" value="Batal"></a>
		  </div>

		  </form>

		  <div class="clearfix"></div>
	</div>
</div>
