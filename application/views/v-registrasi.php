<!--  HEADER -->
<div id="header">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%; visibility:hidden;" alt="header">
  <div class="container header">
<!-- <div class="thumb">
    <a href="#">
        <span>THE BEST ANNUAL MULTIMEDIA SHOW 2017</span>
    </a>
</div> -->
 <!--   <div class="col-md-12">
          <center><img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive"></center>
    </div>
  -->
    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
        <?php if($this->auth->is_logged_in() == false){?>
        <h1 style="color:#FFF">PILIH LOMBA YANG INGIN KAMU IKUTI DIBAWAH INI</h1>
<!--
          <h1 style="color:#FFF;">LIHAT VIDEO TUTORIAL REGISTRASI TEBAS 2017</h1>
          <p style="color:#FFF;">Silahkan klik untuk melihat video prosedur pendaftaran TEBAS 2017 lalu pilih lomba yang ingin kamu ikuti dibawah ini</p>
-->
        <?php }else{ ?>
          <h1 style="color:#FFF;">SELAMAT DATANG <a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($this->session->userdata('id_registrasi')));?>" style="color:#FFF" title="Lihat Profil"><b><?php echo strtoupper($this->session->userdata('nama'));?></b></a></h1>
          <h2 style="color:#FFF">SIILAHKAN PILIH LOMBA YANG INGIN KAMU IKUTI</h2>
<!--          <p style="color:#FFF;">Silahkan klik untuk melihat video prosedur pendaftaran TEBAS 2017 lalu pilih lomba yang ingin kamu ikuti dibawah ini</p>-->
        <?php }?>
<!--        <a class='youtube' href="https://www.youtube.com/embed/VuDyF5AraSU" ><button type="button" class="btn btn-danger btn-lg">LIHAT DISINI</button></a>-->
    </div>
  </div>
  <br>
  <div id="turun-limit"></div>
  <div class="text-center">
    <a href="#turun-limit" title="Turun" class="totop"><span class="glyphicon glyphicon-circle-arrow-down" style="margin-top:3px"></span></a>
  </div>
  <br>
</div>
<!--  KOMPETISI -->
<div id="competition">
  <div class="container">
      <h2 class="caption judul-c">REGISTRATION</h2>
      <a href="<?php echo site_url('kompetisi/invect');?>">
      <div class="col-md-4 icon">
        <img src="<?php echo base_url();?>assets/img/i-invect.png" alt="invect" class="img-responsive">
        <h2>Indie Movie Competition</h2>
        <p>Karya yang dihasilkan bertemakan bebas tapi bukan film dokumenter.</p>
        <button type="button" class="btn btn-lomba btn-sm">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/fotografi');?>">
      <div class="col-md-4 icon lombapc">
        <img src="<?php echo base_url();?>assets/img/i-pc.png" alt="invect" class="img-responsive">
        <h2>Photography Contest</h2>
        <p>Karya yang dihasilkan menggambarkan suasana permainan anak - anak pada era 90'an...</p>
        <button type="button" class="btn btn-lomba btn-sm">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/poster');?>">
      <div class="col-md-4 icon lombabodi">
          <img src="<?php echo base_url();?>assets/img/i-bodi.png" alt="invect" class="img-responsive">
          <h2>Battle Of Digital Illustration</h2>
          <p>Karya yang dihasilkan merupakan illustrasi dari suatu mainan. Contohnya kelereng...</p>
          <button type="button" class="btn btn-lomba btn-sm">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
  </div>
</div>
