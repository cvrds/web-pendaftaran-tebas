<!DOCTYPE html>
<html>
    <head>
      <title><?php echo $title; ?></title>
	<meta name="description" content="TEBAS (The Best Annual Multimedia Show) lomba multimedia akbar dari Jogjakarta hadir kembali di tahun 2017 dengan tema Semangat Kreasi Muda. More...">
	<meta name="keywords" content="tebas, koma, media koma, amikom, indie movie competition, battle of digital illustration, rally photo, lomba, film, fotografi, desain, yogyakarta">
	<meta name="google-site-verification" content="FKA01-3YgMV0UXK-VqCiB-uDKNVhv6NcuHk4NbZUaJU" />
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-49356875-1', 'tebasaward.com');
	  ga('send', 'pageview');
	</script>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.png" type="image/png">
	  <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.png" type="image/png">
      <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/bootflat.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/colorbox.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
      <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/style2.css" rel="stylesheet" media="screen">
      <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">




	  <!--FONT AWESOME-->
	  <link href="<?php echo base_url();?>assets/fontawesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/js/html5shiv.js"></script>
        <script src="<?php echo base_url();?>assets/js/respond.min.js"></script>
      <![endif]-->
      <!-- TAMBAHAN -->
      <script src="<?php echo base_url();?>assets/js/html5shiv.js"></script>
      <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
              <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      html, body {

  height: 100%;

}

.svg-wrapper {
  position: relative;
  transform: translateY(-50%);
  width: 220px;
}

.shape {
  fill: transparent;
  stroke-dasharray: 140 540;
  stroke-dashoffset: -320;
  stroke-width: 8px;
  stroke: white;
}

.text {
  color: #fff;
  font-family: "arista",sans-serif ;
  text-align: center;
  line-height: 32px;
  position: relative;
  font-style: h1;
  top: -48px;
}

@keyframes draw {
  0% {
    stroke-dasharray: 140 540;
    stroke-dashoffset: -320;
    stroke-width: 8px;
  }
  100% {
    stroke-dasharray: 760;
    stroke-dashoffset: 0;
    stroke-width: 2px;
  }
}

.svg-wrapper:hover .shape {
  -webkit-animation: 0.5s draw linear forwards;
  animation: 0.5s draw linear forwards;
}
    </style>


    <script src="<?php echo base_url();?>assets/js/prefixfree.min.js"></script>
    <!-- TAMBAHAN -->
    </head>
    <body>
<!--  MAIN NAV -->
<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="<?php echo base_url(); ?>" class="navbar-brand">

      <div class="svg-wrapper">
        <svg height="60" width="220" xmlns="http://www.w3.org/2000/svg">
        <rect class="shape" height="60" width="220" />
        </svg>
         <div class="text">TEBAS AWARD</div>
      </div>

      </a>
    </div>
    <nav class="navbar-collapse bs-navbar-collapse collapse" role="navigation" style="height: 1px;">
      <ul class="nav navbar-nav navbar-right">
        <li <?php echo $nav['m_beranda'];?>><a href="<?php echo base_url(); ?>">BERANDA</a></li>


        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">KOMPETISI <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('kompetisi/invect');?>">Indie Movie Competition</a></li>
            <li><a href="<?php echo site_url('kompetisi/fotografi');?>">Photography Contest</a></li>
            <li><a href="<?php echo site_url('kompetisi/poster');?>">Battle Of Digital Illustration</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">DEWAN JURI <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('juri/invect');?>">Indie Movie Competition</a></li>
            <li><a href="<?php echo site_url('juri/fotografi');?>">Photography Contest</a></li>
            <li><a href="<?php echo site_url('juri/poster');?>">Battle Of Digital Illustration</a></li>
          </ul>
        </li>
        <li <?php echo $nav['m_arsip'];?>><a href="<?php echo site_url('arsip');?>">GALLERI</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">INFO <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('jadwal');?>">JADWAL</a></li>
            <li><a href="<?php echo site_url('faq');?>">FAQ</a></li>
            <li><a href="<?php echo site_url('kontak');?>">KONTAK</a></li>
          </ul>
        </li>
        <li <?php echo $nav['m_regis'];?>><a href="<?php echo site_url('registrasi');?>">REGISTRASI </a></li>
        <?php if($this->auth->is_logged_in() == false){?>
        <li class="dropdown">
                        <a href="http://phpoll.com/login" class="dropdown-toggle" data-toggle="dropdown">MASUK <b class="caret"></b></a>
                        <ul class="dropdown-menu dropdownlogin dropdown-lr animated slideInRight" role="menu">
                            <div class="col-lg-12">
                                <div class="text-center" style="color:white;"><h3><b>MASUK</b></h3></div>
                                <form class="form-signin" action="<?php echo site_url('beranda/login');?>" method="POST" role="form">
                                    <div class="form-group">
                                        <label for="username" style="color:white; font-family:arial;">Email address</label>
                                        <input type="email" name="text" id="username" tabindex="1" class="form-control" placeholder="Email address" value="" autocomplete="off" required autofocus>
                                    </div>

                                    <div class="form-group">
                                        <label for="password" style="color:white; font-family:arial;">Password</label>
                                        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off" required>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-5 pull-right">
                                                <button class="btn btn-lg btnlogin btn-block" type="submit">Masuk</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </ul>
                    </li>
                    <?php } ?>
        <?php if($this->auth->is_logged_in() == true){?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: rgba(0,204,255,0.3);">PROFIL<b class="caret"></b></a>
          <ul class="dropdown-menu drop-login">
            <li><a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($this->session->userdata('id_registrasi')));?>">Kompetisi</a></li>
            <li><a href="<?php echo site_url('profil/edit/'.tebas_encrypt($this->session->userdata('id_registrasi')));?>">Edit Profil</a></li>
            <li><a href="<?php echo site_url('profil/upload/'.tebas_encrypt($this->session->userdata('id_registrasi')));?>">Upload Karya</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo site_url('beranda/logout');?>">Logout</a></li>
          </ul>
        </li>
        <?php } ?>
      </ul>
    </nav>
  </div>
</header><!-- END MAIN NAV -->

<!-- DYNAMIC CONTENT -->
<?php echo $contents; ?>
<!-- DYNAMIC CONTENT -->

<!-- <div class="footersegitiga"></div> -->

<!--  FOOTER -->
<footer>
  <div class="container">
    <div class="col-md-12" style="margin-bottom:40px">
      <a href="#" target="blank"><a href="http://facebook.com/tebasaward" target="blank"><img src="<?php echo base_url();?>assets/img/sosmed/facebook.png" alt="sosmed" class="sosmed"></a></a>
      <a href="#" target="blank"><a href="http://twitter.com/tebasaward" target="blank"><img src="<?php echo base_url();?>assets/img/sosmed/twitter.png" alt="sosmed" class="sosmed"></a></a>
      <a href="#" target="blank"><a href="http://www.youtube.com/user/MediaVideoKoma" target="blank"><img src="<?php echo base_url();?>assets/img/sosmed/youtube.png" alt="sosmed" class="sosmed"></a></a>
      <a href="#" target="blank"><a href="http://koma.or.id" target="blank"><img src="<?php echo base_url();?>assets/img/sosmed/soskoma.png" alt="sosmed" class="sosmed"></a></a>
      <br><br>
      <p>Sekretariat : Gedung BSC Lt. 3 Ruang VI.3.6<br>Kampus Terpadu Universitas AMIKOM Yogyakarta<br>Jl. Ringroad Utara Condong Catur Yogyakarta. Telp (0274) 884201</p>
      <p style="color:rgb(251, 230, 41);">Copyright &copy; 2017. Komunitas Multimedia Amikom. All Right Reserved</p>
    </div>
	<a href="javascript:void(0);" onclick="jQuery('html, body').animate( { scrollTop: 0 }, 'slow' );" data-original-title="" title="Back to Top" class="totop">
		<span class="glyphicon glyphicon-chevron-up"></span>
	</a>
  </div>
</footer><!-- END FOOTER -->

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
      <script src="<?php echo base_url();?>assets/js/jquery.colorbox.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/init.js"></script>

      <script>
          $(document).ready(function(){
            $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
            $(".group1").colorbox({rel:'group1', maxWidth:800, maxHeight:600});
          });
      </script>

      <script type="text/javascript">
        $(document).ready(function() {
        $('#Carousel').carousel({
        interval: 4000
        })

          $('#Carousel').on('slid.bs.carousel', function() {
            //alert("slid");
        });
      });
      </script>

      <script type="text/javascript">
        $(document).ready(function() {
        $('#CarouselJuri').carousel({
        interval: 4000
        })

          $('#CarouselJuri').on('slid.bs.carousel', function() {
            //alert("slid");
        });
      });
      </script>

      <?php if($this->uri->segment(1) == "" || $this->uri->segment(1) == "beranda" || $this->uri->segment(1) == "registrasi"){?>
      <script type="text/javascript">
        $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 500);
              return false;
            }
          }
        });
      });
      </script>
      <?php }?>

      <script type="text/javascript">
        $('#dp').datepicker({
        format: 'dd/mm/yyyy'
        })
      </script>
      <script>$('#collapseTwo1').collapse('show')</script>
      <script type="text/javascript">
        //switch uploads or link
         $('#karya').hide();
         $('#trailer').hide();
         $('#kategori').hide();
         $("#pilihkarya").change(function () {
            var pilihkarya = this.value;
            switch(pilihkarya){
                case 'photography':
                case 'bodi':
                    $('#karya').hide();
                    $('#trailer').hide();
                    $('#kategori').hide();
                break;
                case 'invect':
                    $('#karya').show();
                    $('#trailer').show();
                    $('#kategori').show();
                break;
            }
        });
    </script>
    </body>
</html>
