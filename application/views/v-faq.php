<!-- FORM -->
<div id="mainkontenjuri">
<div class="container">
    <div class="col-md-12">
     <h2 class="caption" style="color:#000">FAQ</h2>
    </div>
    <div class="col-md-6">
    <ul class="faq">
      <li>
          <h3>Apa itu <b>TEBAS?</b></h3>
          <p>TEBAS singkatan dari The Best Annual Multimedia Show. Merupakan ajang bergengsi bagi pecinta multimedia di Indonesia pada umumnya untuk berkompetisi dalam kreativitas karya seni. Tema TEBAS tahun ini adalah <b>"RAGAM MEMORI KREATIVITAS"</b>.</p>
      </li>
      <li>
          <h3>Kategori <b>lomba</b> TEBAS apa saja?</h3>
          <ul>
            <li>
              <h4>Indie Movie Competition</h4>
              Tema bebas (non dokumenter)<br>
              Kategori Peserta : Pelajar atau Umum <br>
              Deskripsi : <br>Karya yang dihasilkan bertemakan bebas tapi bukan film dokumenter. Diharapkan, hasil karya ini mengandung pesan untuk yang melihatnya dan dapat tersampaikan dengan baik. <br>
              Info lebih lanjut lihat <a href="<?php echo site_url('kompetisi/invect');?>"><b>disini</b></a>
            </li>
            <li>
              <h4>Photography Contest</h4>
              Tema : Aku dan Permainan masa kecilku<br>
              Kategori Peserta : Umum <br>
              Deskripsi : <br>Karya yang dihasilkan menggambarkan suasana permainan anak - anak pada era 90'an yang kini mulai di tinggalkan.<br>
              Info lebih lanjut lihat <a href="<?php echo site_url('kompetisi/fotografi');?>"><b>disini</b></a>
            </li>
            <li>
              <h4>Battle of Digital Illustration</h4>
              Tema : Mainan Masa Kecilku<br>
              Kategori Peserta : Umum <br>
              Deskripsi : <br>Karya yang dihasilkan merupakan illustrasi dari suatu mainan.<br>
              Info lebih lanjut lihat <a href="<?php echo site_url('kompetisi/bodi');?>"><b>disini</b></a>
            </li>
          </ul>
      </li>
    <li>
          <h3>Berapa <b>biaya</b> pendaftaran untuk mengikuti kompetisi TEBAS</h3>
          <p>
            <table>
              <tr>
                <td>Photography Contest</td><td>&nbsp;:&nbsp;</td><td>Rp. 20.000,-</td>
              </tr>
              <tr>
                <td>Indie Movie Competition</td><td>&nbsp;:&nbsp;</td>
              </tr>
              <tr>
                <td>- Umum</td><td>&nbsp;:&nbsp;</td><td>Rp. 65.000,-</td>
              </tr>
              <tr>
                <td>- Pelajar</td><td>&nbsp;:&nbsp;</td><td>Rp. 55.000,-</td>
              </tr>
              <tr>
                <td>Battle Of Digital Illustration &nbsp;</td><td>&nbsp;:&nbsp;</td><td>Rp. 20.000 ,-</td>
              </tr>
            </table>
          </p>
      </li>
      <li>
          <h3>Siapa <b>juri</b> dalam kompetisi TEBAS?</h3>
          <ul>
            <li>
              <h4>Indie Movie Competition</h4>
              - <a href="http://tebasaward.com/juri/invect">Triyanto "Genthong" Hapsoro</a><br>
              - <a href="http://tebasaward.com/juri/invect">Gregorius Arya Dhipayana</a><br>
              - <a href="http://tebasaward.com/juri/invect">Adrian Jonathan Pasaribu</a><br>
         </li>
            <li>
              <h4>Photography Contest</h4>
               - <a href="http://tebasaward.com/juri/fotografi">Risman Marah</a><br>
              - <a href="http://tebasaward.com/juri/fotografi">Misbachul Munir</a><br>
              - <a href="http://tebasaward.com/juri/fotografi">Budi Prast</a><br>
            </li>
            <li>
              <h4>Battle of Digital Illustration</h4>
              - <a href="http://tebasaward.com/juri/poster">Rahmat Tri Basuki</a><br>
              - <a href="http://tebasaward.com/juri/poster">Rahmat Kurniawan</a><br>
              - <a href="http://tebasaward.com/juri/poster">Dedi Rokkinvisual</a><br>
            </li>
          </ul>
      </li>
    </ul>
    </div>
    <div class="col-md-6">
    <ul class="faq">
      <li>
          <h3><b>Kapan</b> pendaftaran TEBAS berlangsung?</h3>
          <p>Pendaftaran pada 13 Maret 2017 s/d 07 Mei 2017</p>
      </li>
      <li>
          <h3>Bagaimana jika saya mendaftar terlebih dahulu, tetapi <b>karyanya menyusul</b>?</h3>
           <p>Boleh, batas pengumpulan karya sampai 07 Mei 2017</p>
      </li>
      <li>
          <h3>Bagaimana cara <b>upload karya</b> ?</h3>
          <p>Bisa langsung klik link berikut tanpa harus login terlebih dahulu <a href="http://tebasaward.com/upload/">TEBAS UPLOAD</a></p>
          <p>Atau bisa dengan login akun dan pilih menu <b>Upload Karya</b></p>
      </li>
      <li>
          <h3>Bagaimana cara <b>mendaftar</b> kompetisi TEBAS?</h3>
          <p>Terdapat 2 cara pendaftaran :</p>
          <h4>Online</h4>
          <ul>
            <li>Kunjungi web <a href="http://tebasaward.com"><b>www.tebasaward.com</b></a></li>
            <li>Pilih kategori lomba</li>
            <li>Isi formulir sesuai ketentuan & syarat pendaftaran</li>
            <li>Peserta akan mendapatkan file bukti pendaftaran, lalu cetak file tersebut</li>
            <li>Pengumpulan karya terdiri dari Surat Pernyataan Keorisinilan Karya, Tanda Bukti Pendaftaran, Karya (sesuai dengan persyaratan dan ketentuan), biaya pendaftaran / bukti pendaftaran, photo copy identitas, dan surat rekomendasi (khusus kategori pelajar)</li>
            <li>Kirim ke alamat Gedung BSC Lt. 3 Ruang VI.3.6 Jl. Ringroad Utara Condong Catur Yogyakarta. Telp (0274) 884201</li>
          </ul>
          <h4>Offline</h4>
          <p>Datang langsung ke kesekretariatan TEBAS (Sekretariat : Gedung BSC Lt. 3 Ruang VI.3.6 Kampus Terpadu Universitas AMIKOM Yogyakarta. Jl. Ringroad Utara Condong Catur Yogyakarta.) dan mengisi Form Pendaftaran.</p>
      </li>
      <li>
          <h3><b>Acara</b> TEBAS itu apa saja?</h3>
          <ul>
            <li>
              <h4>SHOW UP TEBAS (13-14 Mei 2017)</h4>
              <p>Seluruh karya dari peserta TEBAS akan kami tampilkan dalam bentuk pameran karya, yaitu SHOW UP TEBAS 2017 yang akan berlangsung pada tanggal 13-14 Mei 2017 di Loop Station Yogya. Seluruh karya dari Battle of Digital Illustration ditampilkan secara nyata dan menarik bersama seluruh karya dari peserta Photography Contest. Sedangkan film terbaik peserta INVECT ditampilkan dalam ruang screening dan terjadwal.</p>
              <p>Pemilihan karya terfavorit dilakukan secara polling like di <a href="https://www.facebook.com/TEBASAWARD/">Fanspage TEBAS</a> dan <a href="https://www.youtube.com/channel/UCfkirEZV_maYMW6LDC5x_Kw"> Youtube TEBAS</a> mulai tanggal 13-14 Mei 2017. Dan polling oleh pengunjung SHOW UP TEBAS 2017. Pameran karya TEBAS ini dibuka untuk umum (free), diramaikan oleh beberapa perform akustik dan pembagian beberapa doorprize untuk membuat acara semakin semarak.</p>
            </li>
            <li>
              <h4>TEBAS Awards (22 Mei 2017)</h4>
              <p>Pemenang lomba The Best Annual Multimedia Show akan diumumkan secara langsung pada acara penganugrahan kepada karya-karya terbaik dari setiap kategori lomba TEBAS yang akan kami kemas secara special, unik dan menarik di TEBAS AWARDS 2017 yang dilaksanakan pada tanggal 22 Mei 2017 di Taman Budaya Yogyakarta.</p>

            </li>
          </ul>
      </li>
      <li>
          <h3>Apakah saya bisa mengikuti <b>lebih dari 1 lomba</b> atau lebih dari 1 karya?</h3>
          <p>Boleh, untuk pendaftaran harga menyesuaikan lomba yang diikuti dan biaya per Karya</p>
      </li>

      <li>
          <h3>Apakah ada <b>pertanyaan</b> lagi?</h3>
          <p>Kirimkan pertanyaan Anda di <a href="<?php echo site_url('kontak');?>"><b>sini</b></a></p>
      </li>
    </ul>
    </div>
</div>
</div>
