<!--  HEADER -->
<div id="header">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%; visibility: hidden;" alt="header">
  <div class="container header" style="margin-top:-50px;">
    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center">
		<br>
		<br>
        <h1 style="color:#FFF">SELAMAT REGISTRASI ANDA BERHASIL</h1>
        <p style="color:#FFF">Silahkan download bukti formulir pendaftaran di bawah ini, cetak dan simpan sebagai bukti resmi pendaftaran TEBAS 2017<br>
        	Untuk melihat profil Anda klik <a href="<?php echo site_url('profil/peserta/'.$this->session->userdata('id_registrasi'));?>" target="_blank"><b style="color:#FFF">DISINI</b></a></p>
		<br>
        <a href="<?php echo site_url('registrasi/download/'.$komp.'/'.$id );?>" target="blank"><button type="button" class="btn btn-danger btn-lg">DOWNLOAD</button></a>
		<br>
		<br>
		<br>
		<br>
		<br>
    </div>
  </div>
</div>
