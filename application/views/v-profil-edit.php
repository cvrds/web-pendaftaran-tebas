<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
  <div class="container">
    <div class="col-md-12">
       <h2 class="caption">PROFIL</h2>
    </div>
    <div class="col-md-12" style="background:rgba(224, 224, 224, 0.5);border-radius:5px">
      <div class="row">
      <div class="col-md-3">
        <h2>MENU</h2>
        <ul class="nav nav-pills nav-stacked">
        <li><a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($id));?>">Home</a></li>
        <li class="active"><a href="<?php echo site_url('profil/edit/'.tebas_encrypt($id));?>">Profile</a></li>
        <li><a href="<?php echo site_url('profil/upload/'.tebas_encrypt($id));?>">Upload Karya</a></li>
        <li><a href="<?php echo site_url('beranda/logout');?>">Logout</a></li>
      </ul>
      </div>
      <div class="col-md-9">
        <h2>PROFIL</h2>
		    <div class="panel-body" style="background:#fff;margin-bottom:20px">

              <?php echo validation_errors();?>
              <?php if($this->session->flashdata('notif')){ echo $this->session->flashdata('notif'); }?>

              <form role="form" action="<?php echo site_url('profil/edit/'.tebas_encrypt($id));?>" method="POST">

                <div class="col-md-12 col-xs-12 pull-left h-f1">
                  <label>A. AKUN</label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  1. Email
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input value="<?php echo $peserta->email;?>" class="form-control" disabled>
                </div>
                 <div class="col-md-5 col-xs-12 pull-left h-f1">
                  2. Ganti Password
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="password" name="password" class="form-control">
                </div>
                 <div class="col-md-5 col-xs-12 pull-left h-f1">
                  3. Ulangi Password
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="password" name="conf_pass" class="form-control">
                </div>

                <div class="col-md-12 col-xs-12 pull-left h-f1">
                  <label>B. DATA PENDAFTAR</label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  1. Nama
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="nama" value="<?php echo $peserta->nama;?>" class="form-control" required="">
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  2. Jenis Kelamin
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <?php if($peserta->jk == "Pria"){ $pria = "checked"; $wanita = ""; }else{ $pria = ""; $wanita = "checked"; }?>
                  <label><input type="radio" name="jk" value="Pria" <?php echo $pria;?> class="" required=""> Pria </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="jk" value="Wanita" <?php echo $wanita;?> class="" required=""> Wanita </label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  3. Tempat &amp; Tanggal Lahir
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="tpt_lahir" value="<?php echo $peserta->tpt_lahir;?>" required="" class="form-control" placeholder="Tempat Lahir">
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="tgl_lahir" value="<?php echo $peserta->tgl_lahir;?>" required="" class="form-control" placeholder="23/12/1992" id="dp">
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  4. Alamat
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <textarea class="form-control" name="alamat" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"><?php echo $peserta->alamat;?></textarea>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  5. Instansi/sekolah
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="instansi" value="<?php echo $peserta->instansi;?>" class="form-control" placeholder="Kosongkan jika tidak ada">
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  6. No. Telephone (Utama)
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="number" name="telp1" value="<?php echo $peserta->telp1;?>" required="" class="form-control">
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  7. No. Telephone (Sekunder)
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="number" name="telp2" value="<?php echo $peserta->telp2;?>" class="form-control">
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  8. Akun Instagram
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="twitter" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award">
                </div>

                <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
                  <button type="submit" class="btn btn-success pd-btn"> Update </button>
                  &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($id));?>"><input type="button" class="btn btn-danger pd-btn" value="Batal"></a>
                </div>

                </form>
            </div>
      </div>
    </div>
    </div>

  </div>
</div>
