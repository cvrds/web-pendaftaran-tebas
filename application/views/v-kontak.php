<!-- FORM -->
<div id="mainkontenjuri">
  <div class="container">
    <div class="col-md-12">
       <h2 class="caption" style="color:#000">KONTAK</h2>
    </div>

    <div class="col-md-3 col-sm-6">
      <h3>Contact Person</h3>
        <ul>
          <li style="display:block"><span class="glyphicon glyphicon-phone-alt"> </span> Arif  : 0857 0206 9611</li>
          <li style="display:block"><span class="glyphicon glyphicon-phone-alt"> </span> Fanny : 0896 7883 4989</li>
        </ul>
      <h3>Email</h3>
        <ul>
          <li style="display:block"><a href="mailto:info@tebasaward.com" target="blank"> <span class="glyphicon glyphicon-envelope"> </span> info@tebasaward.com</a></li>
        </ul>
      <h3>Fans Page</h3>
        <ul>
          <li style="display:block"><a href="http://fb.com/tebasaward" target="blank"><i class="fa fa-facebook-square"></i> http://fb.com/tebasaward</a></li>
        </ul>
        <h3>Instagram</h3>
        <ul>
          <li style="display:block"><a href="http://instagram.com/tebas.award" target="blank"><i class="fa fa-instagram"></i> tebas.award</a></li>
        </ul>
    </div>
    <div class="col-md-3 col-sm-6">
      <h3>LINE@ Official</h3>
        <ul>
          <li style="display:block"><a href="http://tebasaward.com/akunline" target="blank">@LZC3855J</a></li>
        </ul>
      <h3>Twitter</h3>
        <ul>
          <li style="display:block"><a href="https://twitter.com/tebasaward" target="blank"><i class="fa fa-twitter-square"></i> @tebasaward</a></li>
        </ul>
      <h3>Website</h3>
        <ul>
          <li style="display:block"><a href="http://tebasaward.com" target="blank" ><span class="glyphicon glyphicon-globe"></span> www.tebasaward.com</a></li>
        </ul>
		<h3>Sekretariat</h3>
			<p>Gedung BSC Lt. 3 Ruang VI.3.6 Kampus Terpadu Universitas AMIKOM Yogyakarta. Jl. Ringroad Utara Condong Catur Yogyakarta.</p>
    </div>
     <div class="col-md-6 col-sm-12">
        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCyOVPXYDdVSe9yhxJq1PVu_q6g7-Aeqww'></script><div style='overflow:hidden;height:320px;width:550px;'><div id='gmap_canvas' style='height:320px;width:550px;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a href='https://addmap.net/'>google maps</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=b54a0335353dd8d9de18c3af34329652b32b4824'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:16,center:new google.maps.LatLng(-7.760257600000001,110.4088074),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(-7.760257600000001,110.4088074)});infowindow = new google.maps.InfoWindow({content:'<strong>Sekretariat TEBAS 2017</strong><br>Gedung BSC Lt. 3 Ruang VI.3.6 Universitas Amikom Yogyakarta. Jl. Ringroad Utara Condong Catur Yogyakarta.<br>55283 Yogyakarta<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
      <!-- <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.280090482608!2d110.40891599999999!3d-7.760090999999992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x6e395fc6c5a4a8b2!2sAmikom+-+Yogyakarta!5e0!3m2!1sen!2sid!4v1427426295866" target="_blank"><img src="<?php echo base_url();?>assets/img/map.jpg" class="img-responsive" style="border-radius:10px"></a> -->
    </div>
  </div>
</div>
