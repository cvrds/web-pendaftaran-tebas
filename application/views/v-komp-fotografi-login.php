<!-- FORM -->
<div id="mainkonten">
<div class="container pc">
    <div class="col-md-12">
       <h2 class="caption">PHOTOGRAPHY</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2 isi-form" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h4 style="color:#FFF">Karya yang dihasilkan menggambarkan suasana permainan anak - anak pada era 90'an yang kini mulai di tinggalkan. Contohnya anak - anak yang sedang bermain petak umpet, bermain kelereng, lompat tali, dan permainan lainnya. Diharapkan, hasil karya photography ini mengandung pesan yang dapat tersampaikan terutama untuk anak - anak di zaman sekarang.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
              <li>Lomba ini berlaku untuk umum.</li>
              <li>Peserta lomba adalah perorangan.</li>
              <li>Karya harus orisinil dan belum pernah menjuarai kategori lomba TEBAS.</li>
              <li>Karya tidak diperkenankan mengandung unsur SARA (Suku, Agama, dan RAS) , tidak mengandung unsur pornografi serta tidak bertentangan dengan ketentuan perundangan yang berlaku di Indonesia.</li>
              <li>Diperkenankan mengirim lebih dari 1 (satu) karya.</li>
              <li>Karya yang dikumpulkan dalam bentuk softcopy, diupload / diunggah ke <a href="tebasaward.com">tebasaward.com</a> dengan ketentuan ukuran A3, high-resolution/300 dpi , format *.JPG/JPEG dengan ukuran minimal 2 MB dan maksimal 6 MB dan disertai dengan deskripsi karya.</li>
              <li>Peserta wajib memahami dan mematuhi seluruh ketentuan & syarat lomba seperti yang tercantum di halaman ini.</li>
              <li>Hasil karya dilarang mengikuti perlombaan sejenis selama acara TEBAS berlangsung.</li>
              <li>Panitia berhak menggunakan karya peserta dan pemenang lomba untuk kegiatan promosi dan publikasi tanpa memberikan kompensasi apapun dengan hak cipta karya tetap dipegang oleh pemilik karya.</li>
              <li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.</li>
              <li>Keputusan juri tidak bisa diganggu gugat.</li>
              <li>Ketentuan yang belum di atur dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
              <li>Tahun pembuatan Karya Maksimal dari tahun 2015.</li>
              <li>Karya Foto hanya boleh berwarna dan tidak diperkenankan hitam putih.</li>
              <li>Editing yang diperbolehkan hanya cropping dan exposure (sebatas kamar gelap).</li>
              <li>IDR : 20K per Karya yang dikirimkan</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Biaya Pendaftaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Biaya Pendaftaran Photography Contest adalah :<br>
                - Katagori Umum&nbsp;:&nbsp; Rp. 20.000,-
            </li>
            <li>Biaya pendaftaran diatas berlaku untuk 1 karya dan dikalikan sesuai jumlah karya.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh karya Photography Contest yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh karya peserta yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, Karya peserta akan masuk pada tahap penilaian Juri.</li>
            <li>Semua karya akan di tampilkan di lokasi pameran TEBAS (SHOW UP).</li>
            <li>Pemenang kategori Photography Contest terfavorit diambil dari semua karya yang lolos tahap awal penjurian. Polling like dilakukan di <a href="https://www.facebook.com/TEBASAWARD" target="_blank"> <b>Fanspage TEBAS AWARD</b> </a> pada saat SHOW UP TEBAS 2017 berlangsung.</li>
            <li>Pemenang akan diumumkan dalam acara TEBAS AWARDS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Juara I</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Juara II</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Juara Favorit</td>
          <td>Trophy + Sertifikat </td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Cara Pengumpulan Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">
          <ol>
            <li>Silahkan Login Akun TEBAS dan pilih menu profil --> upload karya</li>
            <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
            <li>Pilih kategori lomba</li>
            <li>File berformat rar/zip yang berisi :
            <ul>
             <li>Karya</li>
             <li><i><b>file *.doc (berisi nama lengkap, judul dan deskripsi karya)</b></i></li>
             <li>Scan Kartu Identitas (KTM/KTP)</li>
             <li>Surat Pernyataan (Gambar/Scan)</li>
             <li>Bukti Transaksi (Gambar/Scan)</li>
             </ul>
            </li>
            <li>Ukuran file .rar/.zip max 6MB</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSeven" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSeven">
          <div class="panel-body text-left">

            <h4>TRANSFER</h4>
            <ol>
              <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Photography Contest = Rp. 20.000,- <br>
                Nomor HP Anda = 0857 0206 9611<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 20.611</i>
              </li>
              <li>Kirim ke nomor rekening MUAMALAT 5370004949<br>AN : Fanny Septiani.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 0896 7883 4989 (Fanny) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Arif Rusmandani#20.611</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim.</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>

          </div>
          </div>
        </div>
      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center" style="color:#FFF"><label>FORMULIR PENDAFTARAN<br />PHOTOGRAPHY CONTEST</label></h3>
      <h4 class="text-center" style="color:#FFF">Aku dan Permainan Masa Kecilku</h4><br /><br />

        <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" class="">
                LOGIN
              </a>
            </h4>
          </div>
          <div id="collapseOne1" class="panel-collapse in" style="height: auto;">
            <div class="panel-body">
              <div class="col-md-12">
              <form class="form-signin" action="<?php echo site_url('beranda/login');?>" method="POST" role="form">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input type="text" name="email" class="form-control h-f1" placeholder="Email address" required="" autofocus="">
                <input type="password" name="password" class="form-control h-f1" placeholder="Password" required="">
                <br><br>
                <button class="btn btn-lg btnlogin btn-block" type="submit">Sign in</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1" class="collapsed">
                REGISTER
              </a>
            </h4>
          </div>
          <div id="collapseTwo1" class="panel-collapse collapse">
            <div class="panel-body">
              <form role="form" action="<?php echo site_url('registrasi/doRegistrasi');?>" method="POST">

                <div class="col-md-12 col-xs-12 pull-left h-f1">
                  <label>A. AKUN</label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  1. Email
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="email" name="email" class="form-control" required=""/>
                </div>
                 <div class="col-md-5 col-xs-12 pull-left h-f1">
                  2. Password
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="password" name="password" class="form-control" required=""/>
                </div>
                 <div class="col-md-5 col-xs-12 pull-left h-f1">
                  3. Ulangi Password
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="password" name="conf_pass" class="form-control" required=""/>
                </div>

                <div class="col-md-12 col-xs-12 pull-left h-f1">
                  <label>B. DATA PENDAFTAR</label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  1. Nama
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="nama" class="form-control" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  2. Jenis Kelamin
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <label><input type="radio" name="jk" value="Pria" class="" required=""/> Pria </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="jk" value="Wanita" class="" required=""/> Wanita </label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  3. Tempat & Tanggal Lahir
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="tpt_lahir" class="form-control" placeholder="Tempat Lahir" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="tgl_lahir" class="form-control" placeholder="23/12/1992" id="dp" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  4. Alamat
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <textarea class="form-control" name="alamat" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"></textarea>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  5. Instansi/sekolah
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="instansi" class="form-control" placeholder="Kosongkan jika tidak ada"/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  6. No. Telephone (Utama)
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="number" name="telp1" class="form-control" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  7. No. Telephone (Sekunder)
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="number" name="telp2" class="form-control"/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  8. Akun Instagram
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="twitter" class="form-control" placeholder="tebas.award"/>
                </div>

                <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
                  <button type="submit" class="btn btnlogin pd-btn"> Daftar </button>
                  &nbsp;&nbsp;&nbsp;
                  <button type="reset" class="btn btn-danger pd-btn"> Batal </button>
                </div>

                </form>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
