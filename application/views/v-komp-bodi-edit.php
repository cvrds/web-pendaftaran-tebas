<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
  <div class="container">
    <div class="col-md-12">
       <h2 class="caption">PROFIL</h2>
    </div>
    <div class="col-md-12" style="background:rgba(224, 224, 224, 0.5);border-radius:5px">
      <div class="row">
      <div class="col-md-3">
        <h2>MENU</h2>
        <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($id));?>">Home</a></li>
        <li><a href="<?php echo site_url('profil/edit/'.tebas_encrypt($id));?>">Profile</a></li>
        <li><a href="<?php echo site_url('beranda/logout');?>">Logout</a></li>
      </ul>
      </div>
      <div class="col-md-9">
        <h2>EDIT BATTLE OF DIGITAL ILLUSTRATION</h2>
		<div style="padding:20px;background:#fff;margin-bottom:20px">

		<?php if($this->session->flashdata('notif')){ echo $this->session->flashdata('notif'); }?>

		<form action="<?php echo site_url('profil/editBodi/'.tebas_encrypt($id).'/'.tebas_encrypt($peserta->id_peserta_bodi) );?>" method="POST">
		  <div class="col-md-12 col-xs-12 pull-left h-f1">
			<label>A. DATA PENDAFTAR</label>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			1. Nama
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			2. Tempat Tanggal Lahir
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->tpt_lahir;?>" class="form-control" placeholder="Tempat Lahir" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->tgl_lahir;?>" class="form-control" placeholder="1992/12/23" id="dp" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			3. Alamat
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" maxlength="300" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat;?></textarea>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			4. Instansi/sekolah
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->instansi;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			5. No. Telephone (Utama)
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" value="<?php echo $peserta->telp1;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			6. No. Telephone (Sekunder)
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="number" value="<?php echo $peserta->telp2;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			7. Email
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="email" value="<?php echo $peserta->email;?>" class="form-control" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			8. Akun Instagram
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award" disabled>
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			9. Judul Karya
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<input type="text" name="judul" required="" value="<?php echo $peserta->judul;?>" class="form-control">
		  </div>
		  <div class="col-md-5 col-xs-12 pull-left h-f1">
			10. Deskripsi Karya
		  </div>
		  <div class="col-md-7 col-xs-12 pull-left h-f2">
			<textarea class="form-control" name="deskripsi" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"><?php echo $peserta->deskripsi;?></textarea>
		  </div>

		  <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
			<button type="submit" name="submit" class="btn btn-success pd-btn"> Update </button>
			&nbsp;&nbsp;&nbsp;
			<a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($id) );?>"><input type="button" class="btn btn-danger pd-btn" value="Batal"></a>
		  </div>

		  </form>

		  <div class="clearfix"></div>
		</div>

      </div>
    </div>
    </div>

  </div>
</div>
