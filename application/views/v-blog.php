<!-- FORM -->
<div id="mainkontenjuri">
  <div class="container">
    <div class="col-md-12">
       <h2 class="caption" style="color:#000">GALLERY</h2>
    </div>
    <div class="col-md-12" id="arsip">
      <h1 style="font-size:60px; color:#000" >FOTO</h1>
      <h2 style="margin-top:0">Galleri Foto TEBAS</h2>
      <br>
      <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/01.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb01.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/02.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb02.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/03.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb03.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/04.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb04.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/05.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb05.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/06.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb06.jpg" alt="arsip" class="img-responsive"></a>
        </div>

        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/07.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb07.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/08.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb08.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/09.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb09.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/010.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb010.jpg" alt="arsip" class="img-responsive"></a>
        </div>
		<div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/011.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb011.jpg" alt="arsip" class="img-responsive"></a>
        </div>
		<div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/012.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb012.jpg" alt="arsip" class="img-responsive"></a>
        </div>
		<div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/013.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb013.jpg" alt="arsip" class="img-responsive"></a>
        </div>
		<div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/014.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb014.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/015.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb015.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/016.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb016.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/017.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb017.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 block">
          <a class="group1" href="<?php echo base_url();?>assets/img/arsip/018.jpg" title="foto"><img src="<?php echo base_url();?>assets/img/arsip/thumb018.jpg" alt="arsip" class="img-responsive"></a>
        </div>
        <div class="clearfix"></div>
        <br><br>
        <h1 style="font-size:60px; color:#000;">VIDEO</h1>
        <h2 style="margin-top:0">Galleri Video TEBAS</h2>
        <br>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="https://www.youtube.com/embed/02SgT9xdI4Q"><img src="http://img.youtube.com/vi/02SgT9xdI4Q/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2017</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="https://www.youtube.com/embed/YW128_bUa9I"><img src="http://img.youtube.com/vi/YW128_bUa9I/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2016</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/FN1b4Cr2bXo"><img src="http://img.youtube.com/vi/FN1b4Cr2bXo/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2015</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/xtcW6hShXl8"><img src="http://img.youtube.com/vi/xtcW6hShXl8/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2014</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/Cgng6SZfYXk"><img src="http://img.youtube.com/vi/Cgng6SZfYXk/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2013</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/g6yW5PUoV3A"><img src="http://img.youtube.com/vi/g6yW5PUoV3A/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2012</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/M5k8Ho9-Ezw"><img src="http://img.youtube.com/vi/M5k8Ho9-Ezw/mqdefault.jpg" class="img-responsive" /></a>
          <h4>SHOW UP TEBAS 2015</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/5LpXeT4cUg4"><img src="http://img.youtube.com/vi/5LpXeT4cUg4/mqdefault.jpg" class="img-responsive" /></a>
          <h4>SHOW UP TEBAS 2014</h4>
        </div>
	    <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/t-VFcH4DuaY"><img src="http://img.youtube.com/vi/t-VFcH4DuaY/mqdefault.jpg" class="img-responsive" /></a>
          <h4>WAWANCARA SHARING INVECT `1st day "Kelik Sri Nugroho"</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/sYv28LwBNxM"><img src="http://img.youtube.com/vi/sYv28LwBNxM/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TESTIMONI PESERTA BODI 1st day</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/4iSzzEylBIA"><img src="http://img.youtube.com/vi/4iSzzEylBIA/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEBAS 2016 INVITATION</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/mvtiMhn6rh8"><img src="http://img.youtube.com/vi/mvtiMhn6rh8/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEBAS 2015 INVITATION</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/7euFNvfQEnQ"><img src="http://img.youtube.com/vi/7euFNvfQEnQ/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEBAS 2014 INVITATION</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/3ysMtO_UOQw"><img src="http://img.youtube.com/vi/3ysMtO_UOQw/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEBAS 2013 INVITATION</h4>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <a class='youtube' href="http://www.youtube.com/embed/FTi8-lb5PDs"><img src="http://img.youtube.com/vi/FTi8-lb5PDs/mqdefault.jpg" class="img-responsive" /></a>
          <h4>TEASER TEBAS 2012</h4>
        </div>

    </div>
  </div>
</div>
