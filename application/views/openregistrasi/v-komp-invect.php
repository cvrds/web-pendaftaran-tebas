<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
<div class="container invect">
    <div class="col-md-12">
       <h2 class="caption">INVECT</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h4>Kompetisi Film Indie yang bertemakan bebas (non dokumenter) diperuntukkan bagi umum dan pelajar se-Indonesia.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Syarat Umum Peserta
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse <!--in-->" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
            <li>Peserta berdomisili di Indonesia.</li>
            <li>Harga pendaftaran Rp 50.000,-</li>
            <li>Peserta boleh terdiri dari individu ataupun kelompok. (Jumlah anggota kelompok bebas)</li>
            <li>Peserta wajib melampirkan fotokopi identitas</li>
            <li>Peserta wajib memahami dan menyepakati seluruh persyaratan lomba seperti yang tercantum di halaman ini.</li>
            <li>Ketentuan yang belum di atur  dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Syarat karya film pendek
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Tema film bebas (Non Dokumenter).</li>
            <li>Karya harus orisinil dan belum pernah menjuarai kategori lomba TEBAS.</li>
            <li>Film yang diikutsertakan berdurasi maksimal 20 menit termasuk opening dan kredit title.</li>
            <li>Film dikirim dalam bentuk DVD dengan label, cover, dan sinopsis film (format MPEG atau MOV).</li>
            <li>Peserta wajib mengirimkan poster  film berupa CD softCopy high-resolution, format *.jpg dengan resolusi minimal 300dpi, Serta hardcopy berupa karya cetak kertas ivory ukuran A3+ (250 gr) untuk standarisasi karya peserta ditampilkan pada SHOW UP TEBAS 2014.</li>
            <li>Peserta wajib mengirimkan trailer dengan format MPEG atau MOV durasi maksimal 30 detik dan penjelasan film (sinopsis).</li>
            <li>Bahasa yang dapat digunakan di dalam keseluruhan film adalah bahasa Indonesia, bahasa asing maupun bahasa daerah. Jika menggunakan bahasa asing atau bahasa daerah diwajibkan untuk menggunakan subtitle bahasa Indonesia.</li>
            <li>Film yang dibuat tidak diperkenankan mendiskreditkan unsur  SARA (Suku, Agama, dan RAS) tertentu, serta tidak mengandung unsur pornografi.</li>
            <li>Setiap peserta yang terdaftar boleh mengikut sertakan lebih dari satu (1) buah film.
            <li>Menyertakan 1 perwakilan biodata lengkap, nomor telepon, fotokopi identitas yang masih berlaku.</li>
            <li>Keputusan Juri tidak dapat diganggu gugat.</li>
            <li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Syarat Penggunaan Hasil Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Panitia TEBAS memegang hak sepenuhnya atas penggunaan hasil karya lomba untuk berbagai kepentingan menyangkut selama acara TEBAS berlangsung.</li>
            <li>Hasil karya dilarang mengikuti perlombaan sejenis selama acara TEBAS berlangsung.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh film yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh film peserta yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, film peserta akan masuk pada tahap penilaian juri pertama.</li>
            <li>Keputusan Juri  akan menghasilkan:</li>
            <a class="btn" href="#collapseFive" data-parent="#accordion" data-toggle="collapse"><button>Aturan Hadiah Perlombaan</button></a>
            <li>Nominasi film terfavorit yang akan di putarkan pada screening SHOW UP 2014 adalah :</li>
              <ul>
              <li>15 besar film terbaik pilihan juri (peserta kategori umum)</li>
              <li>10 besar film terbaik pilihan juri (peserta kategori pelajar)</li>
              </ul>
            <li>Film terfavorit berdasarkan 40% polling pada web TEBAS (http://www.tebasaward.com) dan 60% polling pengunjung SHOW UP.</li>
            <li>Pemenang akan diumumkan dalam acara TEBAS AWARDS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Film Terbaik</td>
          <td>Trophy Dedikasi + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Cerita Terbaik</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Sinematografi</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Film Terfavorit</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Tata Suara Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Penyutradaraan Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Pemeran Utama Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr class="warning">
          <th colspan="2">PELAJAR :</th>
          </tr>
          <tr>
          <td>Film Terbaik</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Cerita Terbaik</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Sinematografi</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Film Terfavorit</td>
          <td>Trophy + Sertifikat</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseSix" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Yang perlu di Perhatikan Oleh Peserta
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSix">
          <div class="panel-body text-left">
          <ol>
            <li>Peserta wajib memenuhi persyaratan yang dianjukan oleh panitia.</li>
            <li>Karya harus orisinil. Panitia tidak menanggung jika terjadi sengketa karya atau soundtrack yang digunakan.</li>
            <li>Jika terjadi kerusakan fisik pada keping DVD dalam pengiriman, maka panitia akan menghubungi peserta untuk membicarakan mengenai tindak lanjut yang harus dilakukan.</li>
            <li>Seluruh Finalis wajib hadir dalam malam puncak penganugrahan.</li>
            <li>Keputusan Juri tidak dapat di ganggu gugat.</li>
            <li>Ketentuan yang belum di atur  dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
          </ol>
          </div>
          </div>
        </div>
		<div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSeven" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSeven">
          <div class="panel-body text-left">
          <h4>KIRIM BERSAMA KARYA</h4>
            <ol>
              <li>Masukkan biaya pendaftaran langsung bersama karya yang akan di kirim.</li>
              <li>Setelah mengirim karya bersama biaya pendaftaran, konfirmasi sms di nomor 085 229 612 162 (Rona).</li>
            </ol>

            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Indie Movie Competition = Rp. 50.000<br>
                Nomor HP Anda = 087 815 332 422<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 50.422</i>
              </li>
              <li>Kirim ke nomor rekening BRI 0078-01-043320-50-7<br>AN : Rona Guines Purnasiwi.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 085229612162 (Rona) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Yoke Lestari#60.422</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim.</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>
          </div>
          </div>
        </div>

      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center"><label>FORMULIR PENDAFTARAN<br />INDIE MOVIE COMPETITION (INVECT)</label></h3>
      <h4 class="text-center" style="color:#7A0000">Bebas (Non Dokumenter)</h4><br /><br />
      <form role="form" action="<?php echo site_url('registrasi/daftarInvect');?>" method="POST">
      <div class="col-md-5 col-xs-12 pull-left">
        Pilih Kategori
      </div>
      <div class="col-md-7 col-xs-12 pull-left">
        <label><input type="radio" name="kategori" value="umum" class="" required=""/> UMUM </label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label><input type="radio" name="kategori" value="pelajar" class="" required=""/> PELAJAR </label>
      </div>



      <div class="col-md-12 col-xs-12 pull-left h-f1">
        <label>A. DATA PENDAFTAR</label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        1. Nama
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="nama" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        2. Instansi/sekolah
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="instansi" value="<?php echo $peserta->instansi;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        3. Alamat
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" name="alamat" maxlength="300" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat;?></textarea>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        4. No. Telephone (Utama)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" name="telp1" value="<?php echo $peserta->telp1;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        5. No. Telephone (Sekunder)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" name="telp2" value="<?php echo $peserta->telp2;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        6. Email
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="email" name="email" value="<?php echo $peserta->email;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        7. Akun Instagram
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="twitter" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award" disabled>
      </div>
      <div class="col-md-12 col-xs-12 pull-left h-f1">
        <label>B. DATA FILM</label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        1. Judul Film
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="judul" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        2. Genre Film
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="genre" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        3. Bahasa
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="bahasa" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        4.  Durasi
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="durasi" required="" class="form-control" placeholder="contohnya 15:32"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        5.  Karegori Penonton
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <label><input type="radio" name="penonton" value="semua" class="" required=""/> Semua Usia </label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label><input type="radio" name="penonton" value="+17" class="" required=""/> Dewasa (+17) </label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        6. Tahun Produksi
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" name="tahun" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        7.  Kota/Daerah/Negara Produksi
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="daerah" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        8. Produser
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="produser" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        9. Sutradara
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="sutradara" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        10. Penulis Naskah
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="naskah" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        11. Editor
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="editor" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        12. Pemeran Utama
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="pemeran" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        13. Pernah Ditayangkan
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <label><input type="radio" name="penayangan" value="pernah" required="" class=""/> Pernah </label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label><input type="radio" name="penayangan" value="belum" required="" class=""/> Belum Pernah </label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        14. Karya Pertama
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <label><input type="radio" name="karyapertama" value="ya" required="" class=""/> Ya </label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label><input type="radio" name="karyapertama" value="tidak" required="" class=""/> Tidak </label>
      </div>
      <div class="col-md-12 col-xs-12 pull-right h-f1">
        <i class="color-i">Film Lain Yang Pernah Dibuat</i>
      </div>

      <div class="col-md-12 col-xs-12 pull-right h-f2">
        <div class="col-md-8 col-xs-8 pull-left h-f2">
          <input type="text" name="film1" class="form-control"placeholder="Judul Film"/>
        </div>
        <div class="col-md-4 col-xs-4 pull-left h-f2">
          <input type="text" name="thn1" class="form-control"placeholder="Tahun"/>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 pull-right h-f2">
        <div class="col-md-8 col-xs-8 pull-left h-f2">
          <input type="text" name="film2" class="form-control"placeholder="Judul Film"/>
        </div>
        <div class="col-md-4 col-xs-4 pull-left h-f2">
          <input type="text" name="thn2" class="form-control"placeholder="Tahun"/>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 pull-right h-f2">
        <div class="col-md-8 col-xs-8 pull-left h-f2">
          <input type="text" name="film3" class="form-control"placeholder="Judul Film"/>
        </div>
        <div class="col-md-4 col-xs-4 pull-left h-f2">
          <input type="text" name="thn3" class="form-control"placeholder="Tahun"/>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 pull-left h-f1">
        15. Kompetisi yang pernah diikuti
      </div>
      <div class="col-md-11 col-xs-12 pull-right h-f2">
        <input type="text" name="kompetisi1" class="form-control"/>
      </div>
      <div class="col-md-11 col-xs-12 pull-right h-f2">
        <input type="text" name="kompetisi2" class="form-control"/>
      </div>
      <div class="col-md-11 col-xs-12 pull-right h-f2">
        <input type="text" name="kompetisi3" class="form-control"/>
      </div>
      <div class="col-md-12 col-xs-12 pull-left h-f1">
        16. Prestasi yang pernah dicapai
      </div>
      <div class="col-md-11 col-xs-12 pull-right h-f2">
        <input type="text" name="prestasi1" class="form-control"/>
      </div>
      <div class="col-md-11 col-xs-12 pull-right h-f2">
        <input type="text" name="prestasi2" class="form-control"/>
      </div>
      <div class="col-md-11 col-xs-12 pull-right h-f2">
        <input type="text" name="prestasi3" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        17. List Backsound
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" name="backsound" rows="4" style="margin-bottom:10px;" ></textarea>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        18. Sinopsis Film
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" name="sinopsis" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"></textarea>
      </div>

      <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
        <input type="hidden" name="id_regis" value="<?php echo $this->session->userdata('id_registrasi');?>">
        <button type="submit" class="btn btn-success pd-btn"> Daftar </button>
        &nbsp;&nbsp;&nbsp;
        <button type="reset" class="btn btn-danger pd-btn"> Batal </button>
      </div>


      </form>
    </div>

  </div>
</div>
