<!--  HEADER -->
<div id="header">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%" alt="header">
  <div class="container header" style="margin-top:-50px;">
    <div class="col-md-12">
          <center><img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive"></center>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center">
        <?php if($this->auth->is_logged_in() == false){?>
          <h1>LIHAT VIDEO TUTORIAL REGISTRASI TEBAS 2014</h1>
          <p>Silahkan klik untuk melihat video prosedur pendaftaran TEBAS 2014 lalu pilih lomba yang ingin kamu ikuti dibawah ini</p>
        <?php }else{ ?>
          <h1>SELAMAT DATANG <a href="<?php echo site_url('profil/peserta/'.$this->session->userdata('id_registrasi'));?>" style="color:#7A0000" title="Lihat Profil"><b><?php echo strtoupper($this->session->userdata('nama'));?></b></a></h1>
          <p>Silahkan klik untuk melihat video prosedur pendaftaran TEBAS 2014 lalu pilih lomba yang ingin kamu ikuti dibawah ini</p>
        <?php }?>
        <a class='youtube' href="http://www.youtube.com/embed/ouFFf7z7hTI"><button type="button" class="btn btn-danger btn-lg">LIHAT DISINI</button></a>
    </div>
  </div>
  <br>
  <div id="turun-limit"></div>
  <div class="text-center">
    <a href="#turun-limit" title="Turun" class="totop"><span class="glyphicon glyphicon-circle-arrow-down" style="margin-top:3px"></span></a>
  </div>
  <br>
</div>
<!--  KOMPETISI -->
<div id="competition">
  <div class="container">
      <h2 class="caption">REGISTRATION</h2>
      <a href="<?php echo site_url('kompetisi/invect');?>">
      <div class="col-md-4 icon">
        <img src="<?php echo base_url();?>assets/img/i-invect.png" alt="invect" class="img-responsive">
        <h2>Indie Movie Competition</h2>
        <p>Kompetisi Film Indie yang bertemakan bebas (non dokumenter) diperuntukkan bagi umum dan pelajar se-Indonesia... </p>
        <button type="button" class="btn btn-danger btn-sm">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/fotografi');?>">
      <div class="col-md-4 icon">
        <img src="<?php echo base_url();?>assets/img/i-pc.png" alt="invect" class="img-responsive">
        <h2>Photography Contest</h2>
        <p>Sebuah lomba foto yang dilakukan dengan cara pesan berantai (rally photo) dengan memadukan ketepatan dan kecepatan...</p>
        <button type="button" class="btn btn-danger btn-sm">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
      <a href="<?php echo site_url('kompetisi/bodi');?>">
      <div class="col-md-4 icon">
          <img src="<?php echo base_url();?>assets/img/i-bodi.png" alt="invect" class="img-responsive">
          <h2>Battle Of Digital Illustration</h2>
          <p>Sebuah lomba dengan menggunakan perangkat digital komputer untuk menghasilkan sebuah karya visual dan digital illustrasi...</p>
          <button type="button" class="btn btn-danger btn-sm">Register <span class="glyphicon glyphicon-chevron-right"></span></button>
      </div>
      </a>
  </div>
</div>
