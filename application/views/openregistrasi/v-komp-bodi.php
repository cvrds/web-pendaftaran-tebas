<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
  <div class="container bodi">
    <div class="col-md-12">
       <h2 class="caption">BODI</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h4>Sebuah teknik dengan menggunakan perangkat digital komputer untuk menghasilkan sebuah karya visual dan digital illustrasi bukan hanya membuat atau memanipulasi gambar menggunakan software, namun lebih pada menciptakan sebuah artwork dengan konsep ide dan visual yang baru.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan dan Pernyataan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
            <li>Hasil karya poster belum pernah dipublikasikan atau diikutsertakan ke lomba sejenis.
            <li>Apabila ada pihak lain yang menyatakan bahwa karya itu bukan hak cipta peserta (dengan bukti yang jelas ) maka peserta yang mendaftar karya tsb didiskualifikasi, pihak panitia lomba tidak akan turut campur dalam persoalan ini.
            <li>Panitia TEBAS memegang hak sepenuhnya atas penggunaan hasil karya lomba untuk berbagai kepentingan menyangkut selama acara TEBAS berlangsung.
            <li>Hasil karya dilarang mengikuti perlombaan sejenis selama acara TEBAS berlangsung
            <li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.
            <li>Ketentuan yang belum di atur dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.
            <li>Ketentuan dan persyaratan lomba dapat di lihat pada web TEBAS <a href="http://www.tebasaward.com" target="_blank">http://www.tebasaward.com</a></li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Lomba ini berlaku untuk Umum</li>
            <li>Hasil karya orisinil, belum pernah dipublikasikan.</li>
            <li>Design yang dibuat tidak diperkenankan mendiskreditkan unsur  SARA (Suku, Agama,  dan RAS) , tidak mengandung unsur pornografi serta tidak bertentangan dengan ketentuan perundangan yang berlaku di Indonesia.</li>
            <li>Karya yang di terima panitia berupa CD softCopy high-resolution, karya dalam format *.jpg dengan resolusi minimal 300dpi. Serta hardcopy berupa karya cetak kertas ivory ukuran A3+ (250 gr) untuk standarisasi karya peserta ditampilkan pada SHOW UP TEBAS 2014</li>
            <li>Setiap peserta boleh Mengirimkan Lebih dari 1 hasil karya.
            <li>peserta lomba adalah perorangan.
            <li>Panitia berhak mendiskualifikasi peserta yang melanggar ketentuan lomba.
            <li>Keputusan Juri tidak dapat diganggu gugat .
            <li>Hasil karya yang masuk menjadi milik penyelenggara selama acara TEBAS berlangsung.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Syarat Penggunaan Hasil Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Panitia TEBAS memegang hak sepenuhnya atas penggunaan hasil karya lomba untuk berbagai kepentingan menyangkut selama acara TEBAS berlangsung.</li>
            <li>Hasil karya dilarang mengikuti perlombaan sejenis selama acara TEBAS berlangsung.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh Digital Illustration yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh Digital Illustration yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, Digital Illustration peserta akan masuk pada tahap penilaian Juri.</li>
            <li>Keputusan Juri  akan menghasilkan:</li>
            <a class="btn" href="#collapseFive" data-parent="#accordion" data-toggle="collapse"><button>Aturan Hadiah Perlombaan</button></a>
            <li>Semua karya akan di tampilkan di lokasi pameran TEBAS (SHOW UP).</li>
            <li>Nominasi Design Illusration terfavorit adalah 15 besar karya terbaik kategori peserta umum pilihan juri.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Juara I</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Juara II</td>
          <td>Trophy + Sertifikat + Uang Pembinaan</td>
          </tr>
          <tr>
          <td>Favorit</td>
          <td>Trophy + Sertifikat</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseSix" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Yang perlu di Perhatikan Oleh Peserta
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSix">
          <div class="panel-body text-left">
          <ol>
            <li>Peserta wajib memenuhi persyaratan yang dianjukan oleh panitia.</li>
            <li>Karya harus orisinil. Panitia tidak menanggung jika terjadi sengketa karya.</li>
            <li>Jika terjadi kerusakan fisik pada keping CD dalam pengiriman, maka panitia akan menghubungi peserta untuk membicarakan mengenai tindak lanjut yang harus dilakukan.</li>
            <li>Seluruh Finalis wajib hadir dalam malam puncak penganugrahan.</li>
            <li>Keputusan Juri tidak dapat di ganggu gugat.</li>
            <li>Ketentuan yang belum di atur dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
          </ol>
          </div>
          </div>
        </div>
		    <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSeven" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSeven">
          <div class="panel-body text-left">
            <h4>KIRIM BERSAMA KARYA</h4>
            <ol>
              <li>Masukkan biaya pendaftaran langsung bersama karya yang akan di kirim.</li>
              <li>Setelah mengirim karya bersama biaya pendaftaran, konfirmasi sms di nomor 085 229 612 162 (Rona).</li>
            </ol>

            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Battle Of Digital Illustration = Rp. 20.000<br>
                Nomor HP Anda = 087 815 332 422<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 20.422</i>
              </li>
              <li>Kirim ke nomor rekening BRI 0078-01-043320-50-7<br>AN : Rona Guines Purnasiwi.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 085229612162 (Rona) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Yoke Lestari#60.422</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim.</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>

          </div>
          </div>
        </div>

      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center"><label>FORMULIR PENDAFTARAN<br />BATTLE OF DIGITAL ILLUSTRATION</label></h3>
      <h4 class="text-center" style="color:#7A0000">Warisan Nusantara</h4><br /><br />

      <form action="<?php echo site_url('registrasi/daftarBodi');?>" method="POST">
      <div class="col-md-12 col-xs-12 pull-left h-f1">
        <label>A. DATA PENDAFTAR</label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        1. Nama
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        2. Tempat Tanggal Lahir
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->tpt_lahir;?>" class="form-control" placeholder="Tempat Lahir" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->tgl_lahir;?>" class="form-control" placeholder="1992/12/23" id="dp" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        3. Alamat
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" maxlength="300" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat;?></textarea>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        4. Instansi/sekolah
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->instansi;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        5. No. Telephone (Utama)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" value="<?php echo $peserta->telp1;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        6. No. Telephone (Sekunder)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" value="<?php echo $peserta->telp2;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        7. Email
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="email" value="<?php echo $peserta->email;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        8. Akun Instagram
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->twitter;?>" class="form-control" placeholder="tebas.award" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        9. Judul Karya
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="judul" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        10. Deskripsi Karya
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" name="deskripsi" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"></textarea>
      </div>

      <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
        <input type="hidden" name="id_regis" value="<?php echo $peserta->id_registrasi;?>">
        <button type="submit" class="btn btn-success pd-btn"> Daftar </button>
        &nbsp;&nbsp;&nbsp;
        <button type="reset" class="btn btn-danger pd-btn"> Batal </button>
      </div>


      </form>
    </div>

  </div>
</div>
