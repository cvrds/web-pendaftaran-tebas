<!-- FORM -->
<div id="mainkonten">
<div class="container pc">
    <div class="col-md-12">
       <h2 class="caption">RALLY PHOTO</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h4>Rally Photo adalah sebuah lomba foto yang di lakukan dengan cara pesan berantai. Dengan memadukan ketepatan dan kecepatan. Tidak hanya mencari jawaban dari setiap soal yang telah di tentukan, namun peserta juga harus mengambil gambar berdasarkan jawaban soal yang sudah ditentukan.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Deskripsi Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseOne">
          <div class="panel-body text-left">
          <p>Rally Photo adalah sebuah lomba foto yang di lakukan dengan cara pesan berantai. Dengan memadukan ketepatan dan kecepatan. Tidak hanya mencari jawaban dari tiap soal yang telah di tentukan, namun peserta juga harus mengambil gambar berdasarkan jawaban soal yang sudah ditentukan.</p>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Peraturan Rally Foto
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Peserta lomba paling lambat hadir satu jam sebelum waktu perlombaan dimulai.</li>
            <li>Apabila dalam jangka 30 menit dari sesudah waku pertandingan dimulai, peserta tidak hadir, maka peserta tersebut dinyatakan gugur. Keputusan juri dalam pertandingan bersifat mutlak & tidak dapat diganggu gugat.</li>
            <li>Setiap peserta harus menaati peraturan-peraturan yang dibuat oleh panitia.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Peraturan Peserta
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          Peserta Rally Photo adalah mereka yang telah terdaftar dan mendapatkan nomor peserta, serta mengenakan atribut peserta, dan mengenakan nomor peserta.
          <ol>
            <li>Acara ini terbuka untuk seluruh WNI.</li>
            <li>Peserta lomba paling lambat hadir 1 jam sebelum waktu perlombaan dimulai.
            <li>Apabila dalam jangka 30 menit dari sesudah waku pertandingan dimulai, peserta tidak hadir, maka peserta tersebut dinyatakan gugur.</li>
            <li>Peserta harus mematuhi semua peraturan Rally Photo.</li>
            <li>Peserta yang berumur kurang dari 17 tahun harus mendapat izin dari orang tua atau wali.</li>
            <li>Panitia berhak menggunakan foto-foto peserta dan pemenang rally photo untuk kegiatan promosi dan publikasi tanpa memberikan kompensasi apapun dengan hak cipta foto tetap dipegang oleh pemilik foto.</li>
            <li>Panitia berhak mendiskualifikasi peserta yang melanggar peraturan lomba.</li>
            <li>Keputusan dewan juri adalah mutlak dan tidak dapat diganggu gugat.</li>
            <li>Panitia tidak bertanggung jawab atas segala kehilangan (kamera, uang serta benda berharga lainnya) atau kerusakan hak milik apapun atau kecelakaan dancedera pada bagian tubuh mana pun dari peserta yang terjadi selama kegiatan berlangsung.</li>
            <li>Dengan melakukan pendaftaran di acara ini, peserta menyatakan telah membaca, mengerti, dan setuju dengan segala syarat dan ketentuan serta peraturan Rally Photo.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseSix" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Pelaksanaan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSix">
          <div class="panel-body text-left">
          <table class="table">
            <tr class="warning"><th colspan="3">Rally photo akan dilaksanakan pada:</th></tr>
            <tr>
            <td>Tanggal</td>
            <td> : </td>
            <td>18 Mei 2014</td>
            </tr>
            <tr>
            <td>Pukul</td>
            <td> : </td>
            <td>06.00 -15.00 WIB (mengikuti acara)</td>
            </tr>
            <tr>
            <td>Tempat</td>
            <td> : </td>
            <td>Kota Gede (Yogyakarta)*</td>
            </tr>
          </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseEight" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Syarat dan Tata Cara
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseEight">
          <div class="panel-body text-left">
          <ol>
            <li>Kamera yang digunakan adalah hanya digital kamera dan jenis DSLR atau Digital Compact Camera, segala merek yang menggunakan memory card, tidak diijinkan menggunakan kamera Handphone, iPad.</li>
            <li>FORMAT atau DELETE SEKARANG JUGA KARTU MEMORY KAMERA anda. Hanya ada file hasil Rally Photo di kartu memory anda saat diserahkan ke panitia.</li>
            <li>IMAGE FILE : yang dipergunakan dan diserahkan harus dalam format JPEG tanpa di edit (sangsi diskualifikasi).</li>
            <li>Nomer file image harus urut, tidak boleh melompat (hindari penghapusan file). Yakin obyeknya shoot.</li>
            <li>File yang diserahkan disimpan dalam media : SD/SDHC/MMC/CF. Selain media tersebut peserta wajib membawa adapter USB sendiri untuk diserahkan bersamaan dengan penyerahan media penyimpanan.</li>
            <li>Peserta harus memotret sendiri dalam menyelesaikan soal-soal Rally Photo ini (tanpa bantuan orang lain, tanpa terkecuali)</li>
            <li>Peserta diharuskan memakai kartu peserta selama rally foto berlangsung.</li>
            <li>Hasil pemotretan dikumpulkan (softcopy) kepada panitia diakhir lomba.</li>
            <li>Penjurian dilakukan tertutup tanggal 2014 (langsung setelah lomba selesai).</li>
            <li>Pengumuman pemenang dan pemberian hadiah akan dilaksanakan pada tanggal 4 Juni 2014 saat acara puncak TEBAS 2014 malam hari.</li>
            <li>Foto yang telah disetor ke panitia menjadi hak milik panitia TEBAS 2014 dan akan dipamerkan dalam pameran foto SHOW UP TEBAS 2014.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penilaian Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">
          Penilaian akan dilakukan oleh tim juri dari luar yang akan bekerja sama dengan panitia. Peserta rally dapat bekerja dalam satu kelompok tetapi penilaiannya tetap didasarkan kepada individu. Berikut nilai-nilai penjurian:
          <ol>
            <li>Ketepatan menjawab soal</li>
            <li>Estetika Foto</li>
            <li>Komposisi Foto</li>
            <li>Teknik Foto</li>
            <li>Kecepatan mengumpulkan semua hasil foto dan jawaban</li>
            <li>Keputusan juri dan panitia tidak dapat diganggu gugat</li>
            <li>Keputusan Juri  akan menghasilkan:</li>
            <a class="btn" href="#collapseFive" data-parent="#accordion" data-toggle="collapse"><button>Aturan Hadiah Perlombaan</button></a>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Juara I</td>
          <td>Trophy + Sertifikat + Uang + Lensa Kamera</td>
          </tr>
          <tr>
          <td>Juara II</td>
          <td>Trophy + Sertifikat + Uang + Flash Kamera</td>
          </tr>
          <tr>
          <td>Juara III</td>
          <td>Trophy + Sertifikat + Tripod Kamera</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSeven" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSeven">
          <div class="panel-body text-left">

            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Photo Rally Contest = Rp. 60.000<br>
                Nomor HP Anda = 087 815 332 422<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 60.422</i>
              </li>
              <li>Kirim ke nomor rekening BRI 0078-01-043320-50-7<br>AN : Rona Guines Purnasiwi.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 085229612162 (Rona) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Yoke Lestari#60.422</i></li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>

          </div>
          </div>
        </div>
		<div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseNine" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Fasilitas
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>
          <div class="panel-collapse collapse" id="collapseNine">
          <div class="panel-body text-left">
          <ol>
            <li>Kaos Rally Photo</li>
			<li>Sesi Foto Model</li>
			<li>Makan Siang + Minum</li>
			<li>Snack + Minum</li>
			<li>Sertifikat Peserta</li>
			<li>Sticker TEBAS 2014</li>
			<li>Co-Card</li>
			<li>Manual Book + Alat Tulis</li>
          </ol>
          </div>
          </div>
        </div>

      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center"><label>FORMULIR PENDAFTARAN<br />PHOTOGRAPHY CONTEST</label></h3>
      <h4 class="text-center" style="color:#7A0000">Rally Photo: Nuansa Kota Lama</h4><br /><br />

        <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" class="">
                LOGIN
              </a>
            </h4>
          </div>
          <div id="collapseOne1" class="panel-collapse in" style="height: auto;">
            <div class="panel-body">
              <div class="col-md-12">
              <form class="form-signin" action="<?php echo site_url('beranda/login');?>" method="POST" role="form">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input type="text" name="email" class="form-control h-f1" placeholder="Email address" required="" autofocus="">
                <input type="password" name="password" class="form-control h-f1" placeholder="Password" required="">
                <br><br>
                <button class="btn btn-lg btn-warning btn-block" type="submit">Sign in</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1" class="collapsed">
                REGISTER
              </a>
            </h4>
          </div>
          <div id="collapseTwo1" class="panel-collapse collapse">
            <div class="panel-body">
              <form role="form" action="<?php echo site_url('registrasi/doRegistrasi');?>" method="POST">

                <div class="col-md-12 col-xs-12 pull-left h-f1">
                  <label>A. AKUN</label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  1. Email
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="email" name="email" class="form-control" required=""/>
                </div>
                 <div class="col-md-5 col-xs-12 pull-left h-f1">
                  2. Password
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="password" name="password" class="form-control" required=""/>
                </div>
                 <div class="col-md-5 col-xs-12 pull-left h-f1">
                  3. Ulangi Password
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="password" name="conf_pass" class="form-control" required=""/>
                </div>

                <div class="col-md-12 col-xs-12 pull-left h-f1">
                  <label>B. DATA PENDAFTAR</label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  1. Nama
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="nama" class="form-control" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  2. Jenis Kelamin
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <label><input type="radio" name="jk" value="Pria" class="" required=""/> Pria </label>
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="jk" value="Wanita" class="" required=""/> Wanita </label>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  3. Tempat & Tanggal Lahir
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="tpt_lahir" class="form-control" placeholder="Tempat Lahir" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="tgl_lahir" class="form-control" placeholder="23/12/1992" id="dp" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  4. Alamat
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <textarea class="form-control" name="alamat" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"></textarea>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  5. Instansi/sekolah
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="instansi" class="form-control" placeholder="Kosongkan jika tidak ada"/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  6. No. Telephone (Utama)
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="number" name="telp1" class="form-control" required=""/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  7. No. Telephone (Sekunder)
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="number" name="telp2" class="form-control"/>
                </div>
                <div class="col-md-5 col-xs-12 pull-left h-f1">
                  8. Akun Instagram
                </div>
                <div class="col-md-7 col-xs-12 pull-left h-f2">
                  <input type="text" name="twitter" class="form-control" placeholder="tebas.award"/>
                </div>

                <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
                  <button type="submit" class="btn btn-success pd-btn"> Daftar </button>
                  &nbsp;&nbsp;&nbsp;
                  <button type="reset" class="btn btn-danger pd-btn"> Batal </button>
                </div>

                </form>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
