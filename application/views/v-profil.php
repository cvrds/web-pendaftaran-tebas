<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
  <div class="container">
    <div class="col-md-12">
       <h2 class="caption">PROFIL</h2>
    </div>
    <div class="col-md-12" style="background:rgba(224, 224, 224, 0.5);border-radius:5px">
      <div class="row">
      <div class="col-md-3">
        <h2>MENU</h2>
        <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="<?php echo site_url('profil/peserta/'.tebas_encrypt($id));?>">Home</a></li>
        <li><a href="<?php echo site_url('profil/edit/'.tebas_encrypt($id));?>">Profile</a></li>
        <li><a href="<?php echo site_url('profil/upload/'.tebas_encrypt($id));?>">Upload Karya</a></li>
        <li><a href="<?php echo site_url('beranda/logout');?>">Logout</a></li>
      </ul>
      </div>
      <div class="col-md-9">
        <h2>DAFTAR KOMPETISI</h2>
    		<a href="<?php echo site_url('kompetisi/invect');?>"><button type="button" class="btn btn-default btn-sm" style="font-size:15px" >Indie Movie Competition</button></a>
    		<a href="<?php echo site_url('kompetisi/fotografi');?>"><button type="button" class="btn btn-default btn-sm" style="font-size:15px" >Photography Contest</button></a>
    		<a href="<?php echo site_url('kompetisi/poster');?>"><button type="button" class="btn btn-default btn-sm" style="font-size:15px" >Battle of Digital Illustration</button></a>
    		<br>

    <?php if(count($invect) != 0){?>
		<h3>Indie Movie Competition</h3>
        <table class="table" style="margin-top:10px">
        <thead>
          <tr>
            <th width="2%">#</th>
            <th width="17%">No Pendaftar</th>
            <th width="33%">Judul</th>
            <th width="14%">Karya</th>
			<th width="14%">Kontribusi</th>
            <th width="20%">Aksi</th>
          </tr>
        </thead>
        <tbody>
        <?php $no=1; foreach ($invect as $row) { ?>
          <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->no_pendf;?></td>
            <td><?php echo $row->judul;?></td>
		<td><?php echo ucfirst($row->berkas);?></td>
            <td><?php echo ucfirst($row->bayar);?></td>
            <td>
              <a href="<?php echo site_url('registrasi/download/invect/'. tebas_encrypt($row->id_peserta_invect) );?>" target="blank">
              <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Download">
                <span class="glyphicon glyphicon-save"></span>
              </button>
              </a>
              <a href="<?php echo site_url('profil/editInvect/'.tebas_encrypt($id).'/'.tebas_encrypt($row->id_peserta_invect) );?>">
              <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Edit">
                <span class="glyphicon glyphicon-pencil"></span>
              </button>
              </a>
              <?php if($row->bayar == "belum"){?>
                <a href="<?php echo site_url('profil/hapus/invect/'.tebas_encrypt($id).'/'.tebas_encrypt($row->id_peserta_invect) );?>" onClick="javascript:return confirm('Anda yakin ingin menghapus pendaftaran Anda?');">
                <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Delete">
                  <span class="glyphicon glyphicon-remove"></span>
                </button>
                </a>
              <?php }?>
            </td>
          </tr>
        <?php $no++;}?>
        </tbody>
      </table>
  <?php }?>
  <?php if(count($fotografi) != 0){?>
	  <h3>Photograhy Contest</h3>
      <table class="table" style="margin-top:10px">
        <thead>
          <tr>
            <th width="2%">#</th>
            <th width="17%">No Pendaftar</th>
            <th width="33%">Judul</th>
		<th width="14%">Karya</th>
            <th width="14%">Kontribusi</th>
            <th width="20%">Aksi</th>
          </tr>
        </thead>
        <tbody>
        <?php $no=1; foreach ($fotografi as $row) { ?>
          <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->no_pendf;?></td>
            <td><?php echo $row->judul;?></td>
		<td><?php echo ucfirst($row->berkas);?></td>
		<td><?php echo ucfirst($row->bayar);?></td>
            <td>
              <a href="<?php echo site_url('registrasi/download/fotografi/'.tebas_encrypt($row->id_peserta_fotografi) );?>" target="blank">
              <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Download">
                <span class="glyphicon glyphicon-save"></span>
              </button>
              </a>
              <a href="<?php echo site_url('profil/editFotografi/'.tebas_encrypt($id).'/'.tebas_encrypt($row->id_peserta_fotografi) );?>">
              <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Edit">
                <span class="glyphicon glyphicon-pencil"></span>
              </button>
              </a>
              <?php if($row->bayar == "belum"){?>
                <a href="<?php echo site_url('profil/hapus/fotografi/'.tebas_encrypt($id).'/'.tebas_encrypt($row->id_peserta_fotografi) );?>" onClick="javascript:return confirm('Anda yakin ingin menghapus pendaftaran Anda?');">
                <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Delete">
                  <span class="glyphicon glyphicon-remove"></span>
                </button>
                </a>
              <?php }?>
            </td>
          </tr>
        <?php $no++;}?>
        </tbody>
      </table>
  <?php }?>
  <?php if(count($bodi) != 0){?>
	  <h3>Battle of Digital Illustration</h3>
      <table class="table" style="margin-top:10px">
        <thead>
          <tr>
            <th width="2%">#</th>
            <th width="17%">No Pendaftar</th>
            <th width="33%">Judul</th>
		<th width="14%">Karya</th>
            <th width="14%">Kontribusi</th>
            <th width="20%">Aksi</th>
          </tr>
        </thead>
        <tbody>
        <?php $no=1; foreach ($bodi as $row) { ?>
          <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $row->no_pendf;?></td>
            <td><?php echo $row->judul;?></td>
		<td><?php echo ucfirst($row->berkas);?></td>
		<td><?php echo ucfirst($row->bayar);?></td>
            <td>
              <a href="<?php echo site_url('registrasi/download/bodi/'.tebas_encrypt($row->id_peserta_bodi) );?>" target="blank">
              <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Download">
                <span class="glyphicon glyphicon-save"></span>
              </button>
              </a>
              <a href="<?php echo site_url('profil/editBodi/'.tebas_encrypt($id).'/'.tebas_encrypt($row->id_peserta_bodi) );?>">
              <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Edit">
                <span class="glyphicon glyphicon-pencil"></span>
              </button>
              </a>
              <?php if($row->bayar == "belum"){?>
                <a href="<?php echo site_url('profil/hapus/bodi/'.tebas_encrypt($id).'/'.tebas_encrypt($row->id_peserta_bodi) );?>" onClick="javascript:return confirm('Anda yakin ingin menghapus pendaftaran Anda?');">
                <button type="button" class="btn btn-danger btn-sm" style="font-size:15px" title="Delete">
                  <span class="glyphicon glyphicon-remove"></span>
                </button>
                </a>
              <?php }?>
            </td>
          </tr>
        <?php $no++;}?>
        </tbody>
      </table>
  <?php }?>


      <?php if(count($invect) == 0 && count($fotografi) == 0 && count($bodi) == 0){?>
        <div class="col-md-12" style="height:80px;background:#fff;margin:15px 0;border-radius:10px;">
          <h2>Belum ada kompetisi yang diikuti</h2>
        </div>
      <?php }?>
      </div>
    </div>
    </div>

  </div>
</div>
