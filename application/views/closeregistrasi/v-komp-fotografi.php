<?php $this->auth->restrict();?>
<!-- FORM -->
<div id="mainkonten">
<div class="container pc">
    <div class="col-md-12">
       <h2 class="caption">PHOTOGRAPHY</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h3>Tema : Manusia dan Pembangunan</h4>
        <h4>Karya yang dihasilkan menggambarkan suatu daerah tentang keadaan dampak pembangunan baik positif atau negatif.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
			  <li>Peserta lomba perorangan dan terbuka untuk umum.</li>
              <li>Karya harus orisinil dan belum pernah memenangkan lomba foto di TEBAS.</li>
              <li>Karya tidak menyingung unsur SARA (Suku, Agama, dan Ras) dan pornografi serta tidak bertentangan dengan ketentuan perundangan yang berlaku di Indonesia.</li>
              <li>Karya foto berwarna.</li>
              <li>Karya yang dikumpulkan dalam bentuk softcopy, di upload / diunggah ke tebasaward.com dengan ketentuan dengan sisi terpanjang 3000 pixel, format* JPG/JPEG dengan ukuran minimal 2 MB dan maksimal 6 MB dan disertakan diskripsi karya.</li>
              <li>Kaya dibuat dari tiga tahun terakhir.</li>
              <li>Perseta wajib memahami dan mamatuhi ketentuan & syarat lomba seperti yang tercantum di halaman ini.</li>
              <li>Panitia berhak menggunakan karya perseta lomba untuk kegiatan promosi dan publikasi tanpa memberikan konpensasi apapun namun hak cipta karya tetap milik fotografer.</li>
              <li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.</li>
              <li>Keputusan juri mutlak dan tidak bisa diganggugat.</li>
              <li>Editing di perbolehkan sebatas dapat dilakukan di kamar gelap : <i>burning, dodging,</i> dan <i>cropping</i>.</li>
              <li>Diperkenankan mengirim lebih dari 1 (satu) karya dengan pembayaran sejumlah karya yang dikirim.</li>
              <li>IDR :25K per karya yang dikirim.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Biaya Pendaftaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Biaya Pendaftaran Photography Contest adalah :<br>
                - Katagori Umum&nbsp;:&nbsp; Rp. 25.000,-
            </li>
            <li>Biaya pendaftaran diatas berlaku untuk 1 karya dan dikalikan sesuai jumlah karya.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh karya Photography Contest yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh karya peserta yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, Karya peserta akan masuk pada tahap penilaian Juri.</li>
            <li>Semua karya akan di tampilkan di lokasi pameran TEBAS (SHOW UP).</li>
            <li>Pemenang kategori Photography Contest terfavorit diambil dari semua karya yang lolos tahap awal penjurian. Polling like dilakukan di <a href="https://www.facebook.com/TEBASAWARD" target="_blank"> <b>Fanspage TEBAS AWARD</b> </a> pada saat SHOW UP TEBAS 2016 berlangsung.</li>
            <li>Pemenang akan diumumkan dalam acara TEBAS AWARDS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Juara I</td>
          <td>Trophy + Sertifikat + Rp 700.000,-</td>
          </tr>
          <tr>
          <td>Juara II</td>
          <td>Trophy + Sertifikat + Rp 500.000,-</td>
          </tr>
          <tr>
          <td>Juara Favorit</td>
          <td>Trophy + Sertifikat + Bingkisan</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Cara Pengumpulan Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">
          <ol>
            <li>Silahkan Login Akun TEBAS dan pilih menu profil --> upload karya</li>
            <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
            <li>Pilih kategori lomba</li>
            <li>File berformat rar/zip yang berisi :
            <ul>
             <li>Karya</li>
      			 <li>Bukti Transaksi (Gambar/Scan)</li>
             <li>Kartu Identitas KTM/KTP (Gambar/Scan)</li>
             <li>Surat Pernyataan (Gambar/Scan)</li>
             </ul>
            </li>
            <li><b>File *.doc</b> berisi nama lengkap, judul dan deskripsi karya.</li>
            <li>Ukuran file .rar/.zip max 6MB</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSeven" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSeven">
          <div class="panel-body text-left">

            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Photography Contest = Rp. 25.000,- <br>
                Nomor HP Anda = 0857 0206 9611<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 25.611</i>
              </li>
              <li>Kirim ke nomor rekening MUAMALAT 5370004949<br>AN : Fanny Septiani.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 0896 7883 4989 (Fanny) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Arif Rusmandani#25.611</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim.</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>

          </div>
          </div>
        </div>
      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center"><label>FORMULIR PENDAFTARAN<br />PHOTOGRAPHY CONTEST</h3>
      <h4 class="text-center" style="color:#7A0000">Tema : Manusia dan Pembangunan</h4><br /><br />
      <form role="form" action="<?php echo site_url('registrasi/daftarFotografi');?>" method="POST">

      <div class="col-md-12 col-xs-12 pull-left h-f1">
        <label>A. DATA PENDAFTAR</label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        1. Nama
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="nama" value="<?php echo $peserta->nama;?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        2. Jenis Kelamin
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <?php if($peserta->jk == "Pria"){ $pria = "checked"; $Wanita = ""; }else{ $pria = ""; $Wanita = "checked"; }?>
        <label><input type="radio" name="karyapertama" value="Pria" <?php echo $pria; ?> class=""/> Pria </label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <label><input type="radio" name="karyapertama" value="Wanita" <?php echo $Wanita; ?> class=""/> Wanita </label>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        3. Tempat & Tanggal Lahir
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->tpt_lahir; ?>" class="form-control" placeholder="Tempat Lahir" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1"></div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->tgl_lahir; ?>" class="form-control" placeholder="23/12/1992" id="dp" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        4. Alamat
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter" disabled><?php echo $peserta->alamat; ?></textarea>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        5. Instansi/sekolah
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->instansi; ?>" class="form-control" placeholder="Kosongkan jika tidak ada" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        6. No. Telephone (Utama)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" value="<?php echo $peserta->telp1; ?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        7. No. Telephone (Sekunder)
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="number" value="<?php echo $peserta->telp2; ?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        8. Email
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->email; ?>" class="form-control" disabled>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        9. Akun Instagram
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" value="<?php echo $peserta->twitter; ?>" class="form-control" placeholder="tebas.award" disabled>
      </div>
     <div class="col-md-5 col-xs-12 pull-left h-f1">
        9. Judul Karya
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <input type="text" name="judul" required="" class="form-control"/>
      </div>
      <div class="col-md-5 col-xs-12 pull-left h-f1">
        10. Deskripsi Karya
      </div>
      <div class="col-md-7 col-xs-12 pull-left h-f2">
        <textarea class="form-control" name="deskripsi" maxlength="300" required="" rows="4" style="margin-bottom:10px;" placeholder="maksimal 300 karakter"></textarea>
      </div>

      <div class="col-md-12 col-xs-12 pull-left h-f1" style="padding-top:30px; padding-bottom:20px;">
        <input type="hidden" name="id_regis" value="<?php echo $peserta->id_registrasi; ?>">
        <button type="submit" class="btn btn-success pd-btn"> Daftar </button>
        &nbsp;&nbsp;&nbsp;
        <button type="reset" class="btn btn-danger pd-btn"> Batal </button>
      </div>

      </form>
    </div>

  </div>
</div>
