<!--  HEADER -->
<div id="header">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%; visibility:hidden;" alt="header">
  <div class="container header" style="margin-top:-50px;">
    <div class="col-md-12">
          <center><img src="<?php echo base_url();?>assets/img/tebas.png" alt="logo" class="img-responsive"></center>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center">
        <?php if($this->auth->is_logged_in() == false){?>
          <h1>PENDAFTARAN TEBAS 2016 SUDAH DI TUTUP</h1>
          <h2>SAMPAI BERTEMU DI TEBAS 2017</h2>
        <?php }else{ ?>
          <h1>SELAMAT DATANG <a href="<?php echo site_url('profil/peserta/'.$this->session->userdata('id_registrasi'));?>" style="color:#7A0000" title="Lihat Profil"><b><?php echo strtoupper($this->session->userdata('nama'));?></b></a></h1>
          <p>TERIMAKASIH ATAS PARTISIPASINYA DI TEBAS 2016</p>
        <?php }?>
    </div>
  </div>
  <br>
  <br>
  <br>
</div>
