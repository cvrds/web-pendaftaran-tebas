<!-- FORM -->
<div id="mainkonten">
<div class="container invect">
    <div class="col-md-12">
       <h2 class="caption">INVECT</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h3>Tema : Bebas (Non Dokumenter)</h4>
        <h4>Karya yang dihasilkan bertemakan bebas tapi bukan film dokumenter. Diharapkan, hasil karya ini mengandung pesan untuk yang melihatnya dan dapat tersampaikan dengan baik.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse <!--in-->" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
            <li>Lomba ini berlaku untuk umum.</li>
      			<li>Karya harus orisinil dan belum pernah menjuarai kategori lomba TEBAS.</li>
      			<li>Karya tidak diperkenankan mengandung unsur SARA (Suku, Agama, dan RAS) , tidak mengandung unsur pornografi serta tidak bertentangan dengan ketentuan perundangan yang berlaku di Ind onesia.</li>
      			<li>Peserta wajib memahami dan mematuhi seluruh ketentuan & syarat lomba seperti yang tercantum di halaman ini.</li>
      			<li>Panitia berhak menggunakan karya peserta dan pemenang lomba untuk kegiatan promosi dan publikasi tanpa memberikan kompensasi apapun dengan hak cipta karya tetap dipegang oleh pemilik karya.</li>
      			<li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.</li>
      			<li>Keputusan juri tidak bisa diganggu gugat.</li>
      			<li>Ketentuan yang belum di atur dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
      			<li>Karya yang sudah pernah diikutkan dalam ajang festival lain, boleh diikutsertakan.</li>
      			<li>Wajib mengirimkan poster film berupa hardcopy (karya cetak) kertas ivory 250 gr ukuran A3 untuk standarisasi karya peserta ditampilkan pada SHOW UP TEBAS 2016. </li>
      			<li>Menyertakan 1 DVD yang berisi Film, Trailer durasi minimal 30 detik (MPEG atau MOV), poster film berupa softCopy high-resolution, format *.jpg dengan resolusi minimal 300dpi, label, cover, sinopsis film, biodata sutradara, biodata pengirim (nomor telepon, fotokopi identitas) yang masih berlaku dalam bentuk teks.</li>
      			<li>Durasi maksimal 20 menit dengan format MPEG atau MOV (sudah termasuk opening dan credit title karya).</li>
      			<li>Menggunakan lagu tema atau scoring original. Apabila menggunakan karya orang atau pihak lain, wajib melampirkan keterangan dari pihak yang bersangkutan. </li>
      			<li>Tahun pembuatan karya maksimal dari tahun 2013.</li>
      			<li>Bahasa yang dapat digunakan di dalam keseluruhan film adalah bahasa Indonesia, bahasa asing maupun bahasa daerah. Jika menggunakan bahasa asing atau bahasa daerah diwajibkan untuk menggunakan subtitle bahasa Indonesia.</li>
      			<li>Diperkenankan mengirim lebih dari 1 (satu) karya dengan pembayaran sejumlah karya yang dikirim.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Biaya Pendaftaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Biaya Pendaftaran Invect adalah :<br>
                - Kategori Pelajar&nbsp;:&nbsp; Rp. 55.000,- <br>
                - Kategori Umum&nbsp;:&nbsp; Rp. 65.000,-
            </li>
            <li>Biaya pendaftaran diatas berlaku untuk 1 karya dan dikalikan sesuai jumlah karya.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh film yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh film peserta yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, film peserta akan masuk pada tahap penilaian juri.</li>
            <li>Film yang akan di putarkan pada screening SHOW UP TEBAS 2016 adalah 25 Film pilihan juri.</li>
            <li>Pemenang kategori film terfavorit diambil dari 25 film yang di screeningkan pada SHOW UP TEBAS 2016 melalui tahap polling like di youtube <a href="https://www.youtube.com/channel/UCfkirEZV_maYMW6LDC5x_Kw" target="_blank"> <b>Tebas Award</b></a> pada saat SHOW UP TEBAS 2016 berlangsung.</li>
            <li>Pemenang akan diumumkan dalam acara TEBAS AWARDS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Film Terbaik</td>
          <td>Trophy + Sertifikat + Rp 1.200.000,-</td>
          </tr>
          <tr>
          <td>Cerita Terbaik</td>
          <td>Trophy + Sertifikat + Rp 1.000.000,-</td>
          </tr>
          <tr>
          <td>Sinematografi</td>
          <td>Trophy + Sertifikat + Rp 800.000,-</td>
          </tr>
          <tr>
          <td>Film Terfavorit</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Tata Suara Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Penyutradaraan Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Pemeran Utama Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr>
          <td>Penata Artistik Terbaik</td>
          <td>Trophy + Sertifikat</td>
          </tr>
          <tr class="warning">
          <th colspan="2">PELAJAR :</th>
          </tr>
          <tr>
          <td>Film Terbaik</td>
          <td>Trophy + Sertifikat + Rp 1.000.000,-</td>
          </tr>
          <tr>
          <td>Cerita Terbaik</td>
          <td>Trophy + Sertifikat + Rp 800.000,-</td>
          </tr>
          <tr>
          <td>Sinematografi</td>
          <td>Trophy + Sertifikat + Rp 600.000,-</td>
          </tr>
          <tr>
          <td>Film Terfavorit</td>
          <td>Trophy + Sertifikat</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Cara Pengumpulan Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">
          <ol>
            <li>Di kirim ke Sekretariatan KOMA di Gedung BSC lantai 3 ruang 6 (VI.3.6), Universitas AMIKOM Yogyakarta Jl. Ringroad Utara, Condongcatur, Yogyakarta</li>
            <li><b>Dengan Format: </b> Karya, bukti pendaftaran, uang atau bukti transfer, fotocopy KTP/KTM dan surat rekomendasi* (*untuk kategori Pelajar) dimasukkan kedalam Amplop coklat dan dituliskan nama dan alamat pengirim dan di sebaliknya dituliskan alamat kesekretariatan KOMA.</li>
          </ol>
          </div>
          </div>
        </div>
		<div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSeven" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSeven">
          <div class="panel-body text-left">
          <h4>KIRIM BERSAMA KARYA</h4>
            <ol>
              <li>Masukkan biaya pendaftaran langsung bersama karya yang akan di kirim.</li>
              <li>Setelah mengirim karya bersama biaya pendaftaran, konfirmasi sms di nomor 0896 7883 4989 (Fanny)

            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Indie Movie Competition = <br>
                - Rp. 65.000,- (Umum)<br>
                - Rp. 55.000,- (Pelajar)<br>
                Nomor HP Anda = 0857 0206 9611<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 55.611</i>
              </li>
              <li>Kirim ke nomor rekening MUAMALAT 5370004949<br>AN : Fanny Septiani.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 0896 7883 4989 (Fanny) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Arif Rusmandani#55.611</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim, bukti pendaftaran, FC KTP/KTM dan surat rekomendasi(pelajar)</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>
          </div>
          </div>
        </div>

      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center"><label>FORMULIR PENDAFTARAN<br />INDIE MOVIE COMPETITION (INVECT)</label></h3>
      <h4 class="text-center" style="color:#7A0000">Tema : Bebas (Non Dokumenter)</h4><br /><br />

        <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" class="">
                LOGIN
              </a>
            </h4>
          </div>
          <div id="collapseOne1" class="panel-collapse in" style="height: auto;">
            <div class="panel-body">
              <div class="col-md-12">
              <form class="form-signin" action="<?php echo site_url('beranda/login');?>" method="POST" role="form">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input type="text" name="email" class="form-control h-f1" placeholder="Email address" required="" autofocus="">
                <input type="password" name="password" class="form-control h-f1" placeholder="Password" required="">
                <br><br>
                <button class="btn btn-lg btn-warning btn-block" type="submit">Sign in</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1" class="collapsed">
                REGISTER
              </a>
            </h4>
          </div>
          <div id="collapseTwo1" class="panel-collapse collapse">
            <div class="panel-body">
              <center><h4>PENDAFTARAN TEBAS 2016 SUDAH DI TUTUP<br>SAMPAI BERTEMU DI TEBAS 2017</h4></center>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
