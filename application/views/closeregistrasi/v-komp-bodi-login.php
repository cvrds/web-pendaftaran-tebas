<!-- FORM -->
<div id="mainkonten">
  <div class="container bodi">
    <div class="col-md-12">
       <h2 class="caption">BATTLE OF DIGITAL ILLUSTRATION</h2>
    </div>
    <div class="col-md-6 col-xs-12 c-form2" style="text-align:center;padding:21px 0 20px 0;margin-botom:20px;">
      <img src="<?php echo base_url();?>assets/img/tebas.png" style="width:80%;margin-bottom:30px;"/>
      <div class="col-md-12" style="padding-bottom:10px;color:#7A0000">
        <h3>Tema : Pesan untuk Tanah Airku</h4>
        <h4>Visualkan hal-hal yang menurut anda baik dan memotivasi dalam bentuk poster, sehingga orang lain yang melihat akan merasa ingin dan harus melalukan pesan pada poster tersebut.</h4>
      </div>
    <div role="navigation" id="sidebar" class="col-xs-12 col-sm-12 col-md-12 sidebar-offcanvas">
          <div class="list-group">
      <div id="accordion" class="panel-group">
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseOne" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Ketentuan Lomba
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseOne">
          <div class="panel-body text-left">
          <ol>
            <li>Lomba ini berlaku untuk umum.</li>
            <li>Peserta lomba adalah perorangan.</li>
            <li>Karya harus orisinil dan belum pernah menjuarai kategori lomba TEBAS.</li>
            <li>Karya tidak diperkenankan mengandung unsur SARA (Suku, Agama, dan RAS) , tidak mengandung unsur pornografi serta tidak bertentangan dengan ketentuan perundangan yang berlaku di Indonesia.</li>
            <li>Desain poster bebas mengunakan teknik apapun (digital,manual,campuran).</li>
            <li>Karya yang dikumpulkan dalam bentuk softcopy (CMYK), diupload / diunggah ke   tebasaward.com dengan ketentuan ukuran A3, high-resolution/300 dpi , format *.JPG/JPEG  dengan ukuran minimal 2 MB dan maksimal 6 MB dan disertai dengan deskripsi karya.</li>
            <li>Hasil karya dilarang mengikuti perlombaan sejenis selama acara TEBAS berlangsung.</li>
            <li>Panitia berhak menggunakan karya peserta dan pemenang lomba untuk kegiatan promosi dan publikasi tanpa memberikan kompensasi apapun dengan hak cipta karya tetap dipegang oleh pemilik karya.</li>
            <li>Peserta wajib memahami dan mematuhi seluruh ketentuan & syarat lomba seperti yang tercantum di halaman ini.</li>
            <li>Panitia berhak mendiskualifikasi peserta yang tidak memenuhi ketentuan dan persyaratan lomba.</li>
            <li>Keputusan juri tidak bisa diganggu gugat.</li>
            <li>Ketentuan yang belum di atur dapat di buat kemudian berdasarkan kesepakatan panitia TEBAS.</li>
            <li>Diperkenankan mengirim lebih dari 1 (satu) karya dengan pembayaran sejumlah karya yang dikirim.</li>
            <li>IDR :25K per Karya yang dikirimkan.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseTwo" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Biaya Pendaftaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseTwo">
          <div class="panel-body text-left">
          <ol>
            <li>Biaya Pendaftaran Battle Of Digital Illustration adalah :<br>
                - Katagori Umum&nbsp;:&nbsp; Rp.    20.000,-
            </li>
            <li>Biaya pendaftaran diatas berlaku untuk 1 karya dan dikalikan sesuai jumlah karya.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseThree" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Penjurian
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseThree">
          <div class="panel-body text-left">
          <ol>
            <li>Seluruh karya Battle Of Digital Illustration yang masuk akan melalui tahap awal seleksi oleh panitia. Kriteria yang di gunakan panitia adalah seluruh karya peserta yang memenuhi syarat administratif dan ketentuan-ketentuan yang di buat oleh panitia.</li>
            <li>Setelah lolos tahap awal oleh panitia, Karya peserta akan masuk pada tahap penilaian Juri.</li>
            <li>Semua karya akan di tampilkan di lokasi pameran TEBAS (SHOW UP).</li>
            <li>Pemenang kategori Battle Of Digital Illustration terfavorit diambil dari semua karya yang lolos tahap awal penjurian. Polling like dilakukan di <a href="https://www.facebook.com/TEBASAWARD" target="_blank"> <b>Fanspage TEBAS AWARD</b> </a> pada saat SHOW UP TEBAS 2016 berlangsung.</li>
            <li>Pemenang akan diumumkan dalam acara TEBAS AWARDS.</li>
          </ol>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFour" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Hadiah Perlombaan
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFour">
          <div class="panel-body text-left">

        <table class="table">
          <tr class="warning">
          <th colspan="2">UMUM :</th>
          </tr>
          <tr>
          <td>Juara I</td>
          <td>Trophy + Sertifikat + Rp 700.000,-</td>
          </tr>
          <tr>
          <td>Juara II</td>
          <td>Trophy + Sertifikat + Rp 500.000,-</td>
          </tr>
          <tr>
          <td>Juara Favorit</td>
          <td>Trophy + Sertifikat + Bingkisan</td>
          </tr>
        </table>
          </div>
          </div>
        </div>
        <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;">
            <a href="#collapseFive" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Cara Pengumpulan Karya
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseFive">
          <div class="panel-body text-left">
          <ol>
            <li>Silahkan Login Akun TEBAS dan pilih menu profil --> upload karya</li>
            <li>Masukan No Pendaftaran dan Nama Lengkap sesuai dengan kwitansi pendaftaran</li>
            <li>Pilih kategori lomba</li>
            <li>File berformat rar/zip yang berisi :
              <ul>
             <li>Karya</li>
			       <li>Bukti Transaksi (Gambar/Scan)</li>
             <li>Kartu Identitas KTM/KTP (Gambar/Scan)</li>
             <li>Surat Pernyataan (Gambar/Scan)</li>
             </ul>
            </li>
            <li><b>File *.doc</b> berisi nama lengkap, judul dan deskripsi karya.</li>
            <li>Ukuran file .rar/.zip max 6MB</li>
          </ol>
          </div>
          </div>
        </div>
		    <div class="panel panel-default" style="border-radius:0px; margin:0; border:1px;border-bottom:solid 1px #ddd">
            <a href="#collapseSix" class="list-group-item" data-parent="#accordion" data-toggle="collapse" style="border-radius:0px;">
            Metode Pembayaran
            <span class="glyphicon glyphicon-circle-arrow-right" style="float:right; font-size:16px;"></span>
            </a>

          <div class="panel-collapse collapse" id="collapseSix">
          <div class="panel-body text-left">
            <h4>TRANSFER</h4>
            <ol>
              <li>
                Transfer sejumlah biaya pendaftaran ditambah 3 digit nomor terakhir Handphone Anda.<br>
                <i>Contoh : <br>
                Biaya pendaftaran kompetisi Battle Of Digital Illustration = Rp. 20.000,- <br>
                Nomor HP Anda = 0857 0206 9611<br>
                Maka total biaya yang harus ditransfer adalah = Rp. 20.611</i>
              </li>
              <li>Kirim ke nomor rekening MUAMALAT 5370004949<br>AN : Fanny Septiani.</li>
              <li>Setelah transfer, konfirmasi sms di nomor 0896 7883 4989 (Fanny) <br>
              <i>Dengan format = No Pendaftaran # Nama Pengirim # Jumlah Transfer <br>
              Contoh : 1234#Arif Rusmandani#20.611</i></li>
              <li>Masukkan bukti transfer pembayaran bersama karya yang dikirim.</li>
            </ol>

            <h4>LANGSUNG</h4>
            <ol>
              <li>Membayar biaya pendaftaran langsung ke Kesekretariatan KOMA.<br>
                <i>Gedung UKM Lt. 3 Ruang VI.3.6 Universitas AMIKOM Yogyakarta</i>
              </li>
            </ol>

          </div>
          </div>
        </div>

      </div>

      </div>
        </div>
    <!--
    <div style="width:100%;">
      <img src="image/m-foot.png" style="width:100%;"/>
    </div>
    -->
    </div>
    <div class="col-md-6 col-xs-12 c-form" style="min-height:200px; padding-bottom:20px;">
      <h3 class="text-center"><label>FORMULIR PENDAFTARAN<br />BATTLE OF DIGITAL ILLUSTRATION</label></h3>
      <h4 class="text-center" style="color:#7A0000">Tema : Pesan untuk Tanah Airku</h4><br /><br />

        <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" class="">
                LOGIN
              </a>
            </h4>
          </div>
          <div id="collapseOne1" class="panel-collapse in" style="height: auto;">
            <div class="panel-body">
              <div class="col-md-12">
              <form class="form-signin" action="<?php echo site_url('beranda/login');?>" method="POST" role="form">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input type="text" name="email" class="form-control h-f1" placeholder="Email address" required="" autofocus="">
                <input type="password" name="password" class="form-control h-f1" placeholder="Password" required="">
                <br><br>
                <button class="btn btn-lg btn-warning btn-block" type="submit">Sign in</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo1" class="collapsed">
                REGISTER
              </a>
            </h4>
          </div>
          <div id="collapseTwo1" class="panel-collapse collapse">
            <div class="panel-body">
             <center><h4>PENDAFTARAN TEBAS 2016 SUDAH DI TUTUP<br>SAMPAI BERTEMU DI TEBAS 2017</h4></center>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
