<!--  HEADER -->
<div id="mainkontenjuri">
  <img src="<?php echo base_url();?>assets/img/header.png" class="img-responsive" style="width:100%; visibility:hidden;" alt="header">
  <div class="container header" style="margin-top:-150px;">
  <div class="col-md-12">
     <h2 class="caption" style="color:#000">TIMELINE ACARA TEBAS 2017</h2>
    </div>
<!-- <div class="thumb">
    <a href="#">
        <span>THE BEST ANNUAL MULTIMEDIA SHOW 2017</span>
    </a>
</div> -->

    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">


    <div class="col-md-12 col-sm-12 col-xs-12 tebas-event-top-section">
        <div class="col-md-3">
            <div id="tebas-event-box-1" class="boxes">
            <h3 class="has-sub-title">
            <i class="fa fa-compass" aria-hidden="true"></i>
                <span>WORKSHOP</span>
                <small>Loop Station Yogyakarta</small>
            </h3>
            <div class="description-box description-switch">
                <div class="description"> Acara Workshop ini akan diadakan di </div>
                <div class="sub-description"> Loop Station Yogyakarta </div>
            </div> <a href="#" class="btns tebas-event-box-button inpage-link">Learn More</a>
            </div>
        </div>
        <div class="col-md-3">
            <div id="tebas-event-box-2" class="boxes">
            <h3 class="has-sub-title">
            <i class="fa fa-users" aria-hidden="true"></i>
                <span>VOTING</span>
                <small>Online Facebook dan Youtube</small>
            </h3>
            <div class="description-box description-switch">
                <div class="description"> Acara VOTING karya ini dilakukan pada saat SHOW UP dan Online pada </div>
                <div class="sub-description"> 13-14 Mei 2017 </div>
            </div> <a href="#" class="btns tebas-event-box-button inpage-link">Learn More</a>
            </div>
        </div>
        <div class="col-md-3">
            <div id="tebas-event-box-3" class="boxes">
            <h3 class="has-sub-title">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
                <span>SHOW UP</span>
                <small>Loop Station Yogyakarta</small>
            </h3>
            <div class="description-box description-switch">
                <div class="description"> Acara pameran karya diadakan pada </div>
                <div class="sub-description"> 13-14 Mei 2017 </div>
            </div> <a href="#" class="btns tebas-event-box-button inpage-link">Learn More</a>
            </div>
        </div>
        <div class="col-md-3">
            <div id="tebas-event-box-4" class="boxes">
            <h3 class="has-sub-title">
            <i class="fa fa-trophy" aria-hidden="true"></i>
                <span>AWARDS</span>
                <small>Taman Budaya Yogyakarta</small>
            </h3>
            <div class="description-box description-switch">
                <div class="description"> Acara malam penganugerahan diadakan pada </div>
                <div class="sub-description"> 22 Mei 2017 </div>
            </div> <a href="#" class="btns tebas-event-box-button inpage-link">Learn More</a>
            </div>
        </div>
    </div>
    <div class="col-md-12">
       <div class="col-md-6">
           <div class="table-responsive">
    <table class="table table-bordered">
      <div class="judul-acara"><b>COMING SOON</b></div>

        <thead>
            <tr>
                <th>No</th>
                <th>Waktu</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">5</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">6</th>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>
       </div>
       <div class="col-md-6">
           <div class="table-responsive">
<table class="table table-bordered">
   <div class="judul-acara"><b>COMING SOON</b></div>
    <thead>
        <tr>
            <th>No</th>
            <th>Waktu</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th scope="row">4</th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th scope="row">5</th>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th scope="row">6</th>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
</div>
       </div>

        <div class="col-md-12">
           <div class="table-responsive">
            <table class="table table-bordered">
               <div class="judul-acara"><b>COMING SOON</b></div>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Waktu</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            </div>
       </div>

    </div>
    </div>
  </div>
  <br>

</div>
<!--  KOMPETISI -->
<!--
<div id="competition">

</div>
-->
