<!DOCTYPE html>
<html>
<head>
<title>Formulir Registrasi Photography Contest Tebas 2017</title>
<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon">
<style>
body {
    margin: 0;
    padding: 0;
	font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
    font-size: 10pt;
}
* {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}
.page {
    width: 21cm;
    min-height: 29cm;
    margin:auto;
}
.subpage {padding: 0 1.5cm;}
@page {
    size: A4;
    margin: 0;
}
@media print {
    .page {
        margin: 0;
        width: initial;
        min-height: initial;
        background: initial;
        page-break-after: always;
    }
}
h2, h3 {text-align:center}
table.border {display:block;border:solid 1px;padding:10px}
h2 {margin-bottom:0;margin-top:-10px}
hr {border:dashed 1px #bbb;margin:10px 0}
div.garis {border-bottom:solid 1px;padding-top:15px}
</style>
</head>
<body onload="print()">
<div class="page">
	<div class="header"><img src="<?php echo base_url();?>assets/img/print/header.png"></div>
	<div class="subpage"><br>
	<h2>PENDAFTARAN TEBAS</h2>
	<h3>PHOTOGRAPHY CONTEST</h3>
	<p style="text-align:justify">Pengisian formulir berhasil. Terima kasih atas partisipasi anda dalam acara TEBAS 2017. Kami mengundang Anda dalam acara pameran karya TEBAS 2017 dan acara puncak penganugrahan TEBAS 2017. Info lengkap kunjungi <a href="http://tebasaward.com/">www.tebasaward.com</a> dan jangan lupa follow twitter kami <a href="http://twitter.com/tebasaward">@TEBASAWARD</a>. CP: 085702069611 (Arif) | 089678834989 (Fanny)</p>
	<h3>TANDA BUKTI PENDAFTARAN<br>PHOTOGRAPHY CONTEST TEBAS 2017</h3>
	<table class="border">
		<tr>
			<td width="200">Nomor Pendaftaran</td><td>:</td><td><?php echo $peserta->no_pendf;?></td>
		</tr>
		<tr>
			<td>Nama</td><td>:</td><td><?php echo strtoupper($peserta->nama);?></td>
		</tr>
		<tr>
			<td>Tempat, Tanggal Lahir</td><td>:</td><td><?php echo strtoupper($peserta->tpt_lahir).", "; echo $peserta->tgl_lahir;?></td>
		</tr>
		<tr>
			<td>Alamat</td><td>:</td><td><?php echo strtoupper($peserta->alamat);?></td>
		</tr>
		<tr>
			<td>Telp/HP</td><td>:</td><td><?php echo $peserta->telp1;?></td>
		</tr>
		<tr>
			<td>Email</td><td>:</td><td><?php echo $peserta->email;?></td>
		</tr>
	</table>
	<div style="text-align:right;font-size:10pt;font-style:italic;margin-top:10px">Dicetak tanggal: <?php echo date("d F Y - H:i");?></div>
    <hr>
	<h3>SURAT PERNYATAAN</h3>
	Yang bertanda tangan di bawah ini
	<table style="margin-left:40px">
		<tr>
			<td width="170">Nama</td><td>:</td><td><?php echo strtoupper($peserta->nama);?></td>
		</tr>
		<tr>
			<td>Alamat</td><td>:</td><td><?php echo strtoupper($peserta->alamat);?></td>
		</tr>
		<tr>
			<td>Telp/HP</td><td>:</td><td><?php echo $peserta->telp1;?></td>
		</tr>
		<tr>
			<td>Sekolah/Instansi</td><td>:</td><td><?php echo strtoupper($peserta->instansi);?></td>
		</tr>
	</table>
	Dengan ini menyatakan bertanggung jawab atas materi gambar dengan rincian sebagai berikut :
	<table style="margin-left:40px">
		<tr>
			<td width="170">Judul Karya</td><td>:</td><td><?php echo strtoupper($peserta->judul);?></td>
		</tr>
		<tr>
			<td>Tanggal Pembuatan</td><td>:</td><td width="200">____ /_____ /_________</td>
		</tr>
	</table>
	<p style="text-align:justify;">Yang akan diikutsertakan dalam The Best Annual Multimedia Show (TEBAS) dengan kategori lomba Photography Contest, lomba yang diadakan oleh KOMA Universitas AMIKOM YOGYAKARTA. Segala hal yang berkaitan dengan penyalahgunaan, tidak menjadi tanggung jawab panitia TEBAS yang bertindak sebagai penyelenggara lomba. Demikian surat pernyataan ini saya buat dengan sebenar-benarnya. Adapun isinya dapat dipertanggungjawabkan sebagaimana seharusnya.</p>
		<div style="float:right;text-align:center">
		_____________ , _______________ 2017<br>
		Yang membuat pernyataan<br><br><br><br>
		...................................................
		</div>
	</div>
	<div class="footer"></div>
    </div>
</body>
</html>
