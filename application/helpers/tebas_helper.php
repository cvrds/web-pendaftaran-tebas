<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function tebas_encrypt($string='')
{
	$CI =& get_instance();
	$CI->load->library('encrypt');

	$key 	= "TEBAS2016";

	$CI->encrypt->set_cipher(MCRYPT_BLOWFISH);

	$string 	= $CI->encrypt->encode($string, $key);

	$string = base64_encode($string);

	return $string;
}
function tebas_decrypt($string='')
{
	$CI =& get_instance();
	$CI->load->library('encrypt');
	$string = base64_decode($string);

	$key 	= "TEBAS2016";

	$CI->encrypt->set_cipher(MCRYPT_BLOWFISH);

	$string 	= $CI->encrypt->decode($string, $key);

	return $string;
}
